//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>29</ObjektNummer>	GoToSiSy:d:7

#define GeneratedBySisy
#define cpp_PortInputOutput
#define SISY_CLASS_NAME PortInputOutput
#include "PortInputOutput.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:7|o:29
//
/////////////////////////////
PortInputOutput::PortInputOutput()
{
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:7|o:29
//
/////////////////////////////
PortInputOutput::~PortInputOutput()
{
	
}
/*/////////////////////////////
//
//	config()	GoToSiSy:d:7|o:29
//
/////////////////////////////
*/
void PortInputOutput::config(port_t port, pinMask_t bitmask)
{
	//  aus Template: SysPortInputOutputAvr		GoToSiSy:o:1699|zBase:1
	this->bitmask = bitmask; 
	this->port	  = &port;
	
	// Eingang
	*(this->port-1) &= ~bitmask;	// DDR Register
	
	// Pullup
	#ifdef PinInput_PullUp
		*(this->port) |= bitmask;	// PORTx Register
	#endif
	
	
	#ifdef PinInput_PullDown
		#warning PortInputOutput::config(..) PullDown wird nicht unterstützt
	#endif
	
	// GoToSiSy:d:7|o:29|zbase:18
		

} 
/*/////////////////////////////
//
//	setDirection()	GoToSiSy:d:7|o:29
//
/////////////////////////////
*/
void PortInputOutput::setDirection(bool asOutput)
{
	//  aus Template: SysPortInputOutputAvr		GoToSiSy:o:1700|zBase:1
	if(asOutput)
	{
		*(this->port-1) |= bitmask;	// DDR Register
	}
	else
	{
		*(this->port-1) &= ~bitmask;	// DDR Register
	}
	
	// GoToSiSy:d:7|o:29|zbase:11
		

} 
/*/////////////////////////////
//
//	getData()	GoToSiSy:d:7|o:29
//
/////////////////////////////
*/
portMask_t PortInputOutput::getData()
{
	//  aus Template: SysPortInputOutputAvr		GoToSiSy:o:1701|zBase:1
	portMask_t data = *(port-2) & bitmask;	// PIN Register
	
	// #ifdef PinInput_LowActive
	// 	data = (~data) & bitmask;
	// #endif
	
	// GoToSiSy:d:7|o:29|zbase:8
		
return data;
} 
/*/////////////////////////////
//
//	operator=()	GoToSiSy:d:7|o:29
//
/////////////////////////////
*/
portMask_t PortInputOutput::operator=(const portMask_t& newValue)
{
	//  aus Template: PecPortInputOutput		GoToSiSy:o:15|zBase:1
	setData( newValue );
	
	// GoToSiSy:d:7|o:29|zbase:4
		
return getData();
} 
/*/////////////////////////////
//
//	setData()	GoToSiSy:d:7|o:29
//
/////////////////////////////
*/
void PortInputOutput::setData(portMask_t newData)
{
	//  aus Template: SysPortInputOutputAvr		GoToSiSy:o:1702|zBase:1
	// #ifndef OutputLogicLowActive
		*port = ((*port) & ~bitmask) | (newData & bitmask) ;			// PORTx
	// #else
	// 	*port = ((*port) & ~bitmask) | ((~newData) & bitmask) ;			// PORTx
	// #endif
	
	// GoToSiSy:d:7|o:29|zbase:8
		

} 
/*/////////////////////////////
//
//	setPullUp()	GoToSiSy:d:7|o:29
//
/////////////////////////////
*/
void PortInputOutput::setPullUp(portMask_t mask)
{
	//  aus Template: SysPortInputOutputAvr		GoToSiSy:o:1705|zBase:1
	uint8_t p = *(this->port);
	p &= ~bitmask;
	p |= (mask & bitmask);
	*(this->port) = p; 
	
	//  aus Template: PecPortInputOutput		GoToSiSy:o:17|zBase:1
	
	
	// GoToSiSy:d:7|o:29|zbase:10
		

} 

