//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1088</ObjektNummer>	GoToSiSy:d:1062

#if !defined(h_PecHYT221)
#define h_PecHYT221

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "PecI2cDeviceInterface.h"

#include "pec_Strings.h"

#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class PecHYT221 : public PecI2cDeviceInterface
{

public:
	//	readTemp100Humi()	GoToSiSy:d:1062|o:1089
	//	Gibt Temperatur in 1/100. Grad und rel. Feuchte in % zur�ck.
	//	
	//	PE:
	//		temp=Temperatur
	//		humi=Feuchtigkeit
	bool readTemp100Humi(int16_t& temp, uint8_t& humi);
	//	startCalc()	GoToSiSy:d:1062|o:1091
	//	Startet eine Messung
	bool startCalc();
	//	sendCmd()	GoToSiSy:d:1062|o:1092
	//	Sendet ein Kommando an den Sensor.
	//	
	//	PE:
	//		cmd=Kommando
	//		data=Daten zum Kommando
	bool sendCmd(uint8_t cmd, uint16_t data);
	//	Konstruktor	GoToSiSy:d:1062|o:1088
	PecHYT221();

	//	Destruktor	GoToSiSy:d:1062|o:1088
	~PecHYT221();

private:

protected:
	//	getData()	GoToSiSy:d:1062|o:1090
	//	Liest die Daten des Sensors in den Puffer.
	//	
	//	PE:
	//		pBuffer=zu f�llender Puffer
	bool getData(uint8_t* pBuffer);


};

#endif
