//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1076</ObjektNummer>	GoToSiSy:d:1062

#if !defined(h_PecI2cDeviceInterface)
#define h_PecI2cDeviceInterface

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "PecI2cBusInterface.h"
#include "PecDevice.h"

#include "pec_Strings.h"

#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class PecI2cDeviceInterface : public PecDevice
{

public:

	//	connect()	GoToSiSy:d:1062|o:1079
	void connect(PecI2cBusInterface& bus, uint8_t i2cSlaveAddr);

	//	init()	GoToSiSy:d:1062|o:1080
	virtual void init();

	//	onStart()	GoToSiSy:d:1062|o:1081
	void onStart();

	//	setAddress()	GoToSiSy:d:1062|o:1082
	void setAddress(uint8_t address);
	//	Konstruktor	GoToSiSy:d:1062|o:1076
	PecI2cDeviceInterface();

	//	Destruktor	GoToSiSy:d:1062|o:1076
	~PecI2cDeviceInterface();

private:

protected:
	uint8_t addr;
	PecI2cBusInterface* pBus;


};

#endif
