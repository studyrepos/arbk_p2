//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1183</ObjektNummer>	GoToSiSy:d:1182

/////////////////////////////
//
//	Klasse: VolumesManager
//
//	driveNr = Physical drive nmuber (0..)
//
/////////////////////////////
#define GeneratedBySisy
#define cpp_VolumesManager
#define SISY_CLASS_NAME VolumesManager
#include "VolumesManager.h"

#include "Controller.h"
#include "VolumesManager.h"


#ifdef __AVR__
#include <stdio.h>
#include <stdlib.h> 

#include "ff.c"
#include "ff_syscall.c"
// #include "ccsbcs.c"
#endif // __AVR__
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1182|o:1183
//
/////////////////////////////
VolumesManager::VolumesManager()
{
	for(uint8_t i=0; i<_VOLUMES; i++)
		volumes[i]=0;
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1182|o:1183
//
/////////////////////////////
VolumesManager::~VolumesManager()
{
	
}
/*/////////////////////////////
//
//	mount()	GoToSiSy:d:1182|o:1190
//
/////////////////////////////
*/
int8_t VolumesManager::mount(Volume& vol, int8_t volNr)
{
	// n�chstes freies finden
	if(volNr == -1)
	{
		for(uint8_t i=0; i<_VOLUMES; i++)
		{
			if(volumes[i] == 0)
			{
				volNr=i;
				break;
			}
		}
	}
	
	if(volNr >=0 && volNr <_VOLUMES)
	{
		volumes[volNr] = &vol;
		FRESULT res = f_mount(&(vol.fs), "", 0);
		if(res != FR_OK)
			volNr = -1;
	}
	else	// Fehler, ung�ltige Nummer oder kein Platz
		volNr = -1;
	
return volNr;
} 

/*/////////////////////////////
//
//	 disk_initialize()	GoToSiSy:d:1182|o:1184
//
/////////////////////////////
*/
extern "C" DSTATUS  disk_initialize(BYTE driveNr)
{
	/* Disk Status Bits (DSTATUS) */
	// 
	// #define STA_NOINIT		0x01	/* Drive not initialized */
	// #define STA_NODISK		0x02	/* No medium in the drive */
	// #define STA_PROTECT		0x04	/* Write protected */
	
	DSTATUS status = 0x07;
	if(driveNr >=0  && driveNr < _VOLUMES)
		status = gVolumesManager.volumes[driveNr]->disk_initialize();
	
return status;
} 
/*/////////////////////////////
//
//	 disk_status()	GoToSiSy:d:1182|o:1185
//
/////////////////////////////
*/
extern "C" DSTATUS  disk_status(BYTE driveNr)
{
	/* Disk Status Bits (DSTATUS) */
	// 
	// #define STA_NOINIT		0x01	/* Drive not initialized */
	// #define STA_NODISK		0x02	/* No medium in the drive */
	// #define STA_PROTECT		0x04	/* Write protected */
	
	DSTATUS status = 0x07;
	if(driveNr >=0  && driveNr < _VOLUMES)
		status = gVolumesManager.volumes[driveNr]->disk_status();
	
return status;
} 
/*/////////////////////////////
//
//	 disk_read()	GoToSiSy:d:1182|o:1186
//
//	PE:
//		pBuffer = Data buffer to store read data
//		sectorNr = Sector address (LBA)
//		sectorCount = Number of sectors to read (1..255)
//
/////////////////////////////
*/
extern "C" DRESULT  disk_read(BYTE driveNr, BYTE* pBuffer, DWORD sectorNr, BYTE sectorCount)
{
	DRESULT result = RES_NOTRDY;
	if(driveNr >=0  && driveNr < _VOLUMES)
		result = gVolumesManager.volumes[driveNr]->disk_read(pBuffer, sectorNr, sectorCount);
	
return result;
} 
/*/////////////////////////////
//
//	 disk_write()	GoToSiSy:d:1182|o:1187
//
//	PE:
//		pBuffer = Data to be written
//		sectorNr = Sector address (LBA)
//		sectorCount = Number of sectors to read (1..255)
//
/////////////////////////////
*/
extern "C" DRESULT  disk_write(BYTE driveNr, const BYTE* pBuffer, DWORD sectorNr, BYTE sectorCount)
{
	DRESULT result = RES_NOTRDY;
	if(driveNr >=0  && driveNr < _VOLUMES)
		result = gVolumesManager.volumes[driveNr]->disk_write(pBuffer, sectorNr, sectorCount);
	
return result;
} 
/*/////////////////////////////
//
//	 disk_ioctl()	GoToSiSy:d:1182|o:1188
//
/////////////////////////////
*/
extern "C" DRESULT  disk_ioctl(BYTE driveNr, BYTE ctrlCode, void* pData)
{
	DRESULT result = RES_NOTRDY;
	if(driveNr >=0  && driveNr < _VOLUMES)
		result = gVolumesManager.volumes[driveNr]->disk_ioctl(ctrlCode, pData);
	
return result;
} 
/*/////////////////////////////
//
//	 get_fattime()	GoToSiSy:d:1182|o:1191
//
/////////////////////////////
*/
extern "C" DWORD  get_fattime()
{
	DWORD fattime = 1;
	
	/*
	struct _FatTime {
		u32 second	: 5;
		u32 minute	: 6;
		u32 hour	: 5;
		u32 day		: 5;
		u32 month	: 4;
		u32 year	: 7;	
	}
	
	_FatTime& t = reinterpret_cast<_FatTime&>(fattime);
	t.second = 0;
	t.minute = 0;
	*/
	
return fattime;
} 

