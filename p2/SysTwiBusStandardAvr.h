//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>2432</ObjektNummer>	GoToSiSy:d:2431

#if !defined(h_SysTwiBusStandardAvr)
#define h_SysTwiBusStandardAvr

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include <avr\io.h>
#include <avr\interrupt.h>
#include "PecI2cBusInterface.h"

#include "avr_Commons.h"
#include "PinList_avr.h"
#include "adcChannel_avr.h"

#define I2CBUS_INSTANCE_1
#ifndef I2CBUS_INSTANCE_1
#if !defined(I2CBUS_INSTANCE)
#define I2CBUS_INSTANCE 
#endif
#ifndef I2CBUS_INSTANCE_COUNT
#define I2CBUS_INSTANCE_COUNT 1
#else
#undef I2CBUS_INSTANCE_COUNT
#define I2CBUS_INSTANCE_COUNT 2
#endif
#endif
#undef I2CBUS_INSTANCE_1
#define _TWINT 0b10000000
#define _TWEA 0b01000000
#define _TWSTA 0b00100000
#define _TWSTO 0b00010000
#define _TWWC 0b00001000
#define _TWEN 0b00000100
#define _TWIE 0b00000001
#ifndef TWI_CLOCK
#define TWI_CLOCK 200000
#endif
#define I2CBUS_STANDARD SysTwiBusStandardAvr
#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class SysTwiBusStandardAvr : public PecI2cBusInterface
{

public:

	//	connect()	GoToSiSy:d:2431|o:2432
	void connect(PecI2cDeviceInterface& device, uint8_t i2cSlaveAddr);

	//	initI2c()	GoToSiSy:d:2431|o:2432
	virtual void initI2c();

	//	startSlaveWrite()	GoToSiSy:d:2431|o:2432
	bool startSlaveWrite(uint8_t slaveI2cAddr);

	//	startSlaveRead()	GoToSiSy:d:2431|o:2432
	bool startSlaveRead(uint8_t slaveI2cAddr);

	//	generateStop()	GoToSiSy:d:2431|o:2432
	void generateStop();

	//	writeByte()	GoToSiSy:d:2431|o:2432
	bool writeByte(uint8_t data);

	//	writeByteAck()	GoToSiSy:d:2431|o:2432
	bool writeByteAck(uint8_t data);

	//	readByte()	GoToSiSy:d:2431|o:2432
	uint8_t readByte();

	//	readByteAck()	GoToSiSy:d:2431|o:2432
	uint8_t readByteAck();

	//	Konstruktor	GoToSiSy:d:2431|o:2432
	SysTwiBusStandardAvr();

	//	Destruktor	GoToSiSy:d:2431|o:2432
	~SysTwiBusStandardAvr();

private:

protected:

	//	generateStart()	GoToSiSy:d:2431|o:2432
	bool generateStart();

	//	lConfigureBitRate()	GoToSiSy:d:2431|o:2432
	void lConfigureBitRate(uint32_t& divValue, uint32_t& stepValue);

	//	waitForReady()	GoToSiSy:d:2431|o:2432
	//	Warten auf TWI ‹bertragung fertig.
	//	
	//	
	//	PA:
	//		errorcode='T' bei timeout, sonst errorcode=0
	bool waitForReady();

	//	startSlave()	GoToSiSy:d:2431|o:2432
	//	PE:
	//		slaveI2CAdr=Slave-Adresse in den Bits 7..1
	//		Bit0 = TWI_READ|TWI_WRITE
	bool startSlave(uint8 slaveI2CAdrAndRW);


};

#endif
