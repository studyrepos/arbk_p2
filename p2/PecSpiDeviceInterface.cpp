//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>413</ObjektNummer>	GoToSiSy:d:388

#define GeneratedBySisy
#define cpp_PecSpiDeviceInterface
#define SISY_CLASS_NAME PecSpiDeviceInterface
#include "PecSpiDeviceInterface.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:388|o:413
//
/////////////////////////////
PecSpiDeviceInterface::PecSpiDeviceInterface()
{
pBus = NULL;
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:388|o:413
//
/////////////////////////////
PecSpiDeviceInterface::~PecSpiDeviceInterface()
{
	
}
/*/////////////////////////////
//
//	connect()	GoToSiSy:d:388|o:415
//
/////////////////////////////
*/
void PecSpiDeviceInterface::connect(PecSpiMasterInterface& bus)
{
	pBus=&bus;
	

} 

