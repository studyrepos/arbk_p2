//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>81</ObjektNummer>	GoToSiSy:d:80

#if !defined(h_PecAppModul)
#define h_PecAppModul

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "PecEventHandler.h"

class PecAppModul;
extern PecAppModul* pFirstPecAppModul;

#include <avr\io.h>
#include <stdlib.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class PecAppModul : public PecEventHandler
{

public:

	//	onPower()	GoToSiSy:d:80|o:82
	virtual void onPower();

	//	onStart()	GoToSiSy:d:80|o:83
	virtual void onStart();

	//	onWork()	GoToSiSy:d:80|o:84
	virtual void onWork();

	//	onTimer10ms()	GoToSiSy:d:80|o:85
	virtual void onTimer10ms();

	//	onEvent100ms()	GoToSiSy:d:80|o:86
	virtual void onEvent100ms();

	//	onEvent1s()	GoToSiSy:d:80|o:87
	virtual void onEvent1s();

	//	llInit()	GoToSiSy:d:80|o:81
	void llInit();

	//	llDeinit()	GoToSiSy:d:80|o:81
	void llDeinit();
	PecAppModul* pNextObject;
	//	Konstruktor	GoToSiSy:d:80|o:81
	PecAppModul();

	//	Destruktor	GoToSiSy:d:80|o:81
	~PecAppModul();

private:

protected:


};

#endif
