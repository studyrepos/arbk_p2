//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1192</ObjektNummer>	GoToSiSy:d:1182

#if !defined(h_Volume)
#define h_Volume

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "ff.h"
#include "pec_Strings.h"

#include "diskio.h"
#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class Volume
{

public:

	//	disk_initialize()	GoToSiSy:d:1182|o:1193
	virtual DSTATUS disk_initialize();

	//	disk_status()	GoToSiSy:d:1182|o:1194
	virtual DSTATUS disk_status();

	//	disk_read()	GoToSiSy:d:1182|o:1195
	virtual DRESULT disk_read(uint8_t* pBuffer, uint32_t sectorNr, uint8_t sectorCount);

	//	disk_write()	GoToSiSy:d:1182|o:1196
	virtual DRESULT disk_write(const uint8_t* pBuffer, uint32_t sectorNr, uint8_t sectorCount);

	//	disk_ioctl()	GoToSiSy:d:1182|o:1197
	virtual DRESULT disk_ioctl(uint8_t ctrlCode, void* pData);

	//	mount()	GoToSiSy:d:1182|o:1199
	int8_t mount(int8_t volNr=-1);
	FATFS fs;
	//	Konstruktor	GoToSiSy:d:1182|o:1192
	Volume();

	//	Destruktor	GoToSiSy:d:1182|o:1192
	~Volume();

private:

protected:


};

#endif
