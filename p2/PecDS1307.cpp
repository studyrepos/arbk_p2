//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1093</ObjektNummer>	GoToSiSy:d:1062

/////////////////////////////
//
//	Klasse: PecDS1307
//
//	Klasse f�r das TWI Add-On myTWI Echtzeituhr.
//	BSP:Verwendung
//		PecDS1307 rtc;
//		...
//		String t = rtc.getTime("%d.%M.%Y %h:%m:%s");
//	
//	Platzhalter:
//		%Y	Jahr vierstellig (es ist nur 20xx m�glich, nicht 19xx)
//		%y	Jahr zweistellig
//		%M	Monat als  Zahl
//		%d	Tag  als Zahl
//		%w	Wochentag als Text (Mo,....,So)
//		%h	Stunden
//		%m	Minuten
//		%s	Sekunden
//		%a	am/pm wenn die Uhr im 12- Stundenformat l�uft
//
/////////////////////////////
#define GeneratedBySisy
#define cpp_PecDS1307
#define SISY_CLASS_NAME PecDS1307
#include "PecDS1307.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1062|o:1093
//
/////////////////////////////
PecDS1307::PecDS1307()
{
	addr=0xD0;
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1062|o:1093
//
/////////////////////////////
PecDS1307::~PecDS1307()
{
	
}
/*/////////////////////////////
//
//	formatTimeToString()	GoToSiSy:d:1062|o:1094
//
//	Gibt aktuelles Datum und Uhrzeit zur�ck.
//	Das Format der R�ckgabe kann mittels einer Maske formatiert und individuell angepasst werden.
//	
//	PE:
//		time = Zeiger auf die Ergebniszeichenkette
//		mask = Zeiger auf die Zeichenkette mit der Maske f�r das Ergebnis
//		BSP:
//			Datum = Montag der 2009.04.20, Zeit  = 16:30:00 
//			mask  = "Am %d.%m.%Y um %h:%i:%s Uhr."   
//			ergibt: "Am 20.04.2009 um 16:30:00 Uhr."
//		
//		Platzhalter:
//			%Y	Jahr vierstellig (es ist nur 20xx m�glich, nicht 19xx)
//			%y	Jahr zweistellig
//			%M	Monat als  Zahl
//			%d	Tag  als Zahl
//			%w	Wochentag als Text (Mo,....,So)
//			%h	Stunden
//			%m	Minuten
//			%s	Sekunden
//			%a	am/pm wenn die Uhr im 12- Stundenformat l�uft
//
/////////////////////////////
*/
void PecDS1307::formatTimeToString(String& time, const char* mask)
{
	time.clear();
	
	read();
	
	////////////////////////////////////////////////////////
	// Formatieren                                        //
	////////////////////////////////////////////////////////
	bool isFlag=false; // n�chstes Zeichen ist Platzhalter
	uint8_t wert;
	char w1,w2;
	char c;
	while( (c=*mask)!=0)
	{
		mask++;
		if(!isFlag)
		{
			if(c=='%')
				isFlag=true;
			// normale Zeichen
			else
				time += c;
		}
		// isFlag
		else
		{
			isFlag=false;
			w1=w2=0;
			wert=255;
			// Wochentag
			if(c=='w')
			{
				if(weekday==1)
				{
					w1='M';
					w2='o';
				}
				else if(weekday==2)
				{
					w1='D';
					w2='i';
				}
				else if(weekday==3)
				{
					w1='M';
					w2='i';
				}	
				else if(weekday==4)
				{
					w1='D';
					w2='o';
				}
				else if(weekday==5)
				{
					w1='F';
					w2='r';
				}	
				else if(weekday==6)
				{
					w1='S';
					w2='a';
				}
				else if(weekday==7)
				{
					w1='S';
					w2='o';
				}
			}				
			// am/pm einf�gen
			else if(c=='a')
			{
				if(hours&0x40)
				{
					// pm
					if(hours&0x20)
						w1= 'p';
					// am
					else
						w1= 'a';
					w2='m';
				}
			}		
			// ersetze zahlen einf�gen
			else
			{
				// Sekunden
				if(c=='s')
					wert=seconds;
				// Minuten
				else if(c=='m')
					wert=minutes;
				// Stunden
				else if(c=='h')
				{
				    // wenn 12 Stunden Format
					if(hours&0x40)
						wert=(hours&0x1F);
					// wenn 24 Stunden Format
					else
						wert=hours;
				}
				// Tag
				else if(c=='d')
					wert=day;
				// Monat
				else if(c=='M')
					wert=month;
				// Jahr (zweistellig)
				else if(c=='y')
					wert=year;
				// Jahr (vierstellig)
				else if(c=='Y')
				{
					time += "20";
					wert=year;
				}
				///// % oder unbekannt
				else
				{
					w1=c;
				}
			}
			///// anf�gen
			if(wert!=255)
			{
				w1 = '0' + wert/10;
				w2 = '0' + wert%10;	
			}			
			if(w1!=0)
				time += w1;
			if(w2!=0)
				time += w2;
		}	
	}
	
	

} 
/*/////////////////////////////
//
//	startClock()	GoToSiSy:d:1062|o:1095
//
//	Startet die Uhr wieder falls sie angehalten war.
//
/////////////////////////////
*/
void PecDS1307::startClock()
{
	read();
	seconds &= ~0x80;
	write();
	

} 
/*/////////////////////////////
//
//	stopClock()	GoToSiSy:d:1062|o:1096
//
//	H�lt die Uhr an.
//
/////////////////////////////
*/
void PecDS1307::stopClock()
{
	read();
	seconds |= 0x80;
	write();
	

} 
/*/////////////////////////////
//
//	getTime()	GoToSiSy:d:1062|o:1097
//
//	Gibt aktuelles Datum und Uhrzeit zur�ck.
//	Das Format der R�ckgabe kann mittels einer Maske formatiert und individuell angepasst werden.
//	
//	PE:
//		time = Zeiger auf die Ergebniszeichenkette
//		mask = Zeiger auf die Zeichenkette mit der Maske f�r das Ergebnis
//		BSP:
//			Datum = Montag der 2009.04.20, Zeit  = 16:30:00 
//			mask  = "Am %d.%m.%Y um %h:%i:%s Uhr."
//			ergibt: "Am 20.04.2009 um 16:30:00 Uhr."
//		
//		Platzhalter:
//			%Y	Jahr vierstellig (es ist nur 20xx m�glich, nicht 19xx)
//			%y	Jahr zweistellig
//			%m	Monat als  Zahl
//			%d	Tag  als Zahl
//			%w	Wochentag als Text (Mo,....,So)
//			%h	Stunden
//			%i	Minuten
//			%s	Sekunden
//			%a	am/pm wenn die Uhr im 12- Stundenformat l�uft
//
/////////////////////////////
*/
bool PecDS1307::getTime(char* time, char* mask)
{
	if(pBus == NULL)
		return false;
	
	// Adressierung
	// TWI starten
	bool ok = false;
	if(pBus->startSlaveWrite(addr))
	{
		// Register adressieren
		ok = pBus->writeByte(REG_RTC_SECONDS); 
	}
	
	uint8_t seconds = 0;
	uint8_t minutes = 0;
	uint8_t hours   = 0;
	uint8_t weekday = 0;
	uint8_t day     = 0;
	uint8_t month   = 0;
	uint8_t year    = 0;
	
	// LeseModus
	if(pBus->startSlaveRead(addr))
	{
		// Daten holen
		if(ok)
		{
			seconds = pBus->readByteAck();
			minutes = pBus->readByteAck();
			hours   = pBus->readByteAck();
			weekday = pBus->readByteAck();
			day     = pBus->readByteAck();
			month   = pBus->readByteAck();
			year    = pBus->readByte();
		}
	}
	
	// TWI stop
	pBus->generateStop();
	
	////////////////////////////////////////////////////////
	// Formatieren                                        //
	////////////////////////////////////////////////////////
	if(ok)
	{
		bool isFlag=false; // n�chstes Zeichen ist Platzhalter
		bool go=true;
		uint8_t wert;
		char wd[2];
		char c;
		while( (c=*mask)!=0)
		{
			if(c=='%')
				isFlag=true;
			else if(!isFlag)
			{
				*time=c;
				time++;
			}
			// Wochentag
			else if(c=='w')
			{
				if(weekday==1)
				{
					wd[0]='M';
					wd[1]='o';
				}
				else if(weekday==2)
				{
					wd[0]='D';
					wd[1]='i';
				}
				else if(weekday==3)
				{
					wd[0]='M';
					wd[1]='i';
				}	
				else if(weekday==4)
				{
					wd[0]='D';
					wd[1]='o';
				}
				else if(weekday==5)
				{
					wd[0]='F';
					wd[1]='r';
				}	
				else if(weekday==6)
				{
					wd[0]='S';
					wd[1]='a';
				}
				else if(weekday==7)
				{
					wd[0]='S';
					wd[1]='o';
				}
				*time=wd[0];
				time++;
				*time=wd[1];
				time++;
				isFlag=false;
			}				
			// am/pm einf�gen
			else if(c=='a')
			{
				if(hours&0x40)
				{
					// pm
					if(hours&0x20)
						*time='p';
					// am
					else
						*time='a';
					time++;
					isFlag=false;
				}
				else
				{
					
				}
			}		
			// ersetze zahlen einf�gen
			else
			{
				// Sekunden
				if(c=='s')
					wert=seconds&0x7f;
				// Minuten
				else if(c=='i')
					wert=minutes;
				// Stunden
				else if(c=='h')
				{
				    // wenn 12 Stunden Format
					if(hours&0x40)
						wert=(hours&0x1F);
					// wenn 24 Stunden Format
					else
						wert=hours;
				}
				// Tag
				else if(c=='d')
					wert=day;
				// Monat
				else if(c=='m')
					wert=month;
				// Jahr (zweistellig)
				else if(c=='y')
					wert=year;
				// Jahr (vierstellig)
				else if(c=='Y')
				{
					*time='2';
					time++;
					*time='0';
					time++;
					wert=year;
				}
				else
				{
					*time='?';
					time++;
					*time=c;
					time++;
					go=false;
				}
				if(go)
				{
					// Zehnerstelle
					*time=(0x30+(wert>>4));	
					time++;
					// Einerstelle
					*time=(0x30+(wert&0x0F));
					time++;
				}
				isFlag=false;
				go=true;
			}
			mask++;
		}
		*time=0;
	}
	
return ok;
} 
/*/////////////////////////////
//
//	setTime()	GoToSiSy:d:1062|o:1098
//
//	Setzt die Uhrzeit.
//	BSP:
//		165308  oder 16:53:08   = 16 Uhr 53 min 08 sek (24-Stunden-Modus)
//		040000a oder 04:00:00:a =  4 Uhr 0 min 0 sek (12-Stunden-Modus)
//		040000p oder 04:00:00:p = 16 Uhr 0 min 0 sek (12-Stunden-Modus)
//		163050  oder 16-30-50   = 16 Uhr 30 min 50 sek (24-Stunden-Modus)
//	Format:
//		Stunden zweistellig, 
//		Minuten zweistellig, 
//		Sekunden zweistellig, 
//		wenn 12-Stunden Format: ein 'p' f�r pm oder ein 'a' f�r am. 
//	Die einzelnen Angaben k�nnen durch ein Trennzeichen abgetrennt werden, g�ltige Zeichen sind :, /, -.
//	
//	PE:
//		time = Zeichenkette mit der Uhrzeit die einstellt werden soll.
//		in24h = ob die Zeit als 24 oder 12 Stundenformat angegeben ist, 24-Stundenformat ist Standard.
//
/////////////////////////////
*/
void PecDS1307::setTime(const char* time)
{
	read();
	
	convertTime(time);
	// Zeit setzen
	write();
	

} 
/*/////////////////////////////
//
//	setDate()	GoToSiSy:d:1062|o:1099
//
//	Setzt das Datum.
//	BSP:
//		0904201 oder 09-04-20-1 = Mo der 20.04.2009
//	Format:
//		Jahr zweistellig, 
//		Monat zweistellig, 
//		Tag zweistellig und 
//		Wochentag als Zahl (1 f�r Montag, 7 f�r Sonntag). 
//	Die einzelnen Angaben k�nnen durch ein Trennzeichen abgetrennt werden, g�ltige Zeichen sind ., / und -.
//	
//	PE:
//		date = Die Zeichenkette mit dem Datum welches einstellt werden soll.
//
/////////////////////////////
*/
void PecDS1307::setDate(const char* date)
{
	read();
	convertDate(date);
	write();
	

} 
/*/////////////////////////////
//
//	convertTime()	GoToSiSy:d:1062|o:1100
//
//	L�dt die Uhrzeit in die Attribute.
//	BSP:
//		165308  oder 16:53:08   = 16 Uhr 53 min 08 sek (24-Stunden-Modus)
//		040000a oder 04:00:00:a =  4 Uhr 0 min 0 sek (12-Stunden-Modus)
//		040000p oder 04:00:00:p = 16 Uhr 0 min 0 sek (12-Stunden-Modus)
//		163050  oder 16-30-50   = 16 Uhr 30 min 50 sek (24-Stunden-Modus)
//	Format:
//		Stunden zweistellig, 
//		Minuten zweistellig, 
//		Sekunden zweistellig, 
//		wenn 12-Stunden Format: ein 'p' f�r pm oder ein 'a' f�r am. 
//	Die einzelnen Angaben k�nnen durch ein Trennzeichen abgetrennt werden, g�ltige Zeichen sind :, /, -.
//	
//	PE:
//		time = Zeichenkette mit der Uhrzeit die einstellt werden soll.
//		in24h = ob die Zeit als 24 oder 12 Stundenformat angegeben ist, 24-Stundenformat ist Standard.
//
/////////////////////////////
*/
void PecDS1307::convertTime(const char* time)
{
	uint8_t x=0;
	
	// Stunden
	hours=((time[x]-0x30)*10);
	x++;
	hours+=(time[x]-0x30);
	x++;
	// eventuelles Trenzeichen �berschringen
	if( !(time[x]>='0'&&time[x]<='9') )
		x++;
	
	// Minuten
	minutes=((time[x]-0x30)*10);
	x++;
	minutes+=(time[x]-0x30);
	x++;
	// eventuelles Trenzeichen �berschringen
	if( !(time[x]>='0'&&time[x]<='9') )
		x++;
		
	// Sekunden
	seconds=((time[x]-0x30)*10);
	x++;
	seconds+=(time[x]-0x30);
	x++;
	// eventuelles Trenzeichen �berschringen
	if( !(time[x]>='0'&&time[x]<='9') )
		x++;
	
	// wenn 12 Stunden Anzeige
	if(time[x]=='p')
	{
		hours+=40;		// eigentlich 0x40
		hours+=20;		// eigentlich 0x20
	}
	else if(time[x]=='a')
		hours+=40;		// eigentlich 0x40
	

} 
/*/////////////////////////////
//
//	convertDate()	GoToSiSy:d:1062|o:1101
//
//	L�dt das Datum in die Attribute.
//	BSP:
//		0904201 oder 09-04-20-1 = Mo der 20.04.2009
//	Format: 
//		Jahr zweistellig, 
//		Monat zweistellig, 
//		Tag zweistellig und 
//		Wochentag als Zahl (1 f�r Montag, 7 f�r Sonntag). 
//	Die einzelnen Angaben k�nnen durch ein Trennzeichen abgetrennt werden, g�ltige Zeichen sind ., / und -.
//	
//	PE:
//		date = Die Zeichenkette mit dem Datum welches einstellt werden soll.
//
/////////////////////////////
*/
void PecDS1307::convertDate(const char* date)
{
	uint8_t x=0;
	
	// Jahr
	year=((date[x]-0x30)*10);
	x++;
	year+=(date[x]-0x30);
	x++;
	// eventuelles Trenzeichen �berschringen
	if( !(date[x]>='0'&&date[x]<='9') )
		x++;
		
	// Monat
	month=((date[x]-0x30)*10);
	x++;
	month+=(date[x]-0x30);
	x++;
	// eventuelles Trenzeichen �berschringen
	if( !(date[x]>='0'&&date[x]<='9') )
		x++;
		
	// Tag
	day=((date[x]-0x30)*10);
	x++;
	day+=(date[x]-0x30);
	x++;
	
	// eventuelles Trenzeichen �berschringen
	if( !(date[x]>='0'&&date[x]<='9') )
		x++;
		
	// Wochentag
	weekday=(date[x]-0x30);
	

} 
/*/////////////////////////////
//
//	getTime()	GoToSiSy:d:1062|o:1102
//
//	Gibt aktuelles Datum und Uhrzeit zur�ck.
//	Das Format der R�ckgabe kann mittels einer Maske formatiert und individuell angepasst werden.
//	
//	PE:
//		time = Zeiger auf die Ergebniszeichenkette
//		mask = Zeiger auf die Zeichenkette mit der Maske f�r das Ergebnis
//		BSP:
//			Datum = Montag der 2009.04.20, Zeit  = 16:30:00 
//			mask  = "Am %d.%m.%Y um %h:%i:%s Uhr."
//			ergibt: "Am 20.04.2009 um 16:30:00 Uhr."
//		
//		Platzhalter:
//			%Y	Jahr vierstellig (es ist nur 20xx m�glich, nicht 19xx)
//			%y	Jahr zweistellig
//			%m	Monat als  Zahl
//			%d	Tag  als Zahl
//			%w	Wochentag als Text (Mo,....,So)
//			%h	Stunden
//			%i	Minuten
//			%s	Sekunden
//			%a	am/pm wenn die Uhr im 12- Stundenformat l�uft
//
/////////////////////////////
*/
String PecDS1307::getTime(const char* mask)
{
	String time;
	formatTimeToString(time,mask);
	
return time;
} 
/*/////////////////////////////
//
//	read()	GoToSiSy:d:1062|o:1110
//
//	Liest die aktuelle Zeit in die Klassenvariablen.
//
/////////////////////////////
*/
bool PecDS1307::read()
{
	if(pBus == NULL)
		return false;
	
	bool ok;
	ok = pBus->startSlaveWrite(addr);
	if(ok)
	{
		// Register adressieren
		pBus->writeByte(REG_RTC_SECONDS); 
		// LeseModus
		pBus->startSlaveRead(addr);
		// Daten holen
	//	seconds = bcd2num( (pBus->readByteAck() & 0x7F) );
		seconds = bcd2num(  pBus->readByteAck() );		// bit 7=1 -> angehalten
		minutes = bcd2num(  pBus->readByteAck() );
	//	hours   = bcd2num( (pBus->readByteAck() & 0x3F) );
		hours   = bcd2num(  pBus->readByteAck() );		// bit 6=1 -> 12h-Format (bit5=am/pm)
		weekday = bcd2num(  pBus->readByteAck() );
		day     = bcd2num(  pBus->readByteAck() );
		month   = bcd2num(  pBus->readByteAck() );
		year    = bcd2num(  pBus->readByte() );
	}
	pBus->generateStop();
	
	
return ok;
} 
/*/////////////////////////////
//
//	write()	GoToSiSy:d:1062|o:1111
//
//	Schreibt die Klassenvariablen.
//
/////////////////////////////
*/
bool PecDS1307::write()
{
	bool ok=false;
	// Adressierung
	// TWI starten
	if(pBus->startSlaveWrite(addr))
	{
		ok = true;	
		// Register adressieren
		pBus->writeByte(REG_RTC_SECONDS);
		// Schreiben
		// Sekunden setzen
		pBus->writeByte( num2bcd( seconds ));
		pBus->writeByte( num2bcd( minutes ));
		pBus->writeByte( num2bcd( hours ));
		pBus->writeByte( num2bcd( weekday ));
		pBus->writeByte( num2bcd( day ));
		pBus->writeByte( num2bcd( month ));
		pBus->writeByte( num2bcd( year ));
	}
	// TWI stop
	pBus->generateStop();
	
return ok;
} 
/*/////////////////////////////
//
//	bcd2num()	GoToSiSy:d:1062|o:1112
//
//	Wandelt eine 2stellige gepackte BCD-Zahl in einen Integer.
//
/////////////////////////////
*/
uint8_t PecDS1307::bcd2num(uint8_t value)
{
	uint8_t valueNum = ((value/16)*10) + (value&0x0F);
	
return valueNum;
} 
/*/////////////////////////////
//
//	num2bcd()	GoToSiSy:d:1062|o:1113
//
//	Wandelt eine Zahl in eine 2stelligen gepackte BCD-Zahl.
//
/////////////////////////////
*/
uint8_t PecDS1307::num2bcd(uint8_t value)
{
	uint8_t valueBcd = value/10;
	value = value- (valueBcd*10);
	valueBcd *=16;
	valueBcd += value;
	
return valueBcd;
} 

