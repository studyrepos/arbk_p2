//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>4613</ObjektNummer>	GoToSiSy:d:4612

#if !defined(h_Controller)
#define h_Controller

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include <avr\io.h>
#include <avr\interrupt.h>
#include "SwitchD2.h"
#include "LedD5.h"
#include "LedD6.h"
#include "LedD7.h"
#include "PecAppModul.h"
#include "PecEvent.h"
#include "EventDefs.h"

#include "Pec.h"
#include "MCU_ATmega_xxx.h"

#ifdef UsedTimer0
#error Controller: Timer0 wird mehrfach verwendet
#endif
#define UsedTimer0

#include <avr\io.h>
#include <stdlib.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class Controller : public PecAppModul
{

public:

	//	onStart()	GoToSiSy:d:4612|o:4614
	void onStart();

	//	onWork()	GoToSiSy:d:4612|o:4615
	void onWork();

	//	onSysTick()	GoToSiSy:d:4612|o:4613
	void onSysTick();

	//	postEvent()	GoToSiSy:d:4612|o:4613
	bool postEvent(uint8_t type, sysUint_t data = 0, const void* source = NULL);

	//	hasEvents()	GoToSiSy:d:4612|o:4613
	bool hasEvents();

	//	main()	GoToSiSy:d:4612|o:4613
	void main();

	//	config()	GoToSiSy:d:4612|o:4613
	//	PE:
	//		prescale = Vorteiler z.B. 1,2,4,8,...32768
	//		topValue = beliebiger Wert 0..65535
	void config(uint16_t prescale, uint16_t topValue);

	//	onTimer()	GoToSiSy:d:4612|o:4613
	virtual void onTimer();

	//	configHz()	GoToSiSy:d:4612|o:4613
	bool configHz(uint32_t frequency);

	//	configMs()	GoToSiSy:d:4612|o:4613
	bool configMs(uint32_t milliSeconds);

	//	configUs()	GoToSiSy:d:4612|o:4613
	bool configUs(uint32_t microSeconds);

	//	stop()	GoToSiSy:d:4612|o:4613
	void stop();

	//	Anzahl der Sekunden seit Systemstart
	uint32_t systimeSec;
	//	0...99 als Bruchteil einer Sekunde
	uint8_t systimeMsec10;
	SwitchD2 switchD2;
	LedD5 ledD5;
	LedD6 ledD6;
	LedD7 ledD7;
	PecEvent events[8];
	//	Konstruktor	GoToSiSy:d:4612|o:4613
	Controller();

	//	Destruktor	GoToSiSy:d:4612|o:4613
	~Controller();

private:

protected:

	//	workEvents()	GoToSiSy:d:4612|o:4613
	void workEvents();

	//	0...9 als Bruchteil einer Sekunde
	uint8_t systimeMsec100;
	sysUint_t firstEventPos;


};

#endif
