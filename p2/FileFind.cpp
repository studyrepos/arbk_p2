//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1229</ObjektNummer>	GoToSiSy:d:1182

#define GeneratedBySisy
#define cpp_FileFind
#define SISY_CLASS_NAME FileFind
#include "FileFind.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1182|o:1229
//
/////////////////////////////
FileFind::FileFind()
{
errorCode = FR_OK;
	#if _USE_LFN 
		fileInfo.lfname = (TCHAR*)lfnBuffer;
		fileInfo.lfsize = _MAX_LFN;
	#endif
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1182|o:1229
//
/////////////////////////////
FileFind::~FileFind()
{
	
}
/*/////////////////////////////
//
//	findFirst()	GoToSiSy:d:1182|o:1230
//
/////////////////////////////
*/
bool FileFind::findFirst(const String& filter)
{
	bool ok = false;
	
	StringSize_t pos = filter.findReverse('/');
	StringSize_t pos2 = filter.findReverse('\\');
	if((pos2 != (StringSize_t)-1) && (pos2 > pos))
		pos = pos2;
		
	if(pos != (StringSize_t)-1)
	{
		ok = true;
		this->path = filter.getPart(0, pos);
		this->filter = filter.getPart(pos+1);
		
		// Directory �ffnen
		errorCode = f_opendir(&dir, path);
		if(errorCode)
			ok = false;
		
		// Directory zur�cksetzen	???
		if(ok)
		{
			errorCode = f_readdir(&dir, NULL);
			if(errorCode)
				ok = false;
		}
		
		if(ok)
		{
			ok = findNext();
		}
	}
	
return ok;
} 
/*/////////////////////////////
//
//	findNext()	GoToSiSy:d:1182|o:1231
//
/////////////////////////////
*/
bool FileFind::findNext()
{
	bool found = false;
	do
	{
		errorCode = f_readdir(&dir, &fileInfo);
		if(errorCode)
			break;
		// End of Dir
		if( fileInfo.fname[0] == 0 )
		{
			break;
		}
		
		String name = getFileName();
		name.toUpperCase();
		StringSize_t pos = name.findReverse('.');
		String ext = name.getPart(pos+1);
		name=name.getPart(0,pos);
		pos = filter.findReverse('.');
		String fName = filter.getPart(0, pos);
		String fExt  = filter.getPart(pos+1);
		fName.toUpperCase();
		fExt.toUpperCase();
	
		// Filter
		uint8_t* ch1 = name.pBuffer;
		uint8_t* ch2 = fName.pBuffer;
		
		bool ok1 = true;
		bool inExt = false;
		
	// 			while(ok1 && !found && *ch1!=0 && *ch2!=0)
		while(ok1 && !found )
		{
			if( *ch1 == *ch2 || *ch2=='?' )
			{
				// Wenn beide 0 => gefunden 
				if(*ch1==0 && *ch2== 0)
				{
					if(inExt)
						found = true;
					else
					{
						ch1 =  ext.pBuffer;
						ch2 = fExt.pBuffer;
						inExt = true;
					}
				}	
				else
				{
					// ok, jeweils n�chster Buchstabe
					ch1++;
					ch2++;
				}
			}
			else if(*ch2 == '*')
			{
				if(inExt)
					found=true;
				else
				{
					ch1 =  ext.pBuffer;
					ch2 = fExt.pBuffer;
					inExt = true;
				}
			}
			else
			{
				// Zeichen verschieden => Abbruch
				ok1 = false;
			}
		}
	}
	while( !found);
	
return found;
} 
/*/////////////////////////////
//
//	getFileName()	GoToSiSy:d:1182|o:1237
//
/////////////////////////////
*/
String FileFind::getFileName()
{
	String fName;
	#if _USE_LFN 
		fName= fileInfo.lfname;
		if(fName.getSize() == 0)
	#endif
			fName = fileInfo.fname;
	
return fName;
} 
/*/////////////////////////////
//
//	isDirectory()	GoToSiSy:d:1182|o:1238
//
/////////////////////////////
*/
bool FileFind::isDirectory()
{
	bool isDir = false;
	if(fileInfo.fname[0]!=0 && fileInfo.fattrib & AM_DIR)
		isDir=true;
	
return isDir;
} 
/*/////////////////////////////
//
//	isReadonly()	GoToSiSy:d:1182|o:1239
//
/////////////////////////////
*/
bool FileFind::isReadonly()
{
	bool isRO = false;
	if(fileInfo.fname[0]!=0 && fileInfo.fattrib & AM_RDO)
		isRO=true;
	
return isRO;
} 
/*/////////////////////////////
//
//	isHidden()	GoToSiSy:d:1182|o:1240
//
/////////////////////////////
*/
bool FileFind::isHidden()
{
	bool isHid = false;
	if(fileInfo.fname[0]!=0 && fileInfo.fattrib & AM_HID)
		isHid=true;
	
return isHid;
} 
/*/////////////////////////////
//
//	isSystem()	GoToSiSy:d:1182|o:1242
//
/////////////////////////////
*/
bool FileFind::isSystem()
{
	bool isSys = false;
	if(fileInfo.fname[0]!=0 && fileInfo.fattrib & AM_SYS)
		isSys=true;
	
return isSys;
} 

