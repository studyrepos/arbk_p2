//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>4629</ObjektNummer>	GoToSiSy:d:4612

#if !defined(h_SwitchD2)
#define h_SwitchD2

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include <avr\io.h>
#include <avr\interrupt.h>
#include "PecAppModul.h"
#include "ButtonEventDef.h"

#include "Pec.h"
#include "MCU_ATmega_xxx.h"

#include <avr\io.h>
#include <stdlib.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class SwitchD2 : public PecAppModul
{

public:

	//	onClick()	GoToSiSy:d:4612|o:4642
	void onClick();

	//	isPressed()	GoToSiSy:d:4612|o:4629
	//	Ermittelt, ob der Button gedr�ckt ist.
	bool isPressed();

	//	waitForPress()	GoToSiSy:d:4612|o:4629
	//	Wartet bis die taste gedr�ckt wurde.
	void waitForPress();

	//	onWork()	GoToSiSy:d:4612|o:84
	virtual void onWork();

	//	getState()	GoToSiSy:d:4612|o:4629
	virtual bool getState();

	int clicked;

	//	Enum state
	enum{state_state_Nothing = 1,state_state_down,state_state_click,state_state_hold};

	//	changeState_state()
	void changeState_state(state_t newState);

	//	Konstruktor	GoToSiSy:d:4612|o:4629
	SwitchD2();

	//	Destruktor	GoToSiSy:d:4612|o:4629
	~SwitchD2();

private:

protected:

	//	onTimer10ms()	GoToSiSy:d:4612|o:4629
	virtual void onTimer10ms();

	//	onHoldStart()	GoToSiSy:d:4612|o:4629
	virtual void onHoldStart();

	//	onHoldEnd()	GoToSiSy:d:4612|o:4629
	virtual void onHoldEnd();

	//	onHolding()	GoToSiSy:d:4612|o:4629
	//	wird alle 10ms aufgerufen solange Hold aktiv ist
	virtual void onHolding();

	//	Zeit seit erstem Dr�cken
	uint8_t volatile holdCounter;
	//	Zeit seit erstem Dr�cken
	uint8_t volatile releaseCounter;

	state_t state;

};

#endif
