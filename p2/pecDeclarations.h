
#define BIT0 (1UL<<0)
#define BIT1 (1UL<<1)
#define BIT2 (1UL<<2)
#define BIT3 (1UL<<3)
#define BIT4 (1UL<<4)
#define BIT5 (1UL<<5)
#define BIT6 (1UL<<6)
#define BIT7 (1UL<<7)
#define BIT8 (1UL<<8)
#define BIT9 (1UL<<9)
#define BIT10 (1UL<<10)
#define BIT11 (1UL<<11)
#define BIT12 (1UL<<12)
#define BIT13 (1UL<<13)
#define BIT14 (1UL<<14)
#define BIT15 (1UL<<15)
#define BIT16 (1UL<<16)
#define BIT17 (1UL<<17)
#define BIT18 (1UL<<18)
#define BIT19 (1UL<<19)
#define BIT20 (1UL<<20)
#define BIT21 (1UL<<21)
#define BIT22 (1UL<<22)
#define BIT23 (1UL<<23)
#define BIT24 (1UL<<24)
#define BIT25 (1UL<<25)
#define BIT26 (1UL<<26)
#define BIT27 (1UL<<27)
#define BIT28 (1UL<<28)
#define BIT29 (1UL<<29)
#define BIT30 (1UL<<30)
#define BIT31 (1UL<<31)

//// Flash
#ifdef __AVR__
	#define FLASHSTR(x)  PSTR(x)
	#define WriteStringFlash writeStringFlash
	#define IsEqualString isEqualFlash
	#define AddContentFlash addContentFlash
	#define SetContentFlash setContentFlash
#else
	#define PROGMEM
	#define __LPM(x) (*x)
	#define FLASHSTR(x) x
	#define prog_uchar unsigned char
	#define pgm_read_byte(p) *((uint8_t*)p)
	#define pgm_read_word(p) *((uint16_t*)p)
	#define WriteStringFlash writeString
	#define IsEqualString isEqual
	#define AddContentFlash addContent
	#define SetContentFlash setContent
#endif


//// Clock
#ifdef F_CPU
	#define SystemCoreClockDef F_CPU
#endif

//// TemplateVars
#ifndef VARISNOTDEF
	#define VARISNOTDEF 1
#endif

//// Debug
#ifndef IsDEBUG
	#define DebugPrint(txt);
	#define DebugPrintFlash(txt);
	#define DebugPrintRam(txt,size);
	#define DebugPrintNoEnter(txt);
	#define DebugPrintHex(txt,num);
	#define DebugPrintHexNoEnter(txt,num);
	#define DebugPrintDez(txt,num);
	#define DebugPrintDezNoEnter(txt,num);
	#define DebugPrintString(txt1,txt2);
	#define DebugPrintDump(pBuffer,anz);
	#define DebugPrintAllocFreelist();
	#define DebugPrintIP(ip);
	#define DebugPrintIp(txt,ip);
	#define DebugPrintMac(txt,mac0,mac1,mac2,mac3,mac4,mac5);
	#define DebugPrintFlashNoEnter(txt);
		
	#define DebugPrintM(mask,txt) 					;
	#define DebugPrintFlashNoEnterM(mask,txt)		;
	#define DebugPrintNoEnterM(mask,txt)  			;
	#define DebugPrintRamM(mask,txt)				;
	#define DebugPrintHexM(mask,txt,num)			;
	#define DebugPrintHexNoEnterM(mask,txt,num)		;
	#define DebugPrintDezM(mask,txt,num)			;
	#define DebugPrintDezNoEnterM(mask,txt,num)		;
	#define DebugPrintStringM(mask,txt1,txt2)		;
	#define DebugPrintDumpM(mask,pBuffer,anz)		;
	#define DebugPrintDumpFlashM(mask,pBuffer,anz)	;
#endif
