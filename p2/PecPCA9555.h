//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1114</ObjektNummer>	GoToSiSy:d:1062

#if !defined(h_PecPCA9555)
#define h_PecPCA9555

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "PecI2cDeviceInterface.h"

#include "pec_Strings.h"

#define REG_PCA9555_INPUT_PORTA           0x00
#define REG_PCA9555_INPUT_PORTB           0x01
#define REG_PCA9555_OUTPUT_PORTA          0x02
#define REG_PCA9555_OUTPUT_PORTB          0x03
#define REG_PCA9555_INVERSEPOLARITY_PORTA 0x04
#define REG_PCA9555_INVERSEPOLARITY_PORTB 0x05
#define REG_PCA9555_CONFIG_PORTA          0x06
#define REG_PCA9555_CONFIG_PORTB          0x07
#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class PecPCA9555 : public PecI2cDeviceInterface
{

public:
	//	configPorts()	GoToSiSy:d:1062|o:1115
	//	Konfiguriert die beiden Ports des Portexpanders.
	//	Je Pin ein Bit, Null steht f�r Ausgang und Eins steht f�r Eingang.
	//	
	//	PE:
	//		confPortA = Eine 8bit-Zahl die A0-A7 konfiguriert.
	//		confPortB = Eine 8bit-Zahl die B0-B7 konfiguriert.
	bool configPorts(uint8_t confPortA, uint8_t confPortB);
	//	outPorts()	GoToSiSy:d:1062|o:1116
	//	Setzt den Ausgang des gew�nschten Ports auf den Wert von data.
	//	
	//	PE:
	//		port = Der Port den man nutzen m�chte. 0 f�r PortA oder 1 f�r PortB
	//		data = Eine 8bit-Zahl deren Bits je f�r ein Pin stehen. (Portx0-Portx7) Null = output-low, Eins = output-high
	bool outPorts(uint8_t port, uint8_t data);
	//	readPorts()	GoToSiSy:d:1062|o:1117
	//	Liest das Inputregister des gew�nschten Ports aus.
	//	
	//	PE:
	//		port = Der Port den man lesen m�chte. 0 f�r PortA oder 1 f�r PortB
	//		
	//	PA:
	//		Eine 8bit-Zahl deren Bits je f�r ein Pin stehen. (Portx0-Portx7)
	uint8_t readPorts(uint8_t port);
	//	Konstruktor	GoToSiSy:d:1062|o:1114
	PecPCA9555();

	//	Destruktor	GoToSiSy:d:1062|o:1114
	~PecPCA9555();

private:

protected:


};

#endif
