//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1229</ObjektNummer>	GoToSiSy:d:1182

#if !defined(h_FileFind)
#define h_FileFind

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "ff.h"
#include "File.h"

#include "pec_Strings.h"

#if !defined(_USE_LFN)
	#undef _MAX_LFN
	#define _MAX_LFN 1
#endif

#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class FileFind
{

public:

	//	findFirst()	GoToSiSy:d:1182|o:1230
	bool findFirst(const String& filter);

	//	findNext()	GoToSiSy:d:1182|o:1231
	bool findNext();

	//	getFileName()	GoToSiSy:d:1182|o:1237
	String getFileName();

	//	isDirectory()	GoToSiSy:d:1182|o:1238
	bool isDirectory();

	//	isReadonly()	GoToSiSy:d:1182|o:1239
	bool isReadonly();

	//	isHidden()	GoToSiSy:d:1182|o:1240
	bool isHidden();

	//	isSystem()	GoToSiSy:d:1182|o:1242
	bool isSystem();
	FRESULT errorCode;
	//	Konstruktor	GoToSiSy:d:1182|o:1229
	FileFind();

	//	Destruktor	GoToSiSy:d:1182|o:1229
	~FileFind();

private:

protected:
	DIR dir;
	String filter;
	TCHAR lfnBuffer[_MAX_LFN];
	FILINFO fileInfo;
	String path;


};

#endif
