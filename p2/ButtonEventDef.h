//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>198</ObjektNummer>	GoToSiSy:d:173

#if !defined(h_ButtonEventDef)
#define h_ButtonEventDef

#include "pec_Gpio.h"
#include "pec_AppManagement.h"

enum ButtonEventDef{OnClick=1,OnHoldStart=2,OnHolding=3,OnHoldEnd=4};

#endif
