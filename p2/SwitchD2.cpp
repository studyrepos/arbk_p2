//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>4629</ObjektNummer>	GoToSiSy:d:4612

#define GeneratedBySisy
#define cpp_SwitchD2
#define SISY_CLASS_NAME SwitchD2
#include "SwitchD2.h"

#include "Controller.h"
#include "VolumesManager.h"


#ifndef ButtonClickDebounce
#define ButtonClickDebounce 5
#endif
#ifndef ButtonHoldTime
#define ButtonHoldTime 100
#endif
#define PinInputLogic_0
#define PinInputLevel_1
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:4612|o:4629
//
/////////////////////////////
SwitchD2::SwitchD2()
{
holdCounter = 0;
releaseCounter = 0;
	//  aus Template: SysPinInputAvr
	#if VARISNOTDEFD
		#error Pin-Modifier fehlt an Klasse SwitchD2
	#else
		#ifndef PORTD
			#error unbekannter Port PORTD
		#endif
		#ifndef BIT2
			#error unbekannte BitNr BIT2
		#endif
	
		DDRD &= ~BIT2;
		#ifdef PinInputLevel_1		// PullUp
			PORTD |= BIT2;
		#endif
	#endif
	
	state=0;
	
	changeState_state(state_state_Nothing);
	
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:4612|o:4629
//
/////////////////////////////
SwitchD2::~SwitchD2()
{
	
	
}
/////////////////////////////
//
//	onClick()	GoToSiSy:d:4612|o:4642
//
/////////////////////////////
void SwitchD2::onClick()
{
	clicked = 1;

} 
/////////////////////////////
//
//	onTimer10ms()	GoToSiSy:d:4612|o:4629
//
/////////////////////////////
void SwitchD2::onTimer10ms()
{
	switch(state)
	{
		case state_state_Nothing:
		{
			// GoToSiSy:d:204|o:3618
			
			if(isPressed())
			{
				changeState_state(state_state_down);
				break;
			}
			break;
		}
		case state_state_down:
		{
			// GoToSiSy:d:204|o:217|zBase:1
			if (isPressed())
			{
				holdCounter++;
				releaseCounter=0;
			}
			else
			{
				holdCounter=0;
				releaseCounter++;
			}
			
			break;
		}
		case state_state_click:
		{
			// GoToSiSy:d:204|o:3620
			
			changeState_state(state_state_Nothing);
			break;
		}
		case state_state_hold:
		{
			// GoToSiSy:d:204|o:223|zBase:1
			onHolding();
			break;
		}
	}
	// GoToSiSy:d:4612|o:4629|zbase:44
		

} 
/////////////////////////////
//
//	onHoldStart()	GoToSiSy:d:4612|o:4629
//
/////////////////////////////
void SwitchD2::onHoldStart()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:209|zBase:1
	app.postEvent(PecSysEvent|OnHoldStart, 0, this);
	
	// GoToSiSy:d:4612|o:4629|zbase:4
		

} 
/////////////////////////////
//
//	onHoldEnd()	GoToSiSy:d:4612|o:4629
//
/////////////////////////////
void SwitchD2::onHoldEnd()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:210|zBase:1
	app.postEvent(PecSysEvent|OnHoldEnd, 0, this);
	
	// GoToSiSy:d:4612|o:4629|zbase:4
		

} 
/////////////////////////////
//
//	isPressed()	GoToSiSy:d:4612|o:4629
//
//	Ermittelt, ob der Button gedr�ckt ist.
//
/////////////////////////////
bool SwitchD2::isPressed()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:211|zBase:1
	bool pressed=getState();
	// #ifdef LogicLowActive
	// 	if (!LogicLowActive)
	// 		pressed = !pressed;
	// #endif
	
	// GoToSiSy:d:4612|o:4629|zbase:8
		
	return pressed;
} 
/////////////////////////////
//
//	onHolding()	GoToSiSy:d:4612|o:4629
//
//	wird alle 10ms aufgerufen solange Hold aktiv ist
//
/////////////////////////////
void SwitchD2::onHolding()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:212|zBase:1
	app.postEvent(PecSysEvent|OnHolding, 0, this);
	
	// GoToSiSy:d:4612|o:4629|zbase:4
		

} 
/////////////////////////////
//
//	waitForPress()	GoToSiSy:d:4612|o:4629
//
//	Wartet bis die taste gedr�ckt wurde.
//
/////////////////////////////
void SwitchD2::waitForPress()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:213|zBase:1
	while( !isPressed() )
		{}
		
	// GoToSiSy:d:4612|o:4629|zbase:5
		

} 
/////////////////////////////
//
//	onWork()	GoToSiSy:d:4612|o:84
//
/////////////////////////////
void SwitchD2::onWork()
{
	switch(state)
	{
		case state_state_down:
		{
			// GoToSiSy:d:204|o:3619
			
			if(releaseCounter>=ButtonClickDebounce)
			{
				changeState_state(state_state_click);
				break;
			}
			// GoToSiSy:d:204|o:3621
			
			if(holdCounter>=ButtonHoldTime )
			{
				changeState_state(state_state_hold);
				break;
			}
			break;
		}
		case state_state_hold:
		{
			// GoToSiSy:d:204|o:3622
			
			if(!isPressed())
			{
				changeState_state(state_state_Nothing);
				break;
			}
			break;
		}
	}
	// GoToSiSy:d:4612|o:84|zbase:33
		

} 
/////////////////////////////
//
//	getState()	GoToSiSy:d:4612|o:4629
//
/////////////////////////////
bool SwitchD2::getState()
{
	//  aus Template: SysPinInputAvr		GoToSiSy:o:1697|zBase:1
	#if defined(PinInputLogic_0)
		bool isOn = !(PIND & BIT2);
	#else //if defined(PinInputLogic_1)
		bool isOn = PIND & BIT2;
	#endif
	
	//  aus Template: PecPinInput		GoToSiSy:o:20|zBase:1
	
	
	// GoToSiSy:d:4612|o:4629|zbase:11
		
	return isOn;
} 

/////////////////////////////
//
//	changeState_state()
//
/////////////////////////////
void SwitchD2::changeState_state(state_t newState)
{
	switch(state)
	{
		case state_state_hold:
		{
			// GoToSiSy:o:222|zBase:1
			onHoldEnd();
			
			break;
		}
	}
	state = newState;
	switch(state)
	{
		case state_state_down:
		{
			// GoToSiSy:o:216|zBase:1
			releaseCounter=0;
			holdCounter=0;
			
			break;
		}
		case state_state_click:
		{
			// GoToSiSy:o:219|zBase:1
			onClick();
			
			break;
		}
		case state_state_hold:
		{
			// GoToSiSy:o:221|zBase:1
			onHoldStart();
			
			break;
		}
	}
}
