//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>37</ObjektNummer>	GoToSiSy:d:7

#define GeneratedBySisy
#define cpp_PortOutput
#define SISY_CLASS_NAME PortOutput
#include "PortOutput.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:7|o:37
//
/////////////////////////////
PortOutput::PortOutput()
{
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:7|o:37
//
/////////////////////////////
PortOutput::~PortOutput()
{
	
}
/*/////////////////////////////
//
//	setData()	GoToSiSy:d:7|o:37
//
/////////////////////////////
*/
void PortOutput::setData(portMask_t newData)
{
	//  aus Template: SysPortOutputAvr		GoToSiSy:o:1715|zBase:1
	*port = ((*port) & ~bitmask) | (newData & bitmask) ;			// PORTx
	
	// GoToSiSy:d:7|o:37|zbase:4
		

} 
/*/////////////////////////////
//
//	operator=()	GoToSiSy:d:7|o:37
//
/////////////////////////////
*/
portMask_t PortOutput::operator=(const portMask_t& newValue)
{
	//  aus Template: PecPortOutput		GoToSiSy:o:34|zBase:1
	setData( newValue );
	
	// GoToSiSy:d:7|o:37|zbase:4
		
return getData();
} 
/*/////////////////////////////
//
//	config()	GoToSiSy:d:7|o:37
//
/////////////////////////////
*/
void PortOutput::config(port_t port, pinMask_t bitmask)
{
	//  aus Template: SysPortOutputAvr		GoToSiSy:o:1713|zBase:1
	this->bitmask = bitmask;
	this->port	  = &port;
	
	// Ausgang
	*(this->port-1) |= bitmask;	// DDR Register
	
	// GoToSiSy:d:7|o:37|zbase:8
		

} 
/*/////////////////////////////
//
//	getData()	GoToSiSy:d:7|o:37
//
/////////////////////////////
*/
portMask_t PortOutput::getData()
{
	//  aus Template: SysPortOutputAvr		GoToSiSy:o:1714|zBase:1
	portMask_t data = *port & bitmask;	// PORTx Register = OutputData
	
	// GoToSiSy:d:7|o:37|zbase:4
		
return data;
} 

