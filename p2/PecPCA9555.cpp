//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1114</ObjektNummer>	GoToSiSy:d:1062

/////////////////////////////
//
//	Klasse: PecPCA9555
//
//	Klasse f�r das TWI Add-On myTWI Portexpander.
//	BSP:Verwendung
//		Twi_PCA9555 expander;
//		...
//		expander.configPorts( 0xFF, 0x00 ); 	// A=Eingang, B=Ausgang
//		uint8_t wert = expander.readPorts(0);	// von A lesen
//		expander.outPorts(1, wert);				// auf B schreiben
//
/////////////////////////////
#define GeneratedBySisy
#define cpp_PecPCA9555
#define SISY_CLASS_NAME PecPCA9555
#include "PecPCA9555.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1062|o:1114
//
/////////////////////////////
PecPCA9555::PecPCA9555()
{
	addr=0x40;
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1062|o:1114
//
/////////////////////////////
PecPCA9555::~PecPCA9555()
{
	
}
/*/////////////////////////////
//
//	configPorts()	GoToSiSy:d:1062|o:1115
//
//	Konfiguriert die beiden Ports des Portexpanders.
//	Je Pin ein Bit, Null steht f�r Ausgang und Eins steht f�r Eingang.
//	
//	PE:
//		confPortA = Eine 8bit-Zahl die A0-A7 konfiguriert.
//		confPortB = Eine 8bit-Zahl die B0-B7 konfiguriert.
//
/////////////////////////////
*/
bool PecPCA9555::configPorts(uint8_t confPortA, uint8_t confPortB)
{
	if(pBus == NULL)
		return false;
		
	// Adressierung
	bool ok = false;
	if(pBus->startSlaveWrite(addr))
	{
		ok = pBus->writeByte(REG_PCA9555_CONFIG_PORTA);
		// Konfigurationen schreiben 
		if(ok) 
			ok=pBus->writeByte(confPortA);                
		if(ok) 
			ok=pBus->writeByte(confPortB);                
	}
	pBus->generateStop();
	
return ok;
} 
/*/////////////////////////////
//
//	outPorts()	GoToSiSy:d:1062|o:1116
//
//	Setzt den Ausgang des gew�nschten Ports auf den Wert von data.
//	
//	PE:
//		port = Der Port den man nutzen m�chte. 0 f�r PortA oder 1 f�r PortB
//		data = Eine 8bit-Zahl deren Bits je f�r ein Pin stehen. (Portx0-Portx7) Null = output-low, Eins = output-high
//
/////////////////////////////
*/
bool PecPCA9555::outPorts(uint8_t port, uint8_t data)
{
	if(pBus == NULL)
		return false;
	
	// Adressierung
	bool ok=false;
	if(pBus->startSlaveWrite(addr))
	{
		ok = pBus->writeByte(REG_PCA9555_OUTPUT_PORTA+port);
		// Wert schreiben
		if(ok) 
			pBus->writeByte(data);
	}
	pBus->generateStop();
	
return ok;
} 
/*/////////////////////////////
//
//	readPorts()	GoToSiSy:d:1062|o:1117
//
//	Liest das Inputregister des gew�nschten Ports aus.
//	
//	PE:
//		port = Der Port den man lesen m�chte. 0 f�r PortA oder 1 f�r PortB
//		
//	PA:
//		Eine 8bit-Zahl deren Bits je f�r ein Pin stehen. (Portx0-Portx7)
//
/////////////////////////////
*/
uint8_t PecPCA9555::readPorts(uint8_t port)
{
	if(pBus == NULL)
		return 0;
	
	uint8_t data=0;
	bool ok = false;
	// Adressierung
	if(pBus->startSlaveWrite(addr))
	{
		ok = pBus->writeByte(REG_PCA9555_INPUT_PORTA+port);
	
	pBus->generateStop();
	
		// Lesen
		if(ok && pBus->startSlaveRead(addr))
		{
			data = pBus->readByte();
		}
		else
			ok = false;
	}
	pBus->generateStop();
	
return data;
} 

