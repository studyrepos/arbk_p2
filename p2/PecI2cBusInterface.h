//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1063</ObjektNummer>	GoToSiSy:d:1062

#if !defined(h_PecI2cBusInterface)
#define h_PecI2cBusInterface

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "pec_Strings.h"

#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class PecI2cBusInterface
{

public:

	//	startSlaveWrite()	GoToSiSy:d:1062|o:1064
	virtual bool startSlaveWrite(uint8_t slaveI2Caddr);

	//	startSlaveRead()	GoToSiSy:d:1062|o:1065
	virtual bool startSlaveRead(uint8_t slaveI2Caddr);

	//	generateStop()	GoToSiSy:d:1062|o:1067
	virtual void generateStop();

	//	writeByte()	GoToSiSy:d:1062|o:1068
	virtual bool writeByte(uint8_t data);

	//	writeByteAck()	GoToSiSy:d:1062|o:1069
	virtual bool writeByteAck(uint8_t data);

	//	readByte()	GoToSiSy:d:1062|o:1070
	virtual uint8_t readByte();

	//	readByteAck()	GoToSiSy:d:1062|o:1071
	virtual uint8_t readByteAck();
	//	Konstruktor	GoToSiSy:d:1062|o:1063
	PecI2cBusInterface();

	//	Destruktor	GoToSiSy:d:1062|o:1063
	~PecI2cBusInterface();

private:

protected:

	//	generateStart()	GoToSiSy:d:1062|o:1066
	virtual bool generateStart();


};

#endif
