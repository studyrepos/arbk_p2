//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>42</ObjektNummer>	GoToSiSy:d:7

#define GeneratedBySisy
#define cpp_PortInput
#define SISY_CLASS_NAME PortInput
#include "PortInput.h"

#include "Controller.h"
#include "VolumesManager.h"


#define PinInput_
#define PinInput_NoPull
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:7|o:42
//
/////////////////////////////
PortInput::PortInput()
{
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:7|o:42
//
/////////////////////////////
PortInput::~PortInput()
{
	
}
/*/////////////////////////////
//
//	config()	GoToSiSy:d:7|o:42
//
/////////////////////////////
*/
void PortInput::config(port_t port, pinMask_t bitmask)
{
	//  aus Template: SysPortInputOutputAvr		GoToSiSy:o:1699|zBase:1
	this->bitmask = bitmask; 
	this->port	  = &port;
	
	// Eingang
	*(this->port-1) &= ~bitmask;	// DDR Register
	
	// Pullup
	#ifdef PinInput_PullUp
		*(this->port) |= bitmask;	// PORTx Register
	#endif
	
	
	#ifdef PinInput_PullDown
		#warning PortInput::config(..) PullDown wird nicht unterstützt
	#endif
	
	// GoToSiSy:d:7|o:42|zbase:18
		

} 
/*/////////////////////////////
//
//	getData()	GoToSiSy:d:7|o:42
//
/////////////////////////////
*/
portMask_t PortInput::getData()
{
	//  aus Template: SysPortInputOutputAvr		GoToSiSy:o:1701|zBase:1
	portMask_t data = *(port-2) & bitmask;	// PIN Register
	
	// #ifdef PinInput_LowActive
	// 	data = (~data) & bitmask;
	// #endif
	
	//  aus Template: PecPortInput		GoToSiSy:o:40|zBase:1
	// portMask_t data;
	// GoToSiSy:d:7|o:42|zbase:10
		
return data;
} 
/*/////////////////////////////
//
//	setPullUp()	GoToSiSy:d:7|o:42
//
/////////////////////////////
*/
void PortInput::setPullUp(portMask_t mask)
{
	//  aus Template: SysPortInputOutputAvr		GoToSiSy:o:1705|zBase:1
	uint8_t p = *(this->port);
	p &= ~bitmask;
	p |= (mask & bitmask);
	*(this->port) = p; 
	
	//  aus Template: PecPortInput		GoToSiSy:o:41|zBase:1
	
	
	// GoToSiSy:d:7|o:42|zbase:10
		

} 
/*/////////////////////////////
//
//	setDirection()	GoToSiSy:d:7|o:42
//
/////////////////////////////
*/
void PortInput::setDirection(bool asOutput)
{
	//  aus Template: SysPortInputOutputAvr		GoToSiSy:o:1700|zBase:1
	if(asOutput)
	{
		*(this->port-1) |= bitmask;	// DDR Register
	}
	else
	{
		*(this->port-1) &= ~bitmask;	// DDR Register
	}
	
	// GoToSiSy:d:7|o:42|zbase:11
		

} 
/*/////////////////////////////
//
//	setData()	GoToSiSy:d:7|o:42
//
/////////////////////////////
*/
void PortInput::setData(portMask_t newData)
{
	//  aus Template: SysPortInputOutputAvr		GoToSiSy:o:1702|zBase:1
	// #ifndef OutputLogicLowActive
		*port = ((*port) & ~bitmask) | (newData & bitmask) ;			// PORTx
	// #else
	// 	*port = ((*port) & ~bitmask) | ((~newData) & bitmask) ;			// PORTx
	// #endif
	
	// GoToSiSy:d:7|o:42|zbase:8
		

} 

