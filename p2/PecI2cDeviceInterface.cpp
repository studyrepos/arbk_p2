//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1076</ObjektNummer>	GoToSiSy:d:1062

#define GeneratedBySisy
#define cpp_PecI2cDeviceInterface
#define SISY_CLASS_NAME PecI2cDeviceInterface
#include "PecI2cDeviceInterface.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1062|o:1076
//
/////////////////////////////
PecI2cDeviceInterface::PecI2cDeviceInterface()
{
addr = 0;
pBus = NULL;
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1062|o:1076
//
/////////////////////////////
PecI2cDeviceInterface::~PecI2cDeviceInterface()
{
	
}
/*/////////////////////////////
//
//	connect()	GoToSiSy:d:1062|o:1079
//
/////////////////////////////
*/
void PecI2cDeviceInterface::connect(PecI2cBusInterface& bus, uint8_t i2cSlaveAddr)
{
	addr = i2cSlaveAddr;
	pBus = &bus;
	
	init();
	

} 
/*/////////////////////////////
//
//	init()	GoToSiSy:d:1062|o:1080
//
/////////////////////////////
*/
void PecI2cDeviceInterface::init()
{
	

} 
/*/////////////////////////////
//
//	onStart()	GoToSiSy:d:1062|o:1081
//
/////////////////////////////
*/
void PecI2cDeviceInterface::onStart()
{
	#if I2CBUS_INSTANCE_COUNT == 0
		#if defined(I2CBUS_STANDARD)
			static I2CBUS_STANDARD* pI2cBus = new I2CBUS_STANDARD;
			// connect		
			pBus = pI2cBus;
			init();
			#warning info: Standard-Bus verwendet
		#else
			#warning automatisches 'connect' fehlgeschlagen, keine Standardklasse vorhanden
		#endif
	#elif I2CBUS_INSTANCE_COUNT == 1
		#if defined(I2CBUS_INSTANCE)
			connect(I2CBUS_INSTANCE, addr);
		#else
			#warning automatisches 'connect' fehlgeschlagen, keine Instanz gefunden
		#endif
	#elif I2CBUS_INSTANCE_COUNT == 2
			#warning automatisches 'connect' fehlgeschlagen, mehrere Instanzen gefunden
	#endif
	
	

} 
/*/////////////////////////////
//
//	setAddress()	GoToSiSy:d:1062|o:1082
//
/////////////////////////////
*/
void PecI2cDeviceInterface::setAddress(uint8_t address)
{
	addr = address;
	init();
	

} 

