//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1183</ObjektNummer>	GoToSiSy:d:1182

#if !defined(h_VolumesManager)
#define h_VolumesManager

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "ff.h"
#include "Volume.h"

#include "pec_Strings.h"

class Volume;
#include "diskio.h"

#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class VolumesManager
{

public:

	//	mount()	GoToSiSy:d:1182|o:1190
	int8_t mount(Volume& vol, int8_t volNr=-1);
	Volume* volumes[_VOLUMES];
	//	Konstruktor	GoToSiSy:d:1182|o:1183
	VolumesManager();

	//	Destruktor	GoToSiSy:d:1182|o:1183
	~VolumesManager();

private:

protected:


};

	//	 disk_initialize()	GoToSiSy:d:1182|o:1184
	extern "C" DSTATUS  disk_initialize(BYTE driveNr);

	//	 disk_status()	GoToSiSy:d:1182|o:1185
	extern "C" DSTATUS  disk_status(BYTE driveNr);
	//	 disk_read()	GoToSiSy:d:1182|o:1186
	//	PE:
	//		pBuffer = Data buffer to store read data
	//		sectorNr = Sector address (LBA)
	//		sectorCount = Number of sectors to read (1..255)
	extern "C" DRESULT  disk_read(BYTE driveNr, BYTE* pBuffer, DWORD sectorNr, BYTE sectorCount);
	//	 disk_write()	GoToSiSy:d:1182|o:1187
	//	PE:
	//		pBuffer = Data to be written
	//		sectorNr = Sector address (LBA)
	//		sectorCount = Number of sectors to read (1..255)
	extern "C" DRESULT  disk_write(BYTE driveNr, const BYTE* pBuffer, DWORD sectorNr, BYTE sectorCount);

	//	 disk_ioctl()	GoToSiSy:d:1182|o:1188
	extern "C" DRESULT  disk_ioctl(BYTE driveNr, BYTE ctrlCode, void* pData);

	//	 get_fattime()	GoToSiSy:d:1182|o:1191
	extern "C" DWORD  get_fattime();

#endif
