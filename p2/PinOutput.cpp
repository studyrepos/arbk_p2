//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>21</ObjektNummer>	GoToSiSy:d:7

#define GeneratedBySisy
#define cpp_PinOutput
#define SISY_CLASS_NAME PinOutput
#include "PinOutput.h"

#include "Controller.h"
#include "VolumesManager.h"


#define PinOutputLogic_
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:7|o:21
//
/////////////////////////////
PinOutput::PinOutput()
{
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:7|o:21
//
/////////////////////////////
PinOutput::~PinOutput()
{
	
}
/*/////////////////////////////
//
//	config()	GoToSiSy:d:7|o:21
//
/////////////////////////////
*/
void PinOutput::config(port_t port, pinMask_t bitmask)
{
	//  aus Template: SysPinOutputBaseAvr		GoToSiSy:o:1766|zBase:1
	this->bitmask = bitmask; 
	this->port	  = &port;
		
	// Ausgang
	*(this->port-1) |= bitmask;	// DDR-Register
	
	
	// GoToSiSy:d:7|o:21|zbase:9
		

} 
/*/////////////////////////////
//
//	on()	GoToSiSy:d:7|o:21
//
/////////////////////////////
*/
void PinOutput::on()
{
	//  aus Template: SysPinOutputBaseAvr		GoToSiSy:o:1768|zBase:1
	#if !defined(PinOutputLogic_0)
		*port |= bitmask;			// PORTx
	#else
		*port &= ~bitmask;			// PORTx
	#endif
	
	//  aus Template: OutBase		GoToSiSy:o:51|zBase:1
	
	
	// GoToSiSy:d:7|o:21|zbase:11
		

} 
/*/////////////////////////////
//
//	off()	GoToSiSy:d:7|o:21
//
/////////////////////////////
*/
void PinOutput::off()
{
	//  aus Template: SysPinOutputBaseAvr		GoToSiSy:o:1769|zBase:1
	#if !defined(PinOutputLogic_0)
		*port &= ~bitmask;			// PORTx
	#else
		*port |= bitmask;			// PORTx
	#endif
	
	//  aus Template: OutBase		GoToSiSy:o:52|zBase:1
	
	
	// GoToSiSy:d:7|o:21|zbase:11
		

} 
/*/////////////////////////////
//
//	toggle()	GoToSiSy:d:7|o:21
//
/////////////////////////////
*/
void PinOutput::toggle()
{
	//  aus Template: SysPinOutputBaseAvr		GoToSiSy:o:1770|zBase:1
	*port ^= bitmask;
	
	//  aus Template: OutBase		GoToSiSy:o:53|zBase:1
	
	
	// GoToSiSy:d:7|o:21|zbase:7
		

} 
/*/////////////////////////////
//
//	operator bool()	GoToSiSy:d:7|o:21
//
/////////////////////////////
*/
 PinOutput::operator bool()
{
	
return getState();
} 
/*/////////////////////////////
//
//	setState()	GoToSiSy:d:7|o:21
//
/////////////////////////////
*/
void PinOutput::setState(bool newState)
{
	//  aus Template: SysPinOutputBaseAvr		GoToSiSy:o:1767|zBase:1
	#if defined(PinOutputLogic_0)
		newState != newState;
	#endif
		
	if(newState)	
		*port |=  bitmask;
	else
		*port &= ~bitmask;
	
	// GoToSiSy:d:7|o:21|zbase:11
		

} 
/*/////////////////////////////
//
//	getState()	GoToSiSy:d:7|o:21
//
/////////////////////////////
*/
bool PinOutput::getState()
{
	//  aus Template: SysPinAvr		GoToSiSy:o:1723|zBase:1
	#if defined(PinInputLogic_0)	// Low
		bool isOn = !(*(port-2) & bitmask);
	#else // defined(PinInputLogic_1)	// High
		bool isOn = *(port-2) & bitmask;
	#endif
	
	// GoToSiSy:d:7|o:21|zbase:8
		
return isOn;
} 

