//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>109</ObjektNummer>	GoToSiSy:d:80

/////////////////////////////
//
//	Klasse: PecDevice
//
//	Basisklasse f�r Ger�te die in der Initialisierung ber�cksichtigt werden m�ssen aber keine AppModule sind.
//	
//	Klassen die hiervon abgeleitet sind erhalten die Initialisierungs-Ereignisse onPower und onStart 
//	aber keine Laufzeit-Ereignisse (onWork, onTimer10ms, ...)
//
/////////////////////////////
#define GeneratedBySisy
#define cpp_PecDevice
#define SISY_CLASS_NAME PecDevice
#include "PecDevice.h"

#include "Controller.h"
#include "VolumesManager.h"


PecDevice** deviceList = NULL;

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:80|o:109
//
/////////////////////////////
PecDevice::PecDevice()
{
	registerDevice();
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:80|o:109
//
/////////////////////////////
PecDevice::~PecDevice()
{
	
}
/*/////////////////////////////
//
//	registerDevice()	GoToSiSy:d:80|o:110
//
/////////////////////////////
*/
void PecDevice::registerDevice()
{
	// deviceList => PointerListe auf PecDevice*
	// Eintrag 0: Anzahl der vorhandenen Eintr�ge (nach uint16_t casten !!!)
	
	// erstes => neue Liste anlegen (feste Gr��e: 20);
	if(deviceList == NULL)
	{
		deviceList = (PecDevice**) malloc( (1 + 20) * sizeof(PecDevice*) );
		deviceList[0] = (PecDevice*)0;
	}
	// Belegung der Liste ermitteln
	sysUint_t& deviceCount = reinterpret_cast<sysUint_t&>(deviceList[0]);
	
	// Eintragen
	deviceCount++;
	deviceList[deviceCount] = this;
	

} 
/*/////////////////////////////
//
//	onPower()	GoToSiSy:d:80|o:111
//
/////////////////////////////
*/
void PecDevice::onPower()
{
	 

} 
/*/////////////////////////////
//
//	onStart()	GoToSiSy:d:80|o:112
//
/////////////////////////////
*/
void PecDevice::onStart()
{
	

} 

