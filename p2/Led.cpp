//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>194</ObjektNummer>	GoToSiSy:d:173

#define GeneratedBySisy
#define cpp_Led
#define SISY_CLASS_NAME Led
#include "Led.h"

#include "Controller.h"
#include "VolumesManager.h"


#define PinOutputLogic_
#ifndef _on_
#ifdef PinOutputLogic_0
#define _on_ false
#else
#define _on_ true
#endif
#endif
#if defined( _on_ )
	#undef _on_
#endif
#define _on_ (!logicLowActive)


extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:173|o:194
//
/////////////////////////////
Led::Led()
{
logicLowActive = false;
blinkCode = 0;
blinkTime = 20;
changeState = 0;
	//  aus Template: PecLed
	#undef xXp
	#undef xXb
	#define xXp
	#define xXb
	#if ( !defined(xXp) && !defined(xXb) )
		#ifndef PORT
			#error unbekannter Port PORT
		#endif
		#ifndef BIT
			#error unbekannte BitNr BIT
		#endif
	
		pin.config( PORT, BIT );
	#endif
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:173|o:194
//
/////////////////////////////
Led::~Led()
{
	
}
/*/////////////////////////////
//
//	on()	GoToSiSy:d:173|o:194
//
//	Schaltet die Led an.
//
/////////////////////////////
*/
void Led::on()
{
	//  aus Template: PecLed		GoToSiSy:o:175|zBase:1
	// *port|=bitmask;
	blink((uint8_t)255);
	
	// GoToSiSy:d:173|o:194|zbase:5
		

} 
/*/////////////////////////////
//
//	off()	GoToSiSy:d:173|o:194
//
//	Schaltet die Led aus.
//
/////////////////////////////
*/
void Led::off()
{
	//  aus Template: PecLed		GoToSiSy:o:176|zBase:1
	//*port&=~bitmask;
	flashTimeout=0;
	blink((uint8_t)0);
	
	// GoToSiSy:d:173|o:194|zbase:6
		

} 
/*/////////////////////////////
//
//	onTimer10ms()	GoToSiSy:d:173|o:194
//
//	Anwendungstimer 10ms, steuert die Blink-Codes.
//
/////////////////////////////
*/
void Led::onTimer10ms()
{
	//  aus Template: PecLed		GoToSiSy:o:178|zBase:1
	#ifndef _on_
		#ifdef PinOutputLogic_0
			#define _on_ false
		#else
			#define _on_ true
		#endif
	#endif
	
	
	// wenn Flash
	if(flashTimeout)
	{
		if( flashTimeout > 20 )
			pin.setState(_on_);
		else if( flashTimeout == 20 )
			pin.setState(!_on_);
		flashTimeout--;
	}
	// wenn Blinken
	else 
	{
		// off
		if(blinkCode == 0)
			pin.setState(!_on_);
		// on
		else if(blinkCode == 0xFF)
			pin.setState(_on_);
		// dimm half
		else if(blinkCode == 0xFE)
			pin.toggle();
		// flicker
		else if(blinkCode == 0xFD)
		{
			if(!blinkTimeout)
			{
				blinkTimeout = blinkTime/4;
				pin.toggle();
			}
			else
				blinkTimeout--;
		}
		else
		{ 
			if(!blinkTimeout)
			{
				// letztes
				if(blinkCounter >= (blinkCode<<1))
				{
					blinkCounter = 0;
					blinkTimeout = blinkTime*4;
					pin.setState(!_on_);
				}
				else
				{
					pin.toggle();
					blinkCounter++;
					blinkTimeout = blinkTime;
				}
			
			
			}
			else
				blinkTimeout--;
		}
	}
	
	// GoToSiSy:d:173|o:194|zbase:68
		

} 
/*/////////////////////////////
//
//	flash()	GoToSiSy:d:173|o:194
//
//	L�sst die Led einmal kurz aufblinken.
//
/////////////////////////////
*/
void Led::flash(uint8_t flashTime)
{
	//  aus Template: PecLed		GoToSiSy:o:179|zBase:1
	if(!flashTimeout)
	{
		flashTimeout = flashTime + 20;
		blinkCounter = 0;
		blinkCode = 0;
		blinkTimeout = 0;
		// pBlinkCode=NULL;
	}
	
	// GoToSiSy:d:173|o:194|zbase:11
		

} 
/*/////////////////////////////
//
//	blink()	GoToSiSy:d:173|o:194
//
//	Aktiviert einen Blink-Code der Led.
//	* 0x00: Led aus
//	* 0xFF: Led an
//	* 0xFE: Led toggle, bei jedem 10ms-Timer umschalten
//	* sonst: Led blinkt die angegebene Anzahl, macht eine Pause und blinkt erneut.
//	
//	PE:
//		blinkCode=neuer Blink-Code laut Beschreibung.
//
/////////////////////////////
*/
void Led::blink(uint8_t blinkCode)
{
	//  aus Template: PecLed		GoToSiSy:o:180|zBase:1
	if( this->blinkCode != blinkCode)
	{
		flashTimeout=0;
		blinkCounter=0;
		blinkTimeout=0;
		this->blinkCode = blinkCode;
		pin.setState(!_on_);
	}
	
	// GoToSiSy:d:173|o:194|zbase:11
		

} 
/*/////////////////////////////
//
//	nextBlinkCode()	GoToSiSy:d:173|o:194
//
//	Schaltet zum n�chsten BlinkCode.
//	Der aktuelle Blink-Code wird um offset erh�ht.
//	
//	PE:
//		offset=Blink-Code Erh�hung
//
/////////////////////////////
*/
void Led::nextBlinkCode(int8_t offset)
{
	//  aus Template: PecLed		GoToSiSy:o:181|zBase:1
	flashTimeout=0;
	// blinkCounter=0;
	// blinkTimeout=0;
	// if(pBlinkCode)
	// {
	// 	pBlinkCode=NULL;
	// 	this->blinkCode = 0+offset;
	// }
	// else
		this->blinkCode += offset;
	
	// GoToSiSy:d:173|o:194|zbase:13
		

} 
/*/////////////////////////////
//
//	toggle()	GoToSiSy:d:173|o:194
//
//	Schaltet die Led an.
//
/////////////////////////////
*/
void Led::toggle()
{
	//  aus Template: PecLed		GoToSiSy:o:186|zBase:1
	if(blinkCode)
		off();
	else
		on();
	
	// GoToSiSy:d:173|o:194|zbase:7
		

} 
/*/////////////////////////////
//
//	getBlinkCode()	GoToSiSy:d:173|o:194
//
//	Liefert den aktuellen Blink-Code der Led.
//	
//	PE:
//		keine
//		
//	PA:
//		aktueller blinkCode
//
/////////////////////////////
*/
uint8_t Led::getBlinkCode()
{
	
return blinkCode;
} 
/*/////////////////////////////
//
//	setBlinkCode()	GoToSiSy:d:173|o:194
//
//	Aktiviert einen Blink-Code der Led.
//	* 0x00: Led aus
//	* 0xFF: Led an
//	* 0xFE: Led toggle, bei jedem 10ms-Timer umschalten
//	* sonst: Led blinkt die angegebene Anzahl, macht eine Pause und blinkt erneut.
//	
//	PE:
//		blinkCode=neuer Blink-Code laut Beschreibung.
//
/////////////////////////////
*/
void Led::setBlinkCode(uint8_t blinkCode)
{
	//  aus Template: PecLed		GoToSiSy:o:188|zBase:1
	flashTimeout=0;
	blinkCounter=0;
	blinkTimeout=0;
	this->blinkCode = blinkCode;
	// pBlinkCode=NULL;
	
	// GoToSiSy:d:173|o:194|zbase:8
		

} 
/*/////////////////////////////
//
//	dimm()	GoToSiSy:d:173|o:194
//
//	Dimmt die LED mit 50%.
//
/////////////////////////////
*/
void Led::dimm()
{
	//  aus Template: PecLed		GoToSiSy:o:191|zBase:1
	//*port&=~bitmask;
	flashTimeout=0;
	blink((uint8_t)0xFE);
	
	// GoToSiSy:d:173|o:194|zbase:6
		

} 
/*/////////////////////////////
//
//	flicker()	GoToSiSy:d:173|o:194
//
/////////////////////////////
*/
void Led::flicker()
{
	//  aus Template: PecLed		GoToSiSy:o:192|zBase:1
	//*port&=~bitmask;
	flashTimeout=0;
	blink((uint8_t)0xFD);
	
	// GoToSiSy:d:173|o:194|zbase:6
		

} 

