//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1093</ObjektNummer>	GoToSiSy:d:1062

#if !defined(h_PecDS1307)
#define h_PecDS1307

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "PecI2cDeviceInterface.h"

#include "pec_Strings.h"

#define REG_RTC_SECONDS  0x00
#define REG_RTC_MINUTES  0x01
#define REG_RTC_HOURS    0x02
#define REG_RTC_WEEKDAY  0x03
#define REG_RTC_DAY      0x04
#define REG_RTC_MONTH    0x05
#define REG_RTC_YEAR     0x06
#define REG_RTC_CONTROL  0x07
#define REG_RTC_RAMSTART 0x08
#define REG_RTC_RAMEND   0x3F
#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class PecDS1307 : public PecI2cDeviceInterface
{

public:
	//	formatTimeToString()	GoToSiSy:d:1062|o:1094
	//	Gibt aktuelles Datum und Uhrzeit zur�ck.
	//	Das Format der R�ckgabe kann mittels einer Maske formatiert und individuell angepasst werden.
	//	
	//	PE:
	//		time = Zeiger auf die Ergebniszeichenkette
	//		mask = Zeiger auf die Zeichenkette mit der Maske f�r das Ergebnis
	//		BSP:
	//			Datum = Montag der 2009.04.20, Zeit  = 16:30:00 
	//			mask  = "Am %d.%m.%Y um %h:%i:%s Uhr."   
	//			ergibt: "Am 20.04.2009 um 16:30:00 Uhr."
	//		
	//		Platzhalter:
	//			%Y	Jahr vierstellig (es ist nur 20xx m�glich, nicht 19xx)
	//			%y	Jahr zweistellig
	//			%M	Monat als  Zahl
	//			%d	Tag  als Zahl
	//			%w	Wochentag als Text (Mo,....,So)
	//			%h	Stunden
	//			%m	Minuten
	//			%s	Sekunden
	//			%a	am/pm wenn die Uhr im 12- Stundenformat l�uft
	void formatTimeToString(String& time, const char* mask);
	//	startClock()	GoToSiSy:d:1062|o:1095
	//	Startet die Uhr wieder falls sie angehalten war.
	void startClock();
	//	stopClock()	GoToSiSy:d:1062|o:1096
	//	H�lt die Uhr an.
	void stopClock();
	//	getTime()	GoToSiSy:d:1062|o:1097
	//	Gibt aktuelles Datum und Uhrzeit zur�ck.
	//	Das Format der R�ckgabe kann mittels einer Maske formatiert und individuell angepasst werden.
	//	
	//	PE:
	//		time = Zeiger auf die Ergebniszeichenkette
	//		mask = Zeiger auf die Zeichenkette mit der Maske f�r das Ergebnis
	//		BSP:
	//			Datum = Montag der 2009.04.20, Zeit  = 16:30:00 
	//			mask  = "Am %d.%m.%Y um %h:%i:%s Uhr."
	//			ergibt: "Am 20.04.2009 um 16:30:00 Uhr."
	//		
	//		Platzhalter:
	//			%Y	Jahr vierstellig (es ist nur 20xx m�glich, nicht 19xx)
	//			%y	Jahr zweistellig
	//			%m	Monat als  Zahl
	//			%d	Tag  als Zahl
	//			%w	Wochentag als Text (Mo,....,So)
	//			%h	Stunden
	//			%i	Minuten
	//			%s	Sekunden
	//			%a	am/pm wenn die Uhr im 12- Stundenformat l�uft
	bool getTime(char* time, char* mask);
	//	setTime()	GoToSiSy:d:1062|o:1098
	//	Setzt die Uhrzeit.
	//	BSP:
	//		165308  oder 16:53:08   = 16 Uhr 53 min 08 sek (24-Stunden-Modus)
	//		040000a oder 04:00:00:a =  4 Uhr 0 min 0 sek (12-Stunden-Modus)
	//		040000p oder 04:00:00:p = 16 Uhr 0 min 0 sek (12-Stunden-Modus)
	//		163050  oder 16-30-50   = 16 Uhr 30 min 50 sek (24-Stunden-Modus)
	//	Format:
	//		Stunden zweistellig, 
	//		Minuten zweistellig, 
	//		Sekunden zweistellig, 
	//		wenn 12-Stunden Format: ein 'p' f�r pm oder ein 'a' f�r am. 
	//	Die einzelnen Angaben k�nnen durch ein Trennzeichen abgetrennt werden, g�ltige Zeichen sind :, /, -.
	//	
	//	PE:
	//		time = Zeichenkette mit der Uhrzeit die einstellt werden soll.
	//		in24h = ob die Zeit als 24 oder 12 Stundenformat angegeben ist, 24-Stundenformat ist Standard.
	void setTime(const char* time);
	//	setDate()	GoToSiSy:d:1062|o:1099
	//	Setzt das Datum.
	//	BSP:
	//		0904201 oder 09-04-20-1 = Mo der 20.04.2009
	//	Format:
	//		Jahr zweistellig, 
	//		Monat zweistellig, 
	//		Tag zweistellig und 
	//		Wochentag als Zahl (1 f�r Montag, 7 f�r Sonntag). 
	//	Die einzelnen Angaben k�nnen durch ein Trennzeichen abgetrennt werden, g�ltige Zeichen sind ., / und -.
	//	
	//	PE:
	//		date = Die Zeichenkette mit dem Datum welches einstellt werden soll.
	void setDate(const char* date);
	//	convertTime()	GoToSiSy:d:1062|o:1100
	//	L�dt die Uhrzeit in die Attribute.
	//	BSP:
	//		165308  oder 16:53:08   = 16 Uhr 53 min 08 sek (24-Stunden-Modus)
	//		040000a oder 04:00:00:a =  4 Uhr 0 min 0 sek (12-Stunden-Modus)
	//		040000p oder 04:00:00:p = 16 Uhr 0 min 0 sek (12-Stunden-Modus)
	//		163050  oder 16-30-50   = 16 Uhr 30 min 50 sek (24-Stunden-Modus)
	//	Format:
	//		Stunden zweistellig, 
	//		Minuten zweistellig, 
	//		Sekunden zweistellig, 
	//		wenn 12-Stunden Format: ein 'p' f�r pm oder ein 'a' f�r am. 
	//	Die einzelnen Angaben k�nnen durch ein Trennzeichen abgetrennt werden, g�ltige Zeichen sind :, /, -.
	//	
	//	PE:
	//		time = Zeichenkette mit der Uhrzeit die einstellt werden soll.
	//		in24h = ob die Zeit als 24 oder 12 Stundenformat angegeben ist, 24-Stundenformat ist Standard.
	void convertTime(const char* time);
	//	convertDate()	GoToSiSy:d:1062|o:1101
	//	L�dt das Datum in die Attribute.
	//	BSP:
	//		0904201 oder 09-04-20-1 = Mo der 20.04.2009
	//	Format: 
	//		Jahr zweistellig, 
	//		Monat zweistellig, 
	//		Tag zweistellig und 
	//		Wochentag als Zahl (1 f�r Montag, 7 f�r Sonntag). 
	//	Die einzelnen Angaben k�nnen durch ein Trennzeichen abgetrennt werden, g�ltige Zeichen sind ., / und -.
	//	
	//	PE:
	//		date = Die Zeichenkette mit dem Datum welches einstellt werden soll.
	void convertDate(const char* date);
	//	getTime()	GoToSiSy:d:1062|o:1102
	//	Gibt aktuelles Datum und Uhrzeit zur�ck.
	//	Das Format der R�ckgabe kann mittels einer Maske formatiert und individuell angepasst werden.
	//	
	//	PE:
	//		time = Zeiger auf die Ergebniszeichenkette
	//		mask = Zeiger auf die Zeichenkette mit der Maske f�r das Ergebnis
	//		BSP:
	//			Datum = Montag der 2009.04.20, Zeit  = 16:30:00 
	//			mask  = "Am %d.%m.%Y um %h:%i:%s Uhr."
	//			ergibt: "Am 20.04.2009 um 16:30:00 Uhr."
	//		
	//		Platzhalter:
	//			%Y	Jahr vierstellig (es ist nur 20xx m�glich, nicht 19xx)
	//			%y	Jahr zweistellig
	//			%m	Monat als  Zahl
	//			%d	Tag  als Zahl
	//			%w	Wochentag als Text (Mo,....,So)
	//			%h	Stunden
	//			%i	Minuten
	//			%s	Sekunden
	//			%a	am/pm wenn die Uhr im 12- Stundenformat l�uft
	String getTime(const char* mask);
	//	read()	GoToSiSy:d:1062|o:1110
	//	Liest die aktuelle Zeit in die Klassenvariablen.
	bool read();
	//	write()	GoToSiSy:d:1062|o:1111
	//	Schreibt die Klassenvariablen.
	bool write();
	//	Sekunden
	//	bit7 = 1 -> angehalten
	uint8_t seconds;
	//	Minuten
	uint8_t minutes;
	//	Stunden
	//	bit6 = 1 -> 12h-Format (bit5=am/pm)
	uint8_t hours;
	//	Wochentag
	//	1=Montag ... 7=Sonntag
	uint8_t weekday;
	//	Tag des Monats
	uint8_t day;
	//	Monat
	uint8_t month;
	//	Jahr, Zweistellig
	uint8_t year;
	//	Konstruktor	GoToSiSy:d:1062|o:1093
	PecDS1307();

	//	Destruktor	GoToSiSy:d:1062|o:1093
	~PecDS1307();

private:

protected:
	//	bcd2num()	GoToSiSy:d:1062|o:1112
	//	Wandelt eine 2stellige gepackte BCD-Zahl in einen Integer.
	static uint8_t bcd2num(uint8_t value);
	//	num2bcd()	GoToSiSy:d:1062|o:1113
	//	Wandelt eine Zahl in eine 2stelligen gepackte BCD-Zahl.
	static uint8_t num2bcd(uint8_t value);


};

#endif
