//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>171</ObjektNummer>	GoToSiSy:d:117

#define GeneratedBySisy
#define cpp_PecString
#define SISY_CLASS_NAME PecString
#include "PecString.h"

#include "Controller.h"
#include "VolumesManager.h"


#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:117|o:171
//
/////////////////////////////
PecString::PecString()
{
pBuffer = NULL;
len = 0;
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:117|o:171
//
/////////////////////////////
PecString::~PecString()
{
	//  aus Template: PecString
	deinit();
	
}
/*/////////////////////////////
//
//	addContent()	GoToSiSy:d:117|o:171
//
//	F�gt an einen vorhandenen String an.
//	
//	PE:
//		text= anzuf�gender Text
//		size=Anzahl der anzuf�genden Zeichen, wenn -1 wird strlen verwendet
//
/////////////////////////////
*/
void PecString::addContent(const char* text, StringSize_t size)
{
	//  aus Template: PecString		GoToSiSy:o:119|zBase:1
	bool ok = false;
	// ben�tigte Gr��e festlegen 
	if(size == (StringSize_t)-1)
		size = strlen(text);
	StringSize_t oldLen = len;
	uint32_t newSize = len;
	newSize += size;
	if(newSize < (StringSize_t)(-1))
		ok = setSize((StringSize_t)newSize);
	if(ok)
	{
		memcpy((void*)(pBuffer+oldLen),text,size);
	}
	
	// GoToSiSy:d:117|o:171|zbase:16
		

} 
/*/////////////////////////////
//
//	deinit()	GoToSiSy:d:117|o:171
//
//	Deinitialisierung/Speicherfreigabe
//
/////////////////////////////
*/
void PecString::deinit()
{
	//  aus Template: PecString		GoToSiSy:o:120|zBase:1
	if(pBuffer!=NULL)
	{
		free(pBuffer);
	}
	pBuffer=NULL;
	len=0;
	
	// GoToSiSy:d:117|o:171|zbase:9
		

} 
/*/////////////////////////////
//
//	find()	GoToSiSy:d:117|o:171
//
//	Liefert das erste Auftreten eines Zeichens im String.
//	
//	PE:
//		c=Zeichen
//		startPos=die Start-Position der Suche
//	PA:
//		Zeichenposition oder -1, wenn nicht gefunden
//		
//
/////////////////////////////
*/
StringSize_t PecString::find(char c, StringSize_t startPos) const
{
	//  aus Template: PecString		GoToSiSy:o:121|zBase:1
	StringSize_t pos = -1;
	if(pBuffer != NULL && startPos<len)
	{
		uint8_t* ch = (uint8_t*)memchr(pBuffer+startPos,c,len);
		if(ch != NULL)
			pos = ch-pBuffer;
	}
	
	// GoToSiSy:d:117|o:171|zbase:10
		
return pos;
} 
/*/////////////////////////////
//
//	find()	GoToSiSy:d:117|o:171
//
//	Liefert das erste Auftreten einer Zeichenkette im String.
//	
//	PE:
//		search = Zeichenkette, die gesucht wird
//		startPos = (optional) die Start-Position der Suche
//	PA:
//		Startposition oder -1, wenn nicht gefunden
//		
//
/////////////////////////////
*/
StringSize_t PecString::find(char* search, StringSize_t startPos) const
{
	//  aus Template: PecString		GoToSiSy:o:122|zBase:1
	StringSize_t pos = -1;
	if(pBuffer != NULL && startPos<len)
	{
		uint8_t* ch = (uint8_t*)strstr((char*)(pBuffer+startPos),search);
		if(ch != NULL)
			pos = ch-pBuffer;
	}
	
	// GoToSiSy:d:117|o:171|zbase:10
		
return pos;
} 
/*/////////////////////////////
//
//	format()	GoToSiSy:d:117|o:171
//
//	Beachte: es werden immer 16bit-Zahlen umgesetzt, d.h. keine Vorzeichen bei 8-bit-Zahlen und keinerlei 32bit-Zahlen.
//
/////////////////////////////
*/
void PecString::format(const char* format, ...)
{
	//  aus Template: PecString		GoToSiSy:o:123|zBase:1
	va_list argList;
	va_start(argList, format);
	
	#if defined(USE_BIG_STRING)
		StringSize_t need = StringFormatSizeStart;
		StringSize_t buffLang;
		do{
				buffLang=need+1;	// zzgl Null
				if(!setSize(buffLang))
				{
					setSize(0);
					break;
				}
					need = vsnprintf((char*)pBuffer, buffLang, format, argList);
		} while(need >= buffLang);
			
		setSize(need);
		pBuffer[need]=0;
	#else
			setSize(0);
			_sprintf (format, argList);
	#endif
	va_end(argList);
	
	// GoToSiSy:d:117|o:171|zbase:26
		

} 
/*/////////////////////////////
//
//	getPart()	GoToSiSy:d:117|o:171
//
//	Gibt eine Teilzeichenkette zur�ck.
//	Wenn zu viel angegeben wurde, wird nur bis zum Ende zur�ckgegeben. Wenn die Startposition zu gro� oder anz gleich Null ist, wird eine leere Zeichenkette zur�ckgegeben.
//	
//	PE:
//		startPos = Offset des ersten Zeichens (mit Null beginnend)
//		anz = Anzahl der gew�nschten Zeichen
//
/////////////////////////////
*/
String PecString::getPart(StringSize_t startPos, StringSize_t anz) const
{
	//  aus Template: PecString		GoToSiSy:o:124|zBase:1
	String str;
	if(startPos >= len)
		anz=0;
	else if(anz>len || startPos+anz > len)
	{
		// maximal der Rest
		anz = len-startPos;
	}
	str.setContent((char*)(pBuffer+startPos), anz);
	
	// GoToSiSy:d:117|o:171|zbase:12
		
return str;
} 
/*/////////////////////////////
//
//	getSize()	GoToSiSy:d:117|o:171
//
//	Liefert die Gr��e des Strings.
//
/////////////////////////////
*/
StringSize_t PecString::getSize() const
{
	
return len;
} 
/*/////////////////////////////
//
//	operator char*()	GoToSiSy:d:117|o:171
//
//	Casting-Operator nach char*
//
/////////////////////////////
*/
 PecString::operator char*()
{
	//  aus Template: PecString		GoToSiSy:o:126|zBase:1
	char* pChar = (char*)pBuffer;
	// GoToSiSy:d:117|o:171|zbase:3
		
return pChar;
} 
/*/////////////////////////////
//
//	operator const char*()	GoToSiSy:d:117|o:171
//
//	Casting-Operator nach const char*
//
/////////////////////////////
*/
 PecString::operator const char*() const
{
	//  aus Template: PecString		GoToSiSy:o:127|zBase:1
	char* pChar = (char*)pBuffer;
	// GoToSiSy:d:117|o:171|zbase:3
		
return pChar;
} 
/*/////////////////////////////
//
//	operator+()	GoToSiSy:d:117|o:171
//
//	Operator f�r das Zusammensetzen von Strings.
//
/////////////////////////////
*/
String PecString::operator+(const String& text)
{
	//  aus Template: PecString		GoToSiSy:o:128|zBase:1
	String str;
	str.setContent((char*)pBuffer,len);
	str.addContent(text,text.len);
	// GoToSiSy:d:117|o:171|zbase:5
		
return str;
} 
/*/////////////////////////////
//
//	operator+=()	GoToSiSy:d:117|o:171
//
//	Operator f�r das Anf�gen eines Zeichens.
//
/////////////////////////////
*/
String& PecString::operator+=(const char c)
{
	//  aus Template: PecString		GoToSiSy:o:129|zBase:1
	addContent(&c,1);
	// GoToSiSy:d:117|o:171|zbase:3
		
return *this;
} 
/*/////////////////////////////
//
//	operator+=()	GoToSiSy:d:117|o:171
//
//	Operator f�r das Anf�gen von Strings.
//
/////////////////////////////
*/
String& PecString::operator+=(const String& text)
{
	//  aus Template: PecString		GoToSiSy:o:130|zBase:1
	addContent(text,text.getSize());
	// GoToSiSy:d:117|o:171|zbase:3
		
return *this;
} 
/*/////////////////////////////
//
//	operator==()	GoToSiSy:d:117|o:171
//
//	Operator zum Vergleich mit einem anderen String.
//
/////////////////////////////
*/
bool PecString::operator==(const String& text) const
{
	//  aus Template: PecString		GoToSiSy:o:131|zBase:1
	bool eq = false;
	if(text.len == len)
	{
		eq = true;
		for(StringSize_t x=0; x<len; ++x)
		{
			if(pBuffer[x] != text.pBuffer[x])
			{
				eq = false;
				break;
			}
		}
	}
	
	// GoToSiSy:d:117|o:171|zbase:16
		
return eq;
} 
/*/////////////////////////////
//
//	operator=()	GoToSiSy:d:117|o:171
//
//	Zuweisungsoperator mit const char*
//	BSP:
//		text="Zeichenkette";
//
/////////////////////////////
*/
String& PecString::operator=(const char* text)
{
	//  aus Template: PecString		GoToSiSy:o:132|zBase:1
	setContent(text);
	// GoToSiSy:d:117|o:171|zbase:3
		
return *this;
} 
/*/////////////////////////////
//
//	setContent()	GoToSiSy:d:117|o:171
//
//	Setzt den Inhalt eines Strings.
//	
//	PE:
//		text = zu verwendender Text
//		size = Anzal der aus text zu �bertragenden Zeichen
//
/////////////////////////////
*/
bool PecString::setContent(const char* text, StringSize_t size)
{
	//  aus Template: PecString		GoToSiSy:o:134|zBase:1
	bool ok = false;
	// ben�tigte Gr��e festlegen
	if(size == (StringSize_t)-1)
		size = strlen(text);
	// Gr��e einstellen
	ok=setSize(size);
	// Daten kopieren
	if(ok && len!=0)
	{
		memcpy((void*)pBuffer,text,len);
	// 	memmove(pBuffer,text,len);
	}
	
	// GoToSiSy:d:117|o:171|zbase:15
		
return ok;
} 
/*/////////////////////////////
//
//	setSize()	GoToSiSy:d:117|o:171
//
//	Setzt die Gr��e des Strings.
//	Die Funktion allokiert Speicher f�r den String in 4-Byte-Bl�cken.
//	Bei OutOfMemory wird ein Watchdog-Reset ausgel�st.
//	
//	PE:
//		newSize=ben�tigte Speicher-Gr��e
//
/////////////////////////////
*/
bool PecString::setSize(StringSize_t newSize)
{
	//  aus Template: PecString		GoToSiSy:o:135|zBase:1
	// realloc mit NULL ist malloc
	// malloc ist nur in 4-Byte schritten
	// malloc mit 0 bzw. 1 byte bringt abst�rze bei folgeaufruf
	
	#ifndef StringStepSize
	//RAMEND
	#define StringStepSize 32	// !!! in Zweierpotenz schritten
	#endif
	
	#define StringStepSizeMask (StringStepSize-1)
	
	bool ok = true;
	if(newSize==0)
		deinit();
	
	if(pBuffer==0 || (newSize|StringStepSizeMask) != (len|StringStepSizeMask))	// Gr��e �ndern, nie leer
	{
		// Interrupts deaktivieren f�r realloc
	// 	uint8_t sreg=SREG;
	// 	cli();
	// 	if(pBuffer==0)
	// 		pBuffer = (uint8_t*)malloc((newSize|StringStepSizeMask)+1);
	// 	else	
			pBuffer = (uint8_t*)realloc((void*)pBuffer,(newSize|StringStepSizeMask)+1);
	// 	SREG=sreg;
	// 	////// Reinlaufen in STACK --> STOPP
	// #ifdef RAM_NO_CHK_HEAP_TO_SP
	// 	if(pBuffer==NULL || pBuffer+newSize+4 > (uint8_t*)SP)
	// #else
	// 	if(pBuffer==NULL)
	// #endif
	// 	{
	// #ifdef isDEBUG	
	// 		DebugPrintFlash("!!!! STOP PecString::setSize(..) out of Memory\n");
	// 		debug.printFreeMem();
	// 		DebugPrintDez("... oldSize=",(len|3)+1);
	// 		DebugPrintDez("... newSize=",(newSize|3)+1);
	// 		DebugPrintDez("... pBuffer=",(uint32_t)pBuffer);
	// 		DebugPrintDez("... SP=",(uint32_t)SP);
	// //		DebugPrintDump((uint8_t*)SP,100);
	// 		debug.printAllocFreelist();
	// 		waitMs(1000);
	// #endif
	// 		{MyRESET;}
	// 	}
		if(pBuffer == 0)
			ok=false;
	}
	if(ok)
	{
		len=newSize;
		pBuffer[len]=0;
	}
	
	
	if(!ok)
	{
	//	DebugPrintFlash("!!! PecString::setSize(..) FEHLER\n");
	}
	
	
	// GoToSiSy:d:117|o:171|zbase:63
		
return ok;
} 
/*/////////////////////////////
//
//	String()	GoToSiSy:d:117|o:171
//
//	Erstellt einen String aus einer konstanten Zeichenkette.
//	
//	PE:
//		text=Zeichenkette, z.B. "der Text"
//
/////////////////////////////
*/
 PecString::String(const char* text)
{
	//  aus Template: PecString		GoToSiSy:o:136|zBase:1
	pBuffer=NULL;
	len=0;
	setContent(text);
	
	// GoToSiSy:d:117|o:171|zbase:6
		
return ;
} 
/*/////////////////////////////
//
//	addInt()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::addInt(int32_t num)
{
	//  aus Template: PecString		GoToSiSy:o:137|zBase:1
	
	#ifdef __AVR__
		char buffer[11] = {0};
		ltoa(num,(char*)buffer,10);
		addContent(buffer);
	#else
		String txt;
		txt.format("%d", num);
		addContent((char*)txt.pBuffer,txt.len);
	#endif
	
	// GoToSiSy:d:117|o:171|zbase:13
		

} 
/*/////////////////////////////
//
//	operator==()	GoToSiSy:d:117|o:171
//
//	Operator zum Vergleich mit einer Flash-Zeichenkette.
//
/////////////////////////////
*/
bool PecString::operator==(const char* pText) const
{
	//  aus Template: PecString		GoToSiSy:o:139|zBase:1
	bool eq = false;
	if(pBuffer != NULL && len > 0)
	{
		if(memcmp((void*)pBuffer, pText, len+1) == 0)
			eq = true;
	}
	// GoToSiSy:d:117|o:171|zbase:8
		
return eq;
} 
/*/////////////////////////////
//
//	toInt()	GoToSiSy:d:117|o:171
//
//	Wandelt einen String in eine Zahl um. Es werden Hexadezimal-Zahlen (0x..), Bin�rzahlen (0b...) und Dezimalzahlen erkannt.
//	
//	PE:
//		endPt = (optional) Zeiger auf Zeiger f�r die R�ckgabe der Position, an der das erste unbekannte Zeichen auftrat.
//		
//	PA:
//		erkannte Zahl, Null bei Fehler
//
/////////////////////////////
*/
int32_t PecString::toInt(uint8_t** endPt) const
{
	//  aus Template: PecString		GoToSiSy:o:140|zBase:1
	int32_t num = 0;
	
	StringSize_t pos=0;
	if(len > 0)
	{
		uint8_t basis = 10;
		// erstes Zeichen: 0 => Formattest (0x=Hex; 0b=Bin)
		if(pBuffer[0] == '0')
		{
			if(pBuffer[1] == 'x')
			{
				basis = 16;
				pos+=2;
			}
			else if(pBuffer[1] == 'b')
			{
				basis = 2;
				pos+=2;
			}
		}
	
		num = strtol((char*)pBuffer+pos,(char**)endPt,basis);
	}
	
	// GoToSiSy:d:117|o:171|zbase:26
		
return num;
} 
/*/////////////////////////////
//
//	findReverse()	GoToSiSy:d:117|o:171
//
//	Liefert das letzte Auftreten eines Zeichens im String.
//	
//	PE:
//		c=Zeichen
//	PA:
//		Zeichenposition von vorn oder -1, wenn nicht gefunden
//		
//
/////////////////////////////
*/
StringSize_t PecString::findReverse(char c) const
{
	//  aus Template: PecString		GoToSiSy:o:142|zBase:1
	StringSize_t pos = -1;
	if(pBuffer != NULL)
	{
		uint8_t* ch = pBuffer + len;
		while(ch > pBuffer)
		{
			ch--;
			if(*ch == c)
			{
				pos = ch-pBuffer;
				break;
			}
		}
	}
	
	// GoToSiSy:d:117|o:171|zbase:17
		
return pos;
} 
/*/////////////////////////////
//
//	toUpperCase()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::toUpperCase()
{
	//  aus Template: PecString		GoToSiSy:o:143|zBase:1
	for(StringSize_t i=0; i<len; i++)
	{
		pBuffer[i] = toupper(pBuffer[i]);
	}
	
	// GoToSiSy:d:117|o:171|zbase:7
		

} 
/*/////////////////////////////
//
//	toLowerCase()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::toLowerCase()
{
	//  aus Template: PecString		GoToSiSy:o:144|zBase:1
	for(StringSize_t i=0; i<len; i++)
	{
		pBuffer[i] = tolower(pBuffer[i]);
	}
	
	// GoToSiSy:d:117|o:171|zbase:7
		

} 
/*/////////////////////////////
//
//	operator=()	GoToSiSy:d:117|o:171
//
//	Zuweisungsoperator mit const char*
//	BSP:
//		text="Zeichenkette";
//
/////////////////////////////
*/
String& PecString::operator=(const String& other)
{
	//  aus Template: PecString		GoToSiSy:o:145|zBase:1
	setContent((char*)other.pBuffer, other.len);
	
	// GoToSiSy:d:117|o:171|zbase:4
		
return *this;
} 
/*/////////////////////////////
//
//	addInt()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::addInt(uint32_t num)
{
	//  aus Template: PecString		GoToSiSy:o:146|zBase:1
	
	#ifdef __AVR__
		char buffer[10] = {0};
		ultoa(num,(char*)buffer,10);
		addContent(buffer);
	#else
		String txt;
		txt.format("%u", num);
		addContent((char*)txt.pBuffer,txt.len);
	#endif
	
	// GoToSiSy:d:117|o:171|zbase:13
		

} 
/*/////////////////////////////
//
//	addNumBin()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::addNumBin(uint32_t num)
{
	//  aus Template: PecString		GoToSiSy:o:147|zBase:1
	char txt[35] = {0}; // '0b' + max 32 Stellen + '\0'
	uint8_t i = 33;
	
	while(num > 0)
	{
		if(num & 1)
			txt[i] = '1';
		else
			txt[i] = '0';
		num /= 2;
		i--;
		if(i<2)
			break;
	}
	while(i>1)
	{
		txt[i] = '0';
		i--;
	}
	txt[0] = '0';
	txt[1] = 'b';
	
	addContent(txt);
	
	// GoToSiSy:d:117|o:171|zbase:26
		

} 
/*/////////////////////////////
//
//	addNumHex()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::addNumHex(uint32_t num)
{
	//  aus Template: PecString		GoToSiSy:o:148|zBase:1
	#ifdef __AVR__
		char buffer[11] = {0};
		buffer[0]='0';
		buffer[1]='x';
		ultoa(num,(char*)&(buffer[2]),16);
		addContent(buffer);
	#else
		String txt;
		txt.format("0x%X", num);
		addContent((char*)txt.pBuffer,txt.len);
	#endif
	
	
	// GoToSiSy:d:117|o:171|zbase:15
		

} 
/*/////////////////////////////
//
//	clear()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::clear()
{
	//  aus Template: PecString		GoToSiSy:o:149|zBase:1
	setSize(0);
	
	// GoToSiSy:d:117|o:171|zbase:4
		

} 
/*/////////////////////////////
//
//	count()	GoToSiSy:d:117|o:171
//
//	Z�hlt das Vorkommen eines bestimmten Zeichens im Text.
//
/////////////////////////////
*/
StringSize_t PecString::count(char ch) const
{
	//  aus Template: PecString		GoToSiSy:o:150|zBase:1
	StringSize_t anz=0;
	if(pBuffer != NULL)
	{
		uint8_t* p = pBuffer;
		StringSize_t i=len;
		while(i)
		{
			if(*p==ch)
				anz++;
			p++;
			i--;
		}
	}
	// GoToSiSy:d:117|o:171|zbase:15
		
return anz;
} 
/*/////////////////////////////
//
//	fromInt()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::fromInt(int32_t num)
{
	//  aus Template: PecString		GoToSiSy:o:151|zBase:1
	setSize(0);
	addInt(num);
	
	// GoToSiSy:d:117|o:171|zbase:5
		

} 
/*/////////////////////////////
//
//	fromInt()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::fromInt(uint32_t num)
{
	//  aus Template: PecString		GoToSiSy:o:152|zBase:1
	setSize(0);
	addInt(num);
	
	// GoToSiSy:d:117|o:171|zbase:5
		

} 
/*/////////////////////////////
//
//	fromNumBin()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::fromNumBin(uint32_t num)
{
	//  aus Template: PecString		GoToSiSy:o:153|zBase:1
	setSize(0);
	addNumBin(num);
	
	// GoToSiSy:d:117|o:171|zbase:5
		

} 
/*/////////////////////////////
//
//	fromNumHex()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::fromNumHex(uint32_t num)
{
	//  aus Template: PecString		GoToSiSy:o:154|zBase:1
	setSize(0);
	addNumHex(num);
	
	// GoToSiSy:d:117|o:171|zbase:5
		

} 
/*/////////////////////////////
//
//	getPartTo()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
String PecString::getPartTo(char trenn)
{
	//  aus Template: PecString		GoToSiSy:o:155|zBase:1
	String part;
	StringSize_t pos = find(trenn, 0);
	if(pos == (StringSize_t)-1)
	{
		part.setContent((char*)pBuffer, len);
		setSize(0);
	}
	else
	{
		part.setContent((char*)pBuffer, pos);
		remove(0, pos+1);
	}
	
	// GoToSiSy:d:117|o:171|zbase:15
		
return part;
} 
/*/////////////////////////////
//
//	insert()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::insert(const String& text, StringSize_t startPos)
{
	//  aus Template: PecString		GoToSiSy:o:156|zBase:1
	StringSize_t oldLen = len;
	setSize(len + text.len);
	// hinteren Teil wegschieben
	uint8_t* von = pBuffer+oldLen;
	uint8_t* nach= von+text.len;
	for(StringSize_t i=oldLen-startPos+1;i>0;i--)
		{ *nach = *von; nach--; von--; }
	// neuen Teil einf�gen
	memcpy((void*)(pBuffer+startPos),text.pBuffer,text.len);
	
	// GoToSiSy:d:117|o:171|zbase:12
		

} 
/*/////////////////////////////
//
//	insert()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::insert(char newChar, StringSize_t pos)
{
	//  aus Template: PecString		GoToSiSy:o:157|zBase:1
	StringSize_t oldLen = len;
	setSize(len+1);
	// hinteren Teil wegschieben
	uint8_t* von = pBuffer+oldLen;
	uint8_t* nach= von+1;
	for(StringSize_t i=oldLen-pos+1;i>0;i--)
		{ *nach = *von; nach--; von--; }
	// neuen Teil einf�gen
	pBuffer[pos]=newChar;
	
	// GoToSiSy:d:117|o:171|zbase:12
		

} 
/*/////////////////////////////
//
//	remove()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::remove(StringSize_t startPos, StringSize_t anz)
{
	//  aus Template: PecString		GoToSiSy:o:158|zBase:1
	if(anz > 0 && startPos < len)
	{
		if(startPos+anz > len)
			anz = len - startPos;
	
		StringSize_t rest = len-(startPos+anz);
		if(rest > 0)
		{
			for(StringSize_t x=0; x<rest; ++x)
				pBuffer[startPos+x] = pBuffer[startPos+anz+x];
		}
		setSize(len-anz);
		pBuffer[len]=0;
	
	}
	
	// GoToSiSy:d:117|o:171|zbase:18
		

} 
/*/////////////////////////////
//
//	replace()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::replace(const char* txtOld, const char* txtNew)
{
	//  aus Template: PecString		GoToSiSy:o:159|zBase:1
	StringSize_t lenOld = strlen(txtOld);
	
	// suchen
	uint8_t* pPos = NULL;
	//while( (pPos = (uint8_t*)memmem(pBuffer,len, txtOld,lenOld)) != NULL)	// solange txtOld gefunden
	while( (pPos = (uint8_t*)strstr((char*)pBuffer, txtOld)) != NULL)	// solange txtOld gefunden
	{
		remove((pPos-pBuffer), lenOld);
		insert(txtNew,pPos-pBuffer);
	}
	
	// GoToSiSy:d:117|o:171|zbase:13
		

} 
/*/////////////////////////////
//
//	replace()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::replace(char charOld, char charNew)
{
	//  aus Template: PecString		GoToSiSy:o:160|zBase:1
	uint8_t* p=pBuffer;
	StringSize_t i = len;
	while( i )
	{
		if((char)*p==charOld)
			*p=charNew;
		p++;
		i--;
	}
	
	// GoToSiSy:d:117|o:171|zbase:12
		

} 
/*/////////////////////////////
//
//	trim()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::trim()
{
	//  aus Template: PecString		GoToSiSy:o:161|zBase:1
	if(pBuffer != NULL && len > 0)
	{
		// links
		for(StringSize_t x=0; x < len; x++)
		{
			if(!isspace(pBuffer[x]))
			{
				if(x > 0)
					remove(0,x);
				break;
			}
			// alles spaces -> alles ok
			if(x==len-1)
				setSize(0);
		}
		// rechts
		for(StringSize_t x=(len-1); x != (StringSize_t)-1 ; --x)
		{
			if(!isspace(pBuffer[x]))
			{
				if(x < (len-1))
					remove(x+1,((len-1)-x));
				break;
			}	
		}	
	}
	// GoToSiSy:d:117|o:171|zbase:28
		

} 
/*/////////////////////////////
//
//	urlDecode()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::urlDecode()
{
	//  aus Template: PecString		GoToSiSy:o:162|zBase:1
	StringSize_t s=0;	// Source
	StringSize_t d=0;	// Destination
	while(s<=len)	// bis einschlie�lich 0
	{
		// ersetzung
		if(pBuffer[s]=='%')
		{
			uint8_t hex[3];
			hex[0]=pBuffer[s+1];
			hex[1]=pBuffer[s+2];
			hex[2]=0;
			uint8_t c = strtoul((char*)hex,NULL,16);
			// ok?
			if(c>0)	
			{
				pBuffer[d++]=c;
				s+=3;
			}
			else
				pBuffer[d++]=pBuffer[s++];
		}
		else
			pBuffer[d++]=pBuffer[s++];
	}
	if(s!=d)
		setSize(d-1);
	
	// GoToSiSy:d:117|o:171|zbase:29
		

} 
/*/////////////////////////////
//
//	urlEncode()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::urlEncode()
{
	//  aus Template: PecString		GoToSiSy:o:163|zBase:1
	String url;
	String tmp;
	uint8_t c;
	for(StringSize_t x=0;x<len;x++)
	{
		c=pBuffer[x];
		if(isalnum(c))
			url.addContent((char*)&c,1);
		else
		{
			tmp.format("%%%02X",c);
			url.addContent(tmp, tmp.getSize());
		}
	}
	setContent(url,url.getSize());
	
	// GoToSiSy:d:117|o:171|zbase:18
		

} 
/*/////////////////////////////
//
//	operator=()	GoToSiSy:d:117|o:171
//
//	Zuweisungsoperator mit const char
//	BSP:
//		text='A';
//
/////////////////////////////
*/
String& PecString::operator=(const char c)
{
	//  aus Template: PecString		GoToSiSy:o:164|zBase:1
	setSize(1);
	pBuffer[0] = c;
	
	// GoToSiSy:d:117|o:171|zbase:5
		
return *this;
} 
/*/////////////////////////////
//
//	String()	GoToSiSy:d:117|o:171
//
//	Erstellt einen String aus einen anderen String (Copy-Konstruktor)
//	
//	PE:
//		other=anderer String
//
/////////////////////////////
*/
 PecString::String(const String& other)
{
	//  aus Template: PecString		GoToSiSy:o:165|zBase:1
	pBuffer=NULL;
	len=0;
	setContent((char*)other.pBuffer, other.len);
	
	// GoToSiSy:d:117|o:171|zbase:6
		
return ;
} 
/*/////////////////////////////
//
//	toFloat()	GoToSiSy:d:117|o:171
//
//	Wandelt einen String in eine Gleitkomma-Zahl um.
//	
//	PE:
//		
//	PA:
//		erkannte Zahl, Null bei Fehler
//
/////////////////////////////
*/
double PecString::toFloat() const
{
	//  aus Template: PecString		GoToSiSy:o:166|zBase:1
	double num = 0;
	
	if(len > 0)
	{
		num = atof((char*)pBuffer);
	}
	
	// GoToSiSy:d:117|o:171|zbase:9
		
return num;
} 
/*/////////////////////////////
//
//	formatAdd()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::formatAdd(const char* format, ...)
{
	//  aus Template: PecString		GoToSiSy:o:167|zBase:1
	String txt;
	
	va_list argList;
	va_start(argList, format);
	
	StringSize_t need = StringFormatSizeStart;
	StringSize_t buffLang;
	
	do{
		buffLang=need+1;	// zzgl Null
		if(!txt.setSize(buffLang))
		{
			txt.setSize(0);
			break;
		}
		need = vsnprintf ((char*)txt.pBuffer, buffLang, format, argList);
	} while(need >= buffLang);
	va_end(argList);
	
	txt.setSize(need);
	txt.pBuffer[need]=0;
	
	addContent((char*)txt.pBuffer, txt.len);
	
	
	// GoToSiSy:d:117|o:171|zbase:27
		

} 
/*/////////////////////////////
//
//	isEqual()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
bool PecString::isEqual(const char* pText)
{
	//  aus Template: PecString		GoToSiSy:o:168|zBase:1
	bool equal = false;
	if(pBuffer != NULL && len > 0)
	{
		if(memcmp((void*)pBuffer, pText, len+1) == 0)
			equal = true;
	}
	
	// GoToSiSy:d:117|o:171|zbase:9
		
return equal;
} 
/*/////////////////////////////
//
//	_itoa()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::_itoa(char* buf, unsigned int d, int base)
{
	//  aus Template: PecString		GoToSiSy:o:169|zBase:1
	unsigned int div = 1;
	
	while ((int)(d/div) >= base)
		div *= base;
	
	while (div != 0)
	{
		unsigned int num = d/div;
		d = d%div;
		div /= base;
		if (num > 9)
			*buf = (char)((num-10) + 'A');
		else
			*buf = (char)(num + '0');
		buf++;
	}
	
	// GoToSiSy:d:117|o:171|zbase:19
		

} 
/*/////////////////////////////
//
//	_sprintf()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
int PecString::_sprintf(const char* fmt, va_list va)
{
	//  aus Template: PecString		GoToSiSy:o:170|zBase:1
	// char *start_buf = buf;
	while(*fmt)
	{
		/* Character needs formating? */
		if (*fmt == '%')
		{
			switch (*(++fmt))
			{
			  case 'c':
			  	{
	//			*buf++ = va_arg(va, int);
					char c = va_arg(va, int);
					addContent(&c, 1);
				}
				break;
			  case 'd':
			  case 'i':
				{
					signed int val = va_arg(va, signed int);
					if (val < 0)
					{
						val *= -1;
	// 					*buf++ = '-';
						operator+=('-');	
					}
					char buf[11] = {0};
					_itoa(buf, val, 10);
					addContent(buf);			
				}
				break;
			  case 's':
				{
					char * arg = va_arg(va, char *);
					addContent(arg);
	// 				while (*arg)
	// 				{
	// 					*buf++ = *arg++;
	// 				}
				}
				break;
			  case 'u':
				{
					char buf[11] = {0};
					_itoa(buf, va_arg(va, unsigned int), 10);
					addContent(buf);			
				}
				break;
			  case 'x':
			  case 'X':
				{
					char buf[8] = {0};
					this->_itoa(buf, va_arg(va, int), 16);
	// 				addContent("0x", 2);		
					addContent(buf);			
				}
				break;
			  case '%':
	// 			  *buf++ = '%';
					operator+=('%');		
				break;
			  default:
			  		operator+=('?');
				break;
			}
			fmt++;
		}
		/* Else just copy */
		else
		{
	// 		*buf++ = *fmt++;
			addContent(fmt, 1);
			fmt++;
		}
	}
	// *buf = 0;
	// int length = (int)(buf - start_buf);
	
	// GoToSiSy:d:117|o:171|zbase:79
		
return len;
} 
/*/////////////////////////////
//
//	addInt()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::addInt(uint16_t num)
{
	//  aus Template: SysStringAvr		GoToSiSy:o:1826|zBase:1
	char buffer[6] = {0};
	utoa(num,(char*)buffer,10);
	addContent(buffer);
	
	// GoToSiSy:d:117|o:171|zbase:6
		

} 
/*/////////////////////////////
//
//	addInt()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::addInt(int16_t num)
{
	//  aus Template: SysStringAvr		GoToSiSy:o:1827|zBase:1
	char buffer[7] = {0};
	itoa(num,(char*)buffer,10);
	addContent(buffer);
	
	// GoToSiSy:d:117|o:171|zbase:6
		

} 
/*/////////////////////////////
//
//	addInt()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::addInt(uint8_t num)
{
	//  aus Template: SysStringAvr		GoToSiSy:o:1828|zBase:1
	addInt( (uint16_t) num);
	
	// GoToSiSy:d:117|o:171|zbase:4
		

} 
/*/////////////////////////////
//
//	addInt()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
void PecString::addInt(int8_t num)
{
	//  aus Template: SysStringAvr		GoToSiSy:o:1829|zBase:1
	addInt( (int16_t) num);
	
	// GoToSiSy:d:117|o:171|zbase:4
		

} 
/*/////////////////////////////
//
//	setContentFlash()	GoToSiSy:d:117|o:171
//
//	Setzt den Inhalt eines Strings.
//	
//	PE:
//		text = zu verwendender Text, muss im Flash stehen
//		size = Anzal der aus text zu �bertragenden Zeichen
//
/////////////////////////////
*/
bool PecString::setContentFlash(const char* text, StringSize_t size)
{
	//  aus Template: SysStringAvr		GoToSiSy:o:1830|zBase:1
	bool ok = false;
	// ben�tigte Gr��e festlegen
	if(size == (StringSize_t)-1)
		size = strlen_P(text);
	// Gr��e einstellen
	ok=setSize(size);
	// Daten kopieren
	if(ok && len!=0)
	{
		memcpy_P((void*)pBuffer,text,len);
	}
	
	// GoToSiSy:d:117|o:171|zbase:14
		
return ok;
} 
/*/////////////////////////////
//
//	addContentFlash()	GoToSiSy:d:117|o:171
//
//	F�gt an einen vorhandenen String an.
//	
//	PE:
//		text= anzuf�gender Text, muss im Flash stehen
//		size=Anzahl der anzuf�genden Zeichen, wenn -1 wird strlen verwendet
//
/////////////////////////////
*/
void PecString::addContentFlash(const char* text, StringSize_t size)
{
	//  aus Template: SysStringAvr		GoToSiSy:o:1831|zBase:1
	bool ok = false;
	// ben�tigte Gr��e festlegen 
	if(size == (StringSize_t)-1)
		size = strlen_P(text);
	StringSize_t oldLen = len;
	uint32_t newSize = len;
	newSize += size;
	if(newSize < (StringSize_t)(-1))
		ok = setSize((StringSize_t)newSize);
	if(ok)
	{
		memcpy_P((void*)(pBuffer+oldLen),text,size);
	}
	
	// GoToSiSy:d:117|o:171|zbase:16
		

} 
/*/////////////////////////////
//
//	isEqualFlash()	GoToSiSy:d:117|o:171
//
/////////////////////////////
*/
bool PecString::isEqualFlash(const char* pText)
{
	//  aus Template: SysStringAvr		GoToSiSy:o:1832|zBase:1
	bool equal = false;
	if(pBuffer != NULL && len > 0)
	{
		if(memcmp_P((void*)pBuffer, pText, len+1) == 0)
			equal = true;
	}
	// GoToSiSy:d:117|o:171|zbase:8
		
return equal;
} 

/*/////////////////////////////
//
//	operator+()	GoToSiSy:d:117|o:171
//
//	Zusammensetzen von Zeichenketten.
//
/////////////////////////////
*/
String operator+(const char* txt1, const String& txt2)
{
	//  aus Template: PecString		GoToSiSy:o:141|zBase:1
	String str;
	str.setContent(txt1);
	str.addContent(txt2,txt2.getSize());
	
	// GoToSiSy:d:117|o:171|zbase:6
		
return str;
} 

