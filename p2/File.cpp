//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1211</ObjektNummer>	GoToSiSy:d:1182

#define GeneratedBySisy
#define cpp_File
#define SISY_CLASS_NAME File
#include "File.h"

#include "Controller.h"
#include "VolumesManager.h"


#include <string.h>
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1182|o:1211
//
/////////////////////////////
File::File()
{
errorCode = FR_OK;
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1182|o:1211
//
/////////////////////////////
File::~File()
{
	
}
/*/////////////////////////////
//
//	open()	GoToSiSy:d:1182|o:1212
//
/////////////////////////////
*/
bool File::open(const String& fileName, bool readonly, bool createIfNotExists)
{
	bool ok = false;
	
	uint8_t mode = FA_READ;
	if(!readonly)
		mode |= FA_WRITE;
	if(createIfNotExists)
		mode |= FA_OPEN_ALWAYS;
	else
		mode |= FA_OPEN_EXISTING;
	
	memset(&file, 0, sizeof(FIL));
	errorCode = f_open(&file, fileName, mode);
	if(!errorCode)
		ok = true;
	
return ok;
} 
/*/////////////////////////////
//
//	close()	GoToSiSy:d:1182|o:1214
//
/////////////////////////////
*/
bool File::close()
{
	bool ok = false;
	errorCode = f_close(&file);
	if(!errorCode)
		ok = true;
	memset(&file, 0, sizeof(FIL));
	
return ok;
} 
/*/////////////////////////////
//
//	read()	GoToSiSy:d:1182|o:1215
//
/////////////////////////////
*/
uint32_t File::read(uint8_t* pBuffer, uint32_t len)
{
	uint32_t count = 0;
	errorCode = f_read(&file, pBuffer, len, (UINT*)&count);
	
return count;
} 
/*/////////////////////////////
//
//	write()	GoToSiSy:d:1182|o:1216
//
/////////////////////////////
*/
uint32_t File::write(const uint8_t* pBuffer, uint32_t len)
{
	uint32_t count = 0;
	errorCode = f_write(&file, pBuffer, len, (UINT*)&count);
	
return count;
} 
/*/////////////////////////////
//
//	isOpen()	GoToSiSy:d:1182|o:1217
//
/////////////////////////////
*/
bool File::isOpen()
{
	bool isOpened=false;
	if(file.fs != 0)
		isOpened=true;
	
return isOpened;
} 
/*/////////////////////////////
//
//	seek()	GoToSiSy:d:1182|o:1218
//
/////////////////////////////
*/
bool File::seek(uint32_t pos)
{
	bool ok = false;
	
	if(pos == (uint32_t)-1)
		pos = getSize();
	
	errorCode = f_lseek(&file, pos);
	if(!errorCode)
		ok = true;
	
return ok;
} 
/*/////////////////////////////
//
//	getSize()	GoToSiSy:d:1182|o:1219
//
/////////////////////////////
*/
uint32_t File::getSize()
{
	uint32_t size = f_size(&file);
	
return size;
} 
/*/////////////////////////////
//
//	eof()	GoToSiSy:d:1182|o:1221
//
/////////////////////////////
*/
bool File::eof()
{
	
	
return f_eof(&file);
} 
/*/////////////////////////////
//
//	setSize()	GoToSiSy:d:1182|o:1222
//
/////////////////////////////
*/
bool File::setSize(uint32_t newSize)
{
	bool ok = seek(newSize);
	if(ok)
	{
		errorCode = f_truncate(&file);
		if(errorCode)
			ok = false;
	}
	
return ok;
} 
/*/////////////////////////////
//
//	tell()	GoToSiSy:d:1182|o:1223
//
/////////////////////////////
*/
uint32_t File::tell()
{
	
return f_tell(&file);
} 
/*/////////////////////////////
//
//	create()	GoToSiSy:d:1182|o:1224
//
/////////////////////////////
*/
bool File::create(const String& fileName, bool overwriteIfExists, bool readonly)
{
	bool ok = false;
	
	uint8_t mode = FA_READ;
	if(!readonly)
		mode |= FA_WRITE;
	
	if(overwriteIfExists)
		mode |= FA_CREATE_ALWAYS;
	else
		mode |= FA_CREATE_NEW;
		
	memset(&file, 0, sizeof(FIL));	
	errorCode = f_open(&file, fileName, mode);
	if(!errorCode)
		ok = true;
	
return ok;
} 
/*/////////////////////////////
//
//	read()	GoToSiSy:d:1182|o:1225
//
/////////////////////////////
*/
uint32_t File::read(String& text, uint32_t len)
{
	if(len > getSize())	// -1 = 0xffffffff
		len = getSize();
	text = "";
	text.setSize(len);
	uint32_t count = read(text.pBuffer, len);
	
return count;
} 
/*/////////////////////////////
//
//	write()	GoToSiSy:d:1182|o:1226
//
/////////////////////////////
*/
uint32_t File::write(const String& text)
{
	uint32_t count = write(text.pBuffer, text.getSize());
	
return count;
} 
/*/////////////////////////////
//
//	append()	GoToSiSy:d:1182|o:1227
//
/////////////////////////////
*/
uint32_t File::append(const uint8_t* pBuffer, uint32_t len)
{
	seek();
	uint32_t count = write(pBuffer, len);
	
return count;
} 
/*/////////////////////////////
//
//	append()	GoToSiSy:d:1182|o:1228
//
/////////////////////////////
*/
uint32_t File::append(const String& text)
{
	seek();
	uint32_t count = write(text.pBuffer, text.getSize());
	
return count;
} 

