//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>365</ObjektNummer>	GoToSiSy:d:352

#if !defined(h_SysAdcMultiChannelBase)
#define h_SysAdcMultiChannelBase

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include <avr\interrupt.h>
#ifndef MaxAdcChannels
#define MaxAdcChannels 8
#endif
#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class SysAdcMultiChannelBase
{

public:

	//	config()	GoToSiSy:d:352|o:367
	bool config();

	//	addChannel()	GoToSiSy:d:352|o:369
	static bool addChannel(SysAdcMultiChannelBase& channelObj);

	//	start()	GoToSiSy:d:352|o:365
	void start();

	//	stop()	GoToSiSy:d:352|o:365
	void stop();

	//	scan()	GoToSiSy:d:352|o:365
	void scan();

	static SysAdcMultiChannelBase* channels[MaxAdcChannels];
	//	MUX-Register-Eintrag f�r diesen Channel
	uint8_t channel;
	static adc_t values[MaxAdcChannels ];
	//	Index des aktuellen Kanals
	static sysUint_t aktChannel;
	//	Konstruktor	GoToSiSy:d:352|o:365
	SysAdcMultiChannelBase();

	//	Destruktor	GoToSiSy:d:352|o:365
	~SysAdcMultiChannelBase();

private:

protected:
	//	Idex im Channel-Array
	sysUint_t index;


};

#endif
