//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1083</ObjektNummer>	GoToSiSy:d:1062

#if !defined(h_PecLM75)
#define h_PecLM75

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "PecI2cDeviceInterface.h"

#include "pec_Strings.h"

#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class PecLM75 : public PecI2cDeviceInterface
{

public:

	//	init()	GoToSiSy:d:1062|o:1084
	void init();

	//	setComparator()	GoToSiSy:d:1062|o:1085
	void setComparator(int16_t tempOn, int16_t tempOff);

	//	getRawValue()	GoToSiSy:d:1062|o:1086
	int16_t getRawValue();

	//	getTemp()	GoToSiSy:d:1062|o:1087
	int8_t getTemp();
	//	Konstruktor	GoToSiSy:d:1062|o:1083
	PecLM75();

	//	Destruktor	GoToSiSy:d:1062|o:1083
	~PecLM75();

private:

protected:


};

#endif
