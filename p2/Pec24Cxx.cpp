//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1118</ObjektNummer>	GoToSiSy:d:1062

/////////////////////////////
//
//	Klasse: Pec24Cxx
//
//	Klasse f�r das TWI Add-On myTWI EEPROM.
//	BSP:Verwendung
//		Pec24Cxx eep;
//		...
//		eep.writeData( 0, 0xC5 );
//
/////////////////////////////
#define GeneratedBySisy
#define cpp_Pec24Cxx
#define SISY_CLASS_NAME Pec24Cxx
#include "Pec24Cxx.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1062|o:1118
//
/////////////////////////////
Pec24Cxx::Pec24Cxx()
{
	addr = 0xa0;
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1062|o:1118
//
/////////////////////////////
Pec24Cxx::~Pec24Cxx()
{
	
}
/*/////////////////////////////
//
//	readData()	GoToSiSy:d:1062|o:1119
//
//	Liest an einer Adresse im EEPROM ein Byte aus.
//	
//	PE:
//		adress = die Speicheradresse im EEPROM
//		
//	PA:
//		das ausgelesene Byte
//
/////////////////////////////
*/
uint8_t Pec24Cxx::readData(uint8_t adress)
{
	if(pBus == NULL)
		return 0;
	
	uint8_t data=0;
	// Adressierung
	// TWI starten
	if(pBus->startSlaveWrite(addr))
	{
		// Register adressieren
		pBus->writeByte(adress); 
	
		// Lesen
		// TWI re-starten
		if(pBus->startSlaveRead(addr))
		{
			// Daten holen
			data = pBus->readByte();	// kein ACKN
		}
	}
	//TWI stop
	pBus->generateStop();
	
return data;
} 
/*/////////////////////////////
//
//	writeData()	GoToSiSy:d:1062|o:1120
//
//	Schreibt an einer Adresse im EEPROM ein Byte.
//	
//	PE:
//		adress = die Speicheradresse im EEPROM
//		data =  das zu schreibende Byte
//		
//	PA:
//		ob Aktion erfolgreich war
//
/////////////////////////////
*/
bool Pec24Cxx::writeData(uint8_t adress, uint8_t data)
{
	if(pBus == NULL)
		return false;
		
	bool ok=false;
	// Adressierung
	// TWI starten
	
	if(pBus->startSlaveWrite(addr))
	{
		// Register adressieren
		ok = pBus->writeByte(adress); 
	
		// Lesen
		// TWI re-starten
		if(ok)
			// Daten schreiben
			ok = pBus->writeByte(data);	// kein ACKN
	}
	
	//TWI stop
	pBus->generateStop();
	
	// mist!!!
	waitMs(2);
	
return ok;
} 
/*/////////////////////////////
//
//	readData()	GoToSiSy:d:1062|o:1121
//
//	Liest an einer Adresse im EEPROM mehrere Bytes.
//	
//	PE:
//		address = die Speicheradresse im EEPROM
//		data =  Pointer f�r die ausgelesenden Daten
//		count = Anzahl der Daten in Byte
//		
//	PA:
//		ob Aktion erfolgreich war
//
/////////////////////////////
*/
bool Pec24Cxx::readData(uint8_t address, void* pData, uint8_t count)
{
	bool ok;
	// Adressierung
	// TWI starten
	ok = pBus->startSlaveWrite(addr);
	if(ok)
		// Adresszeiger setzen
		ok = pBus->writeByte(address); 
	if(ok)
		// Lesen
		// TWI re-starten
		ok = pBus->startSlaveRead(addr);
	if(ok)
	{
		while(count--)
		{
			// Daten holen
			if(count)
				*((uint8_t*)pData) = pBus->readByteAck();	// mit ACKN
			else
				*((uint8_t*)pData) = pBus->readByte();	// kein ACKN
			pData = ((uint8_t*)pData) + 1;
		}	
	}
	//TWI stop
	pBus->generateStop();
	
return ok;
} 
/*/////////////////////////////
//
//	writeData()	GoToSiSy:d:1062|o:1122
//
//	Schreibt an einer Adresse im EEPROM mehrere Bytes.
//	
//	PE:
//		address = die Speicheradresse im EEPROM
//		data =  die zu schreibenden Daten
//		count = Anzahl der Daten in Byte
//		
//	PA:
//		ob Aktion erfolgreich war
//
/////////////////////////////
*/
bool Pec24Cxx::writeData(uint8_t address, void* pData, uint8_t count)
{
	bool ok=true;
	// DebugPrint("Pec24Cxx::writeData(..)");
	while(count && ok)
	{
		// Adressierung
		// TWI starten
		// ggf. bis zu 5ms warten, da evt. interner Schreibprozess noch l�uft
		uint8_t repeats = 5*10;
		
		// warten bis bereit => GEHT NICHT !!?? 	
	 	while( !(ok = pBus->startSlaveWrite(addr)) && repeats--)
		{
			waitUs(100);
		}
	
		if(ok)
		{
			// Register adressieren
			ok = pBus->writeByte(address); 
			// Page schreiben
			do
			{
				// Daten schreiben
				ok = pBus->writeByte( *((uint8_t*)pData) );	// kein ACKN
				pData = (uint8_t*)pData + 1;
				count--;
				address++;
			// �berpr�fen ob die letzten Bits zu schreiben sind
			}while(count && (address & 0b111) != 0);
		}
		//TWI stop
		pBus->generateStop();
		
		// mist
		waitMs(2);
	}
	
return ok;
} 

