//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>413</ObjektNummer>	GoToSiSy:d:388

#if !defined(h_PecSpiDeviceInterface)
#define h_PecSpiDeviceInterface

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "PecSpiMasterInterface.h"

#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class PecSpiDeviceInterface
{

public:

	//	connect()	GoToSiSy:d:388|o:415
	void connect(PecSpiMasterInterface& bus);
	//	Konstruktor	GoToSiSy:d:388|o:413
	PecSpiDeviceInterface();

	//	Destruktor	GoToSiSy:d:388|o:413
	~PecSpiDeviceInterface();

private:

protected:
	PecSpiMasterInterface* pBus;


};

#endif
