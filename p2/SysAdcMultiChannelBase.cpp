//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>365</ObjektNummer>	GoToSiSy:d:352

#define GeneratedBySisy
#define cpp_SysAdcMultiChannelBase
#define SISY_CLASS_NAME SysAdcMultiChannelBase
#include "SysAdcMultiChannelBase.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


SysAdcMultiChannelBase* SysAdcMultiChannelBase::channels[MaxAdcChannels];
adc_t SysAdcMultiChannelBase::values[MaxAdcChannels ];
//	Index des aktuellen Kanals
sysUint_t SysAdcMultiChannelBase::aktChannel=0;
extern "C"
{
	ISR(ADC_vect)
	{
		//  aus Template: SysAdcMultiChannelAvr		GoToSiSy:o:1778|zBase:1 
		SysAdcMultiChannelBase::values[SysAdcMultiChannelBase::aktChannel] = ADC; 
		 
		SysAdcMultiChannelBase::aktChannel++; 
		if(SysAdcMultiChannelBase::channels[SysAdcMultiChannelBase::aktChannel] == NULL  
			|| SysAdcMultiChannelBase::aktChannel >= MaxAdcChannels) 
			SysAdcMultiChannelBase::aktChannel=0; 
			 
		uint8_t& channel = SysAdcMultiChannelBase::channels[SysAdcMultiChannelBase::aktChannel]->channel; 
		// Kanal umschalten 
		 
		ADMUX = (ADMUX & ~0x1F) | (channel&0x1f); 
		#ifdef ADCSRB 
			ADCSRB = (ADCSRB & 0xF7) | ((channel & 0x20)>>2);	// MUX5 
		#endif 
		 
		// Restart 
		ADCSRA |= (1<<ADSC); 
		 
		 
		// GoToSiSy:d:352|o:365|zbase:21 
			
	}
}
/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:352|o:365
//
/////////////////////////////
SysAdcMultiChannelBase::SysAdcMultiChannelBase()
{
index = 0;
channel = 0;
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:352|o:365
//
/////////////////////////////
SysAdcMultiChannelBase::~SysAdcMultiChannelBase()
{
	
}
/*/////////////////////////////
//
//	config()	GoToSiSy:d:352|o:367
//
/////////////////////////////
*/
bool SysAdcMultiChannelBase::config()
{
	bool ok = addChannel(*this);
	
return ok;
} 
/*/////////////////////////////
//
//	addChannel()	GoToSiSy:d:352|o:369
//
/////////////////////////////
*/
bool SysAdcMultiChannelBase::addChannel(SysAdcMultiChannelBase& channelObj)
{
	bool ok = false;
	for(sysUint_t i=0; i<MaxAdcChannels; i++)
	{
		if(channels[i] == NULL)
		{
			channels[i] = &channelObj;
			channelObj.index = i;
			ok = true;
			break;
		}
	}
	
	
return ok;
} 
/*/////////////////////////////
//
//	start()	GoToSiSy:d:352|o:365
//
/////////////////////////////
*/
void SysAdcMultiChannelBase::start()
{
	//  aus Template: SysAdcMultiChannelAvr		GoToSiSy:o:1779|zBase:1
	uint8_t adcsra=0;
	// Ref
	ADMUX = (0b01<<6);
	
	// Prescale 128
	adcsra |= 0b111;
	
	// Enable
	adcsra |= 0b10000000;
	
	// INT
	adcsra |= (1<<ADIE);	// INT enable
	
	// schreiben
	ADCSRA = adcsra;
	
	aktChannel = 0;
	
	uint8_t& channel = channels[aktChannel]->channel;
	// Kanal umschalten
	ADMUX = (ADMUX&~0x1F) | (channel & 0x1F);
	#ifdef ADCSRB
		ADCSRB = (ADCSRB & 0xF7) | ((channel & 0x20)>>2);	// MUX5
	#endif
	
	// Restart
	ADCSRA |= (1<<ADSC);
	
	// GoToSiSy:d:352|o:365|zbase:30
		

} 
/*/////////////////////////////
//
//	stop()	GoToSiSy:d:352|o:365
//
/////////////////////////////
*/
void SysAdcMultiChannelBase::stop()
{
	

} 
/*/////////////////////////////
//
//	scan()	GoToSiSy:d:352|o:365
//
/////////////////////////////
*/
void SysAdcMultiChannelBase::scan()
{
	

} 


