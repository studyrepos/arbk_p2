//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>4632</ObjektNummer>	GoToSiSy:d:4612

#define GeneratedBySisy
#define cpp_LedD5
#define SISY_CLASS_NAME LedD5
#include "LedD5.h"

#include "Controller.h"
#include "VolumesManager.h"


#define PinOutputLogic_
#ifndef _on_
#ifdef PinOutputLogic_0
#define _on_ false
#else
#define _on_ true
#endif
#endif
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:4612|o:4632
//
/////////////////////////////
LedD5::LedD5()
{
blinkCode = 0;
blinkTime = 20;
changeState = 0;
	//  aus Template: PecLed
	#undef xXp
	#undef xXb
	#define xXpD
	#define xXb5
	#if ( !defined(xXp) && !defined(xXb) )
		#ifndef PORTD
			#error unbekannter Port PORTD
		#endif
		#ifndef BIT5
			#error unbekannte BitNr BIT5
		#endif
	
		pin.config( PORTD, BIT5 );
	#endif
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:4612|o:4632
//
/////////////////////////////
LedD5::~LedD5()
{
	
}
/////////////////////////////
//
//	on()	GoToSiSy:d:4612|o:4632
//
//	Schaltet die Led an.
//
/////////////////////////////
void LedD5::on()
{
	//  aus Template: PecLed		GoToSiSy:o:175|zBase:1
	// *port|=bitmask;
	blink((uint8_t)255);
	
	// GoToSiSy:d:4612|o:4632|zbase:5
		

} 
/////////////////////////////
//
//	off()	GoToSiSy:d:4612|o:4632
//
//	Schaltet die Led aus.
//
/////////////////////////////
void LedD5::off()
{
	//  aus Template: PecLed		GoToSiSy:o:176|zBase:1
	//*port&=~bitmask;
	flashTimeout=0;
	blink((uint8_t)0);
	
	// GoToSiSy:d:4612|o:4632|zbase:6
		

} 
/////////////////////////////
//
//	onTimer10ms()	GoToSiSy:d:4612|o:4632
//
//	Anwendungstimer 10ms, steuert die Blink-Codes.
//
/////////////////////////////
void LedD5::onTimer10ms()
{
	//  aus Template: PecLed		GoToSiSy:o:178|zBase:1
	#ifndef _on_
		#ifdef PinOutputLogic_0
			#define _on_ false
		#else
			#define _on_ true
		#endif
	#endif
	
	
	// wenn Flash
	if(flashTimeout)
	{
		if( flashTimeout > 20 )
			pin.setState(_on_);
		else if( flashTimeout == 20 )
			pin.setState(!_on_);
		flashTimeout--;
	}
	// wenn Blinken
	else 
	{
		// off
		if(blinkCode == 0)
			pin.setState(!_on_);
		// on
		else if(blinkCode == 0xFF)
			pin.setState(_on_);
		// dimm half
		else if(blinkCode == 0xFE)
			pin.toggle();
		// flicker
		else if(blinkCode == 0xFD)
		{
			if(!blinkTimeout)
			{
				blinkTimeout = blinkTime/4;
				pin.toggle();
			}
			else
				blinkTimeout--;
		}
		else
		{ 
			if(!blinkTimeout)
			{
				// letztes
				if(blinkCounter >= (blinkCode<<1))
				{
					blinkCounter = 0;
					blinkTimeout = blinkTime*4;
					pin.setState(!_on_);
				}
				else
				{
					pin.toggle();
					blinkCounter++;
					blinkTimeout = blinkTime;
				}
			
			
			}
			else
				blinkTimeout--;
		}
	}
	
	// GoToSiSy:d:4612|o:4632|zbase:68
		

} 
/////////////////////////////
//
//	flash()	GoToSiSy:d:4612|o:4632
//
//	L�sst die Led einmal kurz aufblinken.
//
/////////////////////////////
void LedD5::flash(uint8_t flashTime)
{
	//  aus Template: PecLed		GoToSiSy:o:179|zBase:1
	if(!flashTimeout)
	{
		flashTimeout = flashTime + 20;
		blinkCounter = 0;
		blinkCode = 0;
		blinkTimeout = 0;
		// pBlinkCode=NULL;
	}
	
	// GoToSiSy:d:4612|o:4632|zbase:11
		

} 
/////////////////////////////
//
//	blink()	GoToSiSy:d:4612|o:4632
//
//	Aktiviert einen Blink-Code der Led.
//	* 0x00: Led aus
//	* 0xFF: Led an
//	* 0xFE: Led toggle, bei jedem 10ms-Timer umschalten
//	* sonst: Led blinkt die angegebene Anzahl, macht eine Pause und blinkt erneut.
//	
//	PE:
//		blinkCode=neuer Blink-Code laut Beschreibung.
//
/////////////////////////////
void LedD5::blink(uint8_t blinkCode)
{
	//  aus Template: PecLed		GoToSiSy:o:180|zBase:1
	if( this->blinkCode != blinkCode)
	{
		flashTimeout=0;
		blinkCounter=0;
		blinkTimeout=0;
		this->blinkCode = blinkCode;
		pin.setState(!_on_);
	}
	
	// GoToSiSy:d:4612|o:4632|zbase:11
		

} 
/////////////////////////////
//
//	nextBlinkCode()	GoToSiSy:d:4612|o:4632
//
//	Schaltet zum n�chsten BlinkCode.
//	Der aktuelle Blink-Code wird um offset erh�ht.
//	
//	PE:
//		offset=Blink-Code Erh�hung
//
/////////////////////////////
void LedD5::nextBlinkCode(int8_t offset)
{
	//  aus Template: PecLed		GoToSiSy:o:181|zBase:1
	flashTimeout=0;
	// blinkCounter=0;
	// blinkTimeout=0;
	// if(pBlinkCode)
	// {
	// 	pBlinkCode=NULL;
	// 	this->blinkCode = 0+offset;
	// }
	// else
		this->blinkCode += offset;
	
	// GoToSiSy:d:4612|o:4632|zbase:13
		

} 
/////////////////////////////
//
//	toggle()	GoToSiSy:d:4612|o:4632
//
//	Schaltet die Led an.
//
/////////////////////////////
void LedD5::toggle()
{
	//  aus Template: PecLed		GoToSiSy:o:186|zBase:1
	if(blinkCode)
		off();
	else
		on();
	
	// GoToSiSy:d:4612|o:4632|zbase:7
		

} 
/////////////////////////////
//
//	getBlinkCode()	GoToSiSy:d:4612|o:4632
//
//	Liefert den aktuellen Blink-Code der Led.
//	
//	PE:
//		keine
//		
//	PA:
//		aktueller blinkCode
//
/////////////////////////////
uint8_t LedD5::getBlinkCode()
{
	
	return blinkCode;
} 
/////////////////////////////
//
//	setBlinkCode()	GoToSiSy:d:4612|o:4632
//
//	Aktiviert einen Blink-Code der Led.
//	* 0x00: Led aus
//	* 0xFF: Led an
//	* 0xFE: Led toggle, bei jedem 10ms-Timer umschalten
//	* sonst: Led blinkt die angegebene Anzahl, macht eine Pause und blinkt erneut.
//	
//	PE:
//		blinkCode=neuer Blink-Code laut Beschreibung.
//
/////////////////////////////
void LedD5::setBlinkCode(uint8_t blinkCode)
{
	//  aus Template: PecLed		GoToSiSy:o:188|zBase:1
	flashTimeout=0;
	blinkCounter=0;
	blinkTimeout=0;
	this->blinkCode = blinkCode;
	// pBlinkCode=NULL;
	
	// GoToSiSy:d:4612|o:4632|zbase:8
		

} 
/////////////////////////////
//
//	config()	GoToSiSy:d:4612|o:4632
//
/////////////////////////////
void LedD5::config(port_t port, pinMask_t bitmask)
{
	//  aus Template: PecLed		GoToSiSy:o:189|zBase:1
	pin.config(port, bitmask);
	
	// GoToSiSy:d:4612|o:4632|zbase:4
		

} 
/////////////////////////////
//
//	dimm()	GoToSiSy:d:4612|o:4632
//
//	Dimmt die LED mit 50%.
//
/////////////////////////////
void LedD5::dimm()
{
	//  aus Template: PecLed		GoToSiSy:o:191|zBase:1
	//*port&=~bitmask;
	flashTimeout=0;
	blink((uint8_t)0xFE);
	
	// GoToSiSy:d:4612|o:4632|zbase:6
		

} 
/////////////////////////////
//
//	flicker()	GoToSiSy:d:4612|o:4632
//
/////////////////////////////
void LedD5::flicker()
{
	//  aus Template: PecLed		GoToSiSy:o:192|zBase:1
	//*port&=~bitmask;
	flashTimeout=0;
	blink((uint8_t)0xFD);
	
	// GoToSiSy:d:4612|o:4632|zbase:6
		

} 

