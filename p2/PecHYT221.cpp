//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1088</ObjektNummer>	GoToSiSy:d:1062

/////////////////////////////
//
//	Klasse: PecHYT221
//
//	Klasse f�r den TWI-Temperatur- und Feuchtigkeitssensor HYT221.
//
/////////////////////////////
#define GeneratedBySisy
#define cpp_PecHYT221
#define SISY_CLASS_NAME PecHYT221
#include "PecHYT221.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1062|o:1088
//
/////////////////////////////
PecHYT221::PecHYT221()
{
	addr=0x28;
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1062|o:1088
//
/////////////////////////////
PecHYT221::~PecHYT221()
{
	
}
/*/////////////////////////////
//
//	readTemp100Humi()	GoToSiSy:d:1062|o:1089
//
//	Gibt Temperatur in 1/100. Grad und rel. Feuchte in % zur�ck.
//	
//	PE:
//		temp=Temperatur
//		humi=Feuchtigkeit
//
/////////////////////////////
*/
bool PecHYT221::readTemp100Humi(int16_t& temp, uint8_t& humi)
{
	bool ok=true;
	
	//// solange versuchen bis ok
	uint8_t buffer[4];
	uint8_t anz=10;			//versuche
	do{
		ok = getData(buffer);
		if(!ok)
		{
			//DebugPrintFlash("...wait\n");
			waitMs(10);
		}
		anz--;
	}while(!ok && anz!=0); // && errorcode=='B');	// warte auf konvertierung
	
	
	//// Umrechnung Messwert -> 1/10 �C
	//// ( 0...16383 -> -40...+125�C)
	if(ok)
	{
		uint32_t t;
		t  = (uint8_t)buffer[2] << 8;							
		t += (uint8_t)buffer[3];
		t &= 0x0000FFFF;		// = Kampf gegen C++
	// DebugPrintHex("...0x",t);
		t = t >> 2;
	// DebugPrintHex("...0x",t);
		t *= 165UL*100UL;		// f�r 1/100 Grad
	// DebugPrintHex("...0x",t);
		t /= 16384UL;
	// DebugPrintHex("...0x",t);
		temp = (int16_t) t;
		temp -= 40*100;
	// DebugPrintDez("...",temp);
	}
	
	//// Umrechnung Messwert -> rel Feuchte
	//// ( 0...16383 -> 0...100%)
	if(ok)
	{
		uint32_t t;
		t  = buffer[0] << 8;							
		t += buffer[1];
	// DebugPrintHex("...0x",t);
		t *= 100;
		t /= 16384L;
		humi = (uint8_t) t;
	// DebugPrintDez("...",humi);
	}
	
return ok;
} 
/*/////////////////////////////
//
//	getData()	GoToSiSy:d:1062|o:1090
//
//	Liest die Daten des Sensors in den Puffer.
//	
//	PE:
//		pBuffer=zu f�llender Puffer
//
/////////////////////////////
*/
bool PecHYT221::getData(uint8_t* pBuffer)
{
	if(pBus == NULL)
		return false;
	
	bool ok=false;
	uint8_t state=0;
	// TWI (re-)starten
	if(pBus->startSlaveRead(addr))
	{
		// lesen
		*pBuffer = state = pBus->readByteAck();	// mit ACKN = acknowledge = TWI-Datenanforderung
		pBuffer++;
		*pBuffer = pBus->readByteAck();			// mit ACKN = acknowledge = TWI-Datenanforderung
		pBuffer++;
		*pBuffer = pBus->readByteAck();			// mit ACKN = acknowledge = TWI-Datenanforderung
		pBuffer++;
		*pBuffer = pBus->readByte();				// ohne ACKN = acknowledge = keine TWI-Datenanforderung
		ok=true;
	}	
	// Stopp
	pBus->generateStop();
		
	// wenn busy
	if(state & 0b11000000)
	{
		ok=false;
	//	errorcode = 'B';
	}
	
return ok;
} 
/*/////////////////////////////
//
//	startCalc()	GoToSiSy:d:1062|o:1091
//
//	Startet eine Messung
//
/////////////////////////////
*/
bool PecHYT221::startCalc()
{
	if(pBus == NULL)
		return false;
	
	bool ok=true;
	ok = pBus->startSlaveWrite(addr);
	pBus->generateStop();
	
return ok;
} 
/*/////////////////////////////
//
//	sendCmd()	GoToSiSy:d:1062|o:1092
//
//	Sendet ein Kommando an den Sensor.
//	
//	PE:
//		cmd=Kommando
//		data=Daten zum Kommando
//
/////////////////////////////
*/
bool PecHYT221::sendCmd(uint8_t cmd, uint16_t data)
{
	if(pBus == NULL)
		return false;
	
	bool ok=true;
	
	if(pBus->startSlaveWrite(addr))
	{
		ok=pBus->writeByte(cmd);
		if(ok)
			ok=pBus->writeByte(data>>8);
		if(ok)
			ok=pBus->writeByte(data&0xFF);
	}
	else
		ok = false;
	pBus->generateStop();
	
	
	
return ok;
} 

