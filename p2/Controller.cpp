//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>4613</ObjektNummer>	GoToSiSy:d:4612

#define GeneratedBySisy
#define cpp_Controller
#define SISY_CLASS_NAME Controller
#include "Controller.h"

#include "PecDevice.h"

#include "Controller.h"
#include "VolumesManager.h"


#define IsPecAppKernel 1
#if VARISNOTDEF0
#error Controller: timerNr wurde nicht angegeben
#endif
#include "myAVR_TimerDef.h"
#warning info: Controller = Timer0
// Bit f�r CTC-Mode
#if defined(CTC0)
#define MyCTC CTC0
#elif defined(WGM03)
#define MyCTC WGM02
#else
#define MyCTC WGM01
#endif
// Register f�r CTC-Bit
#if defined(TCCR0)
#define MyCtcReg TCCR0
#elif defined(CTC0)
#define MyCtcReg TCCR0B
#elif defined(WGM03)
#define MyCtcReg TCCR0B
#else
#define MyCtcReg TCCR0A
#endif
// Register f�r Prescaler
// nur TCCRn, wie ATmega8
#if defined(TCCR0)
#define MyTCCR TCCR0
// TCCRnA und TCCRnB, wie ATmega168
#elif defined(TCCR0B)
#define MyTCCR TCCR0B
// nur TCCRnA, wie AT90CAN128
#else
#define MyTCCR TCCR0A
#endif
// OCR
#if defined(OCR0)
#define MyOCR OCR0
#define MyOCIE OCIE0
#elif defined(OCR0A)
#define MyOCR OCR0A
#define MyOCIE OCIE0A
#else
#define SCALE
uint8_t timer0ReloadValue = 0;
#if defined(TCNT0)
#define MyTCNT TCNT0
#endif
#endif
// TIMSK
#if defined(TIMSK)
#define MyTIMSK TIMSK
#elif defined(TIMSK0)
#define MyTIMSK TIMSK0
#endif
// ISR-vect
#ifdef SCALE
#define ISR_vect TIMER0_OVF_vect
#elif defined(TIMER0_COMP_vect)
#define ISR_vect TIMER0_COMP_vect
#else
#define ISR_vect TIMER0_COMPA_vect
#endif
extern  Controller app;
extern  VolumesManager gVolumesManager;


extern "C"
{
	ISR(ISR_vect)
	{
		//  aus Template: SysTimerAvr		GoToSiSy:o:1791|zBase:1 
		// reload 
		#ifdef SCALE 
		MyTCNT = timer0ReloadValue; 
		#endif 
		 
		#ifdef IsPecAppKernel 
			app.onSysTick(); 
		#else 
			app.onTimer(); 
		#endif 
		 
		// GoToSiSy:d:4612|o:4613|zbase:13 
			
	}
}
/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
Controller::Controller()
{
systimeSec = 0;
systimeMsec10 = 0;
systimeMsec100 = 0;
firstEventPos = 0;
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
Controller::~Controller()
{
	
}
/////////////////////////////
//
//	onStart()	GoToSiSy:d:4612|o:4614
//
/////////////////////////////
void Controller::onStart()
{
	// boot sequence after start SysTick

} 
/////////////////////////////
//
//	onWork()	GoToSiSy:d:4612|o:4615
//
/////////////////////////////
void Controller::onWork()
{
	// continuous event from the Mainloop
	ledD5.off();
	ledD6.off();
	ledD7.off();
	switchD2.clicked = 0;
	
	int wait = rand() % 2000 + 750;
	waitMs(wait);
	ledD5.on();
	for (int i = 0; i < 500; i++) {
		waitMs(1);	
		if (switchD2.getState() == 1){
			ledD6.on();
			break;
		}
	}
	if (switchD2.getState() == 0) {
		ledD7.on();
	}
	waitMs(2500);

} 
/////////////////////////////
//
//	onSysTick()	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
void Controller::onSysTick()
{
	//  aus Template: PecAppKernel		GoToSiSy:o:92|zBase:1
	uint8_t todo = 0;
	
	////// Rechnen ///////
	systimeMsec100++;
	if ( systimeMsec100 >= 10 )	// jedes 10.
	{
		// alle 100ms
		systimeMsec100 = 0;
		todo = 1;
	}
	systimeMsec10++;
	if( systimeMsec10 >= 100 )	// jedes 100.
	{
		// alle Sekunde
		systimeMsec10 = 0;
		systimeSec++;	// Sekunde hochz�hlen
		todo = 2;
	}
	
	// alle Module: onTimer10ms()
	PecAppModul* pm = pFirstPecAppModul;
	while( pm )
	{
		pm->onTimer10ms();
		pm = pm->pNextObject;
	}
	
	if(todo)
		postEvent(PecSysEvent|OnTimer100ms);
	if(todo>1)
		postEvent(PecSysEvent|OnTimer1s);
	
	
	// GoToSiSy:d:4612|o:4613|zbase:35
		

} 
/////////////////////////////
//
//	workEvents()	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
void Controller::workEvents()
{
	//  aus Template: PecEventQueue		GoToSiSy:o:94|zBase:1
	// solange Events vorhanden
	while(events[firstEventPos].type != 0)
	{
		PecEvent& event = events[firstEventPos];
	
		// wenn nur ein Empf�nger
		if( event.type & DirectedEvent )
		{
			PecEventHandler* pHandler = (PecEventHandler*)event.object;
			pHandler->onEvent( event );
		}
		else
		{
			sysUint_t ret = 0;
			PecAppModul* pm = pFirstPecAppModul;
			// je AppModul
			while( pm && ret==0)
			{
				if(	event.type & PecSysEvent )	// wenn PecSysEvent
				{
					if( (event.type &0x0F) == OnTimer100ms)
						pm->onEvent100ms( );
					else if( (event.type &0x0F) == OnTimer1s)
						pm->onEvent1s( );
					else
						ret = pm->onEvent( event );
				}
				else
					ret = pm->onEvent( event );	
				
				// next AppModul
				pm = pm->pNextObject;
			}
		}
		// Event bearbeitet	-> L�schen
		event.type = 0;
		
		// next Event
		firstEventPos++;
		if( firstEventPos >= 8 )
			firstEventPos = 0;
	}
	
	// GoToSiSy:d:4612|o:4613|zbase:45
		

} 
/////////////////////////////
//
//	postEvent()	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
bool Controller::postEvent(uint8_t type, sysUint_t data, const void* source)
{
	//  aus Template: PecEventQueue		GoToSiSy:o:95|zBase:1
	bool ok = false;
	sysUint_t pos = firstEventPos;
	do
	{
		// leerer Slot gefunden
		if(events[pos].type == 0)
		{
			ok = true;
			events[pos].type = type;
			events[pos].data = data;
			events[pos].object = const_cast<void*>(source);
			break;
		}
		else
		{
			pos++;
			if(pos >= 8)
				pos=0;
		}
	} while(pos != firstEventPos);
	
	
	// GoToSiSy:d:4612|o:4613|zbase:24
		
	return ok;
} 
/////////////////////////////
//
//	hasEvents()	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
bool Controller::hasEvents()
{
	
	return (events[firstEventPos].type != 0);
} 
/////////////////////////////
//
//	main()	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
void Controller::main()
{
	//  aus Template: SysAppKernelAvr		GoToSiSy:o:1634|zBase:1
	extern PecDevice** deviceList;
	PecAppModul* pm;
	////////// powerOn //////////////////////////
	// devices
	if(deviceList != NULL)
	{
		sysUint_t& deviceCount = reinterpret_cast<sysUint_t&>(deviceList[0]);
		for(sysUint_t i=0; i<deviceCount; i++)
		{
			deviceList[i+1]->onPower();
		}
	}
	// module
	pm=pFirstPecAppModul;
	while(pm)
	{
		pm->onPower();
		pm=pm->pNextObject;
	}
	////////// Timer 10ms ////////////////////////
	configHz(100);
	// configMs(10);
	
	////////// powerOn2 //////////////////////////
	// devices
	if(deviceList != NULL)
	{
		sysUint_t& deviceCount = reinterpret_cast<sysUint_t&>(deviceList[0]);
		for(sysUint_t i=0; i<deviceCount; i++)
		{
			deviceList[i+1]->onStart();
		}
	}
	// module
	pm=pFirstPecAppModul;
	while(pm)
	{
		pm->onStart();
		pm=pm->pNextObject;
	}
	
	// deviceList aufr�umen
	if(deviceList != NULL)
	{
		free( deviceList );
		deviceList = NULL;
	}
	
	////////// Mainloop /////////////
	//DebugPrintFlash("---------- Start Mainloop\n");
	while(1)
	{
		workEvents();
		// module
		pm=pFirstPecAppModul;
		while(pm)
		{
			pm->onWork();
			pm=pm->pNextObject;
		}
	}
	
	// GoToSiSy:d:4612|o:4613|zbase:64
		

} 
/////////////////////////////
//
//	config()	GoToSiSy:d:4612|o:4613
//
//	PE:
//		prescale = Vorteiler z.B. 1,2,4,8,...32768
//		topValue = beliebiger Wert 0..65535
//
/////////////////////////////
void Controller::config(uint16_t prescale, uint16_t topValue)
{
	//  aus Template: SysTimerAvr		GoToSiSy:o:1788|zBase:1
	// Scale
	#ifdef SCALE
		timer0ReloadValue = 256-topValue;
		
		MyTCCR  = prescale;
		TCNT0 = timer0ReloadValue;
		MyTIMSK |= (1<<TOIE0);	
	// OC 
	#else
		MyCtcReg |= (1<<MyCTC);
		MyTCCR   |= prescale;
		MyOCR     = topValue-1;
		MyTIMSK  |= (1<<MyOCIE);	
	#endif
	
	sei();
	
	// GoToSiSy:d:4612|o:4613|zbase:19
		

} 
/////////////////////////////
//
//	onTimer()	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
void Controller::onTimer()
{
	

} 
/////////////////////////////
//
//	configHz()	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
bool Controller::configHz(uint32_t frequency)
{
	//  aus Template: SysTimerAvr		GoToSiSy:o:1790|zBase:1
	bool ok=false;
	
	uint32_t topValue;
	const uint16_t prescale[] = Timer0_Prescalers;
	
	#if ((0 == 0) || (0 == 2))
		#ifdef SCALE
			#define TOP_MAX 250
		#else
			#define TOP_MAX 254
		#endif
	#else
		#define TOP_MAX 65534
	#endif
	
	// ersten passenden Wert nehmen
	int8_t i=-1;
	int8_t cnt=(sizeof(prescale)/sizeof(prescale[0]));
	for(; i<cnt; i++)
	{
		uint32_t freq1;
		if(i<0)
			freq1 = SystemCoreClock;
		else
			freq1 = SystemCoreClock/prescale[i];
		topValue = freq1 / frequency;
		if( topValue < TOP_MAX )
		{
			ok=true;
			break;
		}
	}
	
	if(ok)
	{
		config( i+2, topValue & 0xFFFF );
	}
	
	// GoToSiSy:d:4612|o:4613|zbase:40
		
	return ok;
} 
/////////////////////////////
//
//	configMs()	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
bool Controller::configMs(uint32_t milliSeconds)
{
	//  aus Template: SysTimerAvr		GoToSiSy:o:1792|zBase:1
	bool ok=false;
	
	uint32_t topValue;
	const uint16_t prescale[] = Timer0_Prescalers;
	
	#if ((0 == 0) || (0 == 2))
		#ifdef SCALE
			#define TOP_MAX 250
		#else
			#define TOP_MAX 254
		#endif
	#else
		#define TOP_MAX 65534
	#endif
	
	// ersten passenden Wert nehmen
	int8_t i=-1;
	int8_t cnt=(sizeof(prescale)/sizeof(prescale[0]));
	for(; i<cnt; i++)
	{
		uint16_t ps = prescale[i];
		uint16_t divi;
		uint32_t freq1;
		if(ps>1000)
			divi = 1;
		else if(ps>100)
			divi = 10;
		else if(ps>10)
			divi = 100;
		else
			divi=1000;
			
		if(i<0)
			freq1 = SystemCoreClock;
		else
			freq1 = SystemCoreClock/ps;
			
		topValue = (freq1/divi) * milliSeconds / (1000/divi);
		
		if( topValue < TOP_MAX )
		{
			ok=true;
			break;
		}
	}
	
	if(ok)
	{
		config( i+2, topValue & 0xFFFF );
	}
		
	// GoToSiSy:d:4612|o:4613|zbase:53
		
	return ok;
} 
/////////////////////////////
//
//	configUs()	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
bool Controller::configUs(uint32_t microSeconds)
{
	//  aus Template: SysTimerAvr		GoToSiSy:o:1793|zBase:1
	bool ok=false;
	
	uint32_t topValue;
	const uint16_t prescale[] = Timer0_Prescalers;
	
	#if ((0 == 0) || (0 == 2))
		#ifdef SCALE
			#define TOP_MAX 250
		#else
			#define TOP_MAX 254
		#endif
	#else
		#define TOP_MAX 65534
	#endif
	
	// ersten passenden Wert nehmen
	int8_t i=-1;
	int8_t cnt=(sizeof(prescale)/sizeof(prescale[0]));
	for(; i<cnt; i++)
	{
		uint32_t freq1;
		if(i<0)
			freq1 = SystemCoreClock;
		else
			freq1 = SystemCoreClock/prescale[i];
		topValue = ((freq1/1000UL) * microSeconds) / 1000UL;
		if( topValue < TOP_MAX )
		{
			ok=true;
			break;
		}
	}
	
	if(ok)
	{
		config( i+2, topValue & 0xFFFF );
	}
	
	// GoToSiSy:d:4612|o:4613|zbase:40
		
	return ok;
} 
/////////////////////////////
//
//	stop()	GoToSiSy:d:4612|o:4613
//
/////////////////////////////
void Controller::stop()
{
	//  aus Template: SysTimerAvr		GoToSiSy:o:1794|zBase:1
	MyTCCR  = 0;
	
	// GoToSiSy:d:4612|o:4613|zbase:4
		

} 


