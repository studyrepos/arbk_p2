//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1063</ObjektNummer>	GoToSiSy:d:1062

#define GeneratedBySisy
#define cpp_PecI2cBusInterface
#define SISY_CLASS_NAME PecI2cBusInterface
#include "PecI2cBusInterface.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1062|o:1063
//
/////////////////////////////
PecI2cBusInterface::PecI2cBusInterface()
{
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1062|o:1063
//
/////////////////////////////
PecI2cBusInterface::~PecI2cBusInterface()
{
	
}
/*/////////////////////////////
//
//	startSlaveWrite()	GoToSiSy:d:1062|o:1064
//
/////////////////////////////
*/
bool PecI2cBusInterface::startSlaveWrite(uint8_t slaveI2Caddr)
{
	bool ok = false;
	
return ok;
} 
/*/////////////////////////////
//
//	startSlaveRead()	GoToSiSy:d:1062|o:1065
//
/////////////////////////////
*/
bool PecI2cBusInterface::startSlaveRead(uint8_t slaveI2Caddr)
{
	bool ok = false;
	
return ok;
} 
/*/////////////////////////////
//
//	generateStart()	GoToSiSy:d:1062|o:1066
//
/////////////////////////////
*/
bool PecI2cBusInterface::generateStart()
{
	bool ok = false;
	
return ok;
} 
/*/////////////////////////////
//
//	generateStop()	GoToSiSy:d:1062|o:1067
//
/////////////////////////////
*/
void PecI2cBusInterface::generateStop()
{
	

} 
/*/////////////////////////////
//
//	writeByte()	GoToSiSy:d:1062|o:1068
//
/////////////////////////////
*/
bool PecI2cBusInterface::writeByte(uint8_t data)
{
	bool ok = false;
	
return ok;
} 
/*/////////////////////////////
//
//	writeByteAck()	GoToSiSy:d:1062|o:1069
//
/////////////////////////////
*/
bool PecI2cBusInterface::writeByteAck(uint8_t data)
{
	bool ok = false;
	
return ok;
} 
/*/////////////////////////////
//
//	readByte()	GoToSiSy:d:1062|o:1070
//
/////////////////////////////
*/
uint8_t PecI2cBusInterface::readByte()
{
	uint8_t data = 0;
	
return data;
} 
/*/////////////////////////////
//
//	readByteAck()	GoToSiSy:d:1062|o:1071
//
/////////////////////////////
*/
uint8_t PecI2cBusInterface::readByteAck()
{
	uint8_t data = 0;
	
return data;
} 

