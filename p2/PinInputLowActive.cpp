//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>30</ObjektNummer>	GoToSiSy:d:7

#define GeneratedBySisy
#define cpp_PinInputLowActive
#define SISY_CLASS_NAME PinInputLowActive
#include "PinInputLowActive.h"

#include "Controller.h"
#include "VolumesManager.h"


#define PinInputLogic_0
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:7|o:30
//
/////////////////////////////
PinInputLowActive::PinInputLowActive()
{
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:7|o:30
//
/////////////////////////////
PinInputLowActive::~PinInputLowActive()
{
	
}
/*/////////////////////////////
//
//	configPu()	GoToSiSy:d:7|o:30
//
/////////////////////////////
*/
void PinInputLowActive::configPu(port_t port, pinMask_t bitmask)
{
	//  aus Template: SysPinInputBaseAvr		GoToSiSy:o:1764|zBase:1
	this->bitmask = bitmask; 
	this->port	  = &port;
	
	// Eingang
	*(this->port-1) &= ~bitmask;	// DDR Register
		
	// PullUp
	*(this->port) |= bitmask;		// PORTx Register
	
	// GoToSiSy:d:7|o:30|zbase:11
		

} 
/*/////////////////////////////
//
//	config()	GoToSiSy:d:7|o:30
//
/////////////////////////////
*/
void PinInputLowActive::config(port_t port, pinMask_t bitmask)
{
	//  aus Template: SysPinInputBaseAvr		GoToSiSy:o:1763|zBase:1
	this->bitmask = bitmask; 
	this->port	  = &port;
		
	// Eingang
	*(this->port-1) &= ~bitmask;	// DDR Register
	
	// GoToSiSy:d:7|o:30|zbase:8
		

} 
/*/////////////////////////////
//
//	configPd()	GoToSiSy:d:7|o:30
//
/////////////////////////////
*/
void PinInputLowActive::configPd(port_t port, pinMask_t bitmask)
{
	

} 
/*/////////////////////////////
//
//	getState()	GoToSiSy:d:7|o:30
//
/////////////////////////////
*/
bool PinInputLowActive::getState()
{
	//  aus Template: SysPinAvr		GoToSiSy:o:1723|zBase:1
	#if defined(PinInputLogic_0)	// Low
		bool isOn = !(*(port-2) & bitmask);
	#else // defined(PinInputLogic_1)	// High
		bool isOn = *(port-2) & bitmask;
	#endif
	
	//  aus Template: PinInputBase		GoToSiSy:o:47|zBase:1
	
	
	// GoToSiSy:d:7|o:30|zbase:11
		
return isOn;
} 

