//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>197</ObjektNummer>	GoToSiSy:d:173

#if !defined(h_ButtonHighActive)
#define h_ButtonHighActive

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "PecAppModul.h"
#include "ButtonEventDef.h"

#include "pec_Gpio.h"
#include "pec_AppManagement.h"

#include <avr\io.h>
#include <stdlib.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class ButtonHighActive : public PecAppModul
{

public:

	//	configPu()	GoToSiSy:d:173|o:197
	void configPu(port_t port, pinMask_t bitmask);

	//	config()	GoToSiSy:d:173|o:197
	void config(port_t port, pinMask_t bitmask);

	//	configPd()	GoToSiSy:d:173|o:197
	void configPd(port_t port, pinMask_t bitmask);

	//	getState()	GoToSiSy:d:173|o:197
	bool getState();
	//	isPressed()	GoToSiSy:d:173|o:197
	//	Ermittelt, ob der Button gedr�ckt ist.
	bool isPressed();
	//	waitForPress()	GoToSiSy:d:173|o:197
	//	Wartet bis die taste gedr�ckt wurde.
	void waitForPress();

	//	onWork()	GoToSiSy:d:173|o:84
	virtual void onWork();

	//	Enum state
	enum{state_state_Nothing = 1,state_state_down,state_state_click,state_state_hold};

	//	changeState_state()
	void changeState_state(state_t newState);

	//	Konstruktor	GoToSiSy:d:173|o:197
	ButtonHighActive();

	//	Destruktor	GoToSiSy:d:173|o:197
	~ButtonHighActive();

private:

protected:

	//	onClick()	GoToSiSy:d:173|o:197
	virtual void onClick();

	//	onTimer10ms()	GoToSiSy:d:173|o:197
	virtual void onTimer10ms();

	//	onHoldStart()	GoToSiSy:d:173|o:197
	virtual void onHoldStart();

	//	onHoldEnd()	GoToSiSy:d:173|o:197
	virtual void onHoldEnd();
	//	onHolding()	GoToSiSy:d:173|o:197
	//	wird alle 10ms aufgerufen solange Hold aktiv ist
	virtual void onHolding();
	//	Zeit seit erstem Dr�cken
	uint8_t volatile holdCounter;
	//	Zeit seit erstem Dr�cken
	uint8_t volatile releaseCounter;
	volatile uint8_t* port;
	uint8_t bitmask;

	state_t state;

};

#endif
