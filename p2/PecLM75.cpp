//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1083</ObjektNummer>	GoToSiSy:d:1062

#define GeneratedBySisy
#define cpp_PecLM75
#define SISY_CLASS_NAME PecLM75
#include "PecLM75.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1062|o:1083
//
/////////////////////////////
PecLM75::PecLM75()
{
	addr=0x90;
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1062|o:1083
//
/////////////////////////////
PecLM75::~PecLM75()
{
	
}
/*/////////////////////////////
//
//	init()	GoToSiSy:d:1062|o:1084
//
/////////////////////////////
*/
void PecLM75::init()
{
	if(pBus == NULL)
		return;
	
	if(pBus->startSlaveWrite(addr))
	{
		//Register adressieren
		pBus->writeByteAck(0x01);		// LM75_REG_CONFIG
		//Wert schreiben
		pBus->writeByteAck(0);
		//Wert schreiben
		pBus->writeByte(0);
	}
	
	// Stopp
	pBus->generateStop();
	

} 
/*/////////////////////////////
//
//	setComparator()	GoToSiSy:d:1062|o:1085
//
/////////////////////////////
*/
void PecLM75::setComparator(int16_t tempOn, int16_t tempOff)
{
	if(pBus == NULL)
		return;
	
	if(pBus->startSlaveWrite(addr))
	{
		//Register adressieren
		pBus->writeByteAck(0x03);		// LM75_REG_SET
		//Wert schreiben
		pBus->writeByteAck(tempOn>>8);
		//Wert schreiben
		pBus->writeByte(tempOn&0xFF);
	}
	
	if(pBus->startSlaveWrite(addr))
	{
		//Register adressieren
		pBus->writeByteAck(0x02);		// LM75_REG_HYST
		//Wert schreiben
		pBus->writeByteAck(tempOff>>8);
		//Wert schreiben
		pBus->writeByte(tempOff&0xFF);
	}
	
	pBus->generateStop();
	

} 
/*/////////////////////////////
//
//	getRawValue()	GoToSiSy:d:1062|o:1086
//
/////////////////////////////
*/
int16_t PecLM75::getRawValue()
{
	int16_t value = 0;
	if(pBus == NULL)
		return value;
	
	if(pBus->startSlaveWrite(addr))
	{
		// Temperatur adressieren
		pBus->writeByte(0x00);		// LM75_REG_TEMP
	
		//--- Lesezyklus
		if(pBus->startSlaveRead(addr))
		{
			int16_t	highData;
			uint8_t lowData;
			
			// lesen
			highData = (int8_t)pBus->readByteAck(); // mit ACKN = acknowledge = TWI-Datenanforderung
			lowData  = pBus->readByte(); 			// kein ACKN
			
			value = ((highData*8)&(~7)) | (lowData>>5);
		}
	}
	
	// Stopp
	pBus->generateStop();
	
return value;
} 
/*/////////////////////////////
//
//	getTemp()	GoToSiSy:d:1062|o:1087
//
/////////////////////////////
*/
int8_t PecLM75::getTemp()
{
	int8_t temp = 0;
	int16_t value = getRawValue();
	temp = value/8;
	
return temp;
} 

