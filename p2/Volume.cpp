//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1192</ObjektNummer>	GoToSiSy:d:1182

#define GeneratedBySisy
#define cpp_Volume
#define SISY_CLASS_NAME Volume
#include "Volume.h"

#include "Controller.h"
#include "VolumesManager.h"


#include <string.h>
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:1182|o:1192
//
/////////////////////////////
Volume::Volume()
{
	memset(&fs, 0, sizeof(FATFS));
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:1182|o:1192
//
/////////////////////////////
Volume::~Volume()
{
	
}
/*/////////////////////////////
//
//	disk_initialize()	GoToSiSy:d:1182|o:1193
//
/////////////////////////////
*/
DSTATUS Volume::disk_initialize()
{
	DSTATUS state = 0x07;
	
return state;
} 
/*/////////////////////////////
//
//	disk_status()	GoToSiSy:d:1182|o:1194
//
/////////////////////////////
*/
DSTATUS Volume::disk_status()
{
	DSTATUS state = 0x07;
	
return state;
} 
/*/////////////////////////////
//
//	disk_read()	GoToSiSy:d:1182|o:1195
//
/////////////////////////////
*/
DRESULT Volume::disk_read(uint8_t* pBuffer, uint32_t sectorNr, uint8_t sectorCount)
{
	DRESULT result =RES_ERROR;
	
return result;
} 
/*/////////////////////////////
//
//	disk_write()	GoToSiSy:d:1182|o:1196
//
/////////////////////////////
*/
DRESULT Volume::disk_write(const uint8_t* pBuffer, uint32_t sectorNr, uint8_t sectorCount)
{
	
	DRESULT result =RES_ERROR;
	
return result;
} 
/*/////////////////////////////
//
//	disk_ioctl()	GoToSiSy:d:1182|o:1197
//
/////////////////////////////
*/
DRESULT Volume::disk_ioctl(uint8_t ctrlCode, void* pData)
{
	
	DRESULT result =RES_ERROR;
	
return result;
} 
/*/////////////////////////////
//
//	mount()	GoToSiSy:d:1182|o:1199
//
/////////////////////////////
*/
int8_t Volume::mount(int8_t volNr)
{
	volNr = gVolumesManager.mount(*this, volNr);
	
return volNr;
} 

