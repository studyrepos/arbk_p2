//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>29</ObjektNummer>	GoToSiSy:d:7

#if !defined(h_PortInputOutput)
#define h_PortInputOutput

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8


#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class PortInputOutput
{

public:

	//	config()	GoToSiSy:d:7|o:29
	virtual void config(port_t port, pinMask_t bitmask);

	//	setDirection()	GoToSiSy:d:7|o:29
	void setDirection(bool asOutput);

	//	getData()	GoToSiSy:d:7|o:29
	portMask_t getData();

	//	operator=()	GoToSiSy:d:7|o:29
	portMask_t operator=(const portMask_t& newValue);

	//	setData()	GoToSiSy:d:7|o:29
	void setData(portMask_t newData);

	//	setPullUp()	GoToSiSy:d:7|o:29
	virtual void setPullUp(portMask_t mask);
	//	Konstruktor	GoToSiSy:d:7|o:29
	PortInputOutput();

	//	Destruktor	GoToSiSy:d:7|o:29
	~PortInputOutput();

private:

protected:
	volatile uint8_t* port;
	uint8_t bitmask;


};

#endif
