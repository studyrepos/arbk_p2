//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>2432</ObjektNummer>	GoToSiSy:d:2431

#define GeneratedBySisy
#define cpp_SysTwiBusStandardAvr
#define SISY_CLASS_NAME SysTwiBusStandardAvr
#include "SysTwiBusStandardAvr.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
SysTwiBusStandardAvr::SysTwiBusStandardAvr()
{
	//  aus Template: PecI2cBus
	initI2c();
	
	
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
SysTwiBusStandardAvr::~SysTwiBusStandardAvr()
{
	
}
/////////////////////////////
//
//	connect()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
void SysTwiBusStandardAvr::connect(PecI2cDeviceInterface& device, uint8_t i2cSlaveAddr)
{
	//  aus Template: PecI2cBus		GoToSiSy:o:1073|zBase:1
	device.connect(*this, i2cSlaveAddr);
	
	// GoToSiSy:d:2431|o:2432|zbase:4
		

} 
/////////////////////////////
//
//	initI2c()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
void SysTwiBusStandardAvr::initI2c()
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1663|zBase:1
	// Clock
	TWBR=((F_CPU/TWI_CLOCK)-16)/2;
	// TWI-Status-Register (Vorteiler)
	TWSR=0;
	// Bus-Addr
	// TWAR=0x90;
	// Enable
	TWCR=_TWEN;
	
	// GoToSiSy:d:2431|o:2432|zbase:11
		

} 
/////////////////////////////
//
//	startSlaveWrite()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
bool SysTwiBusStandardAvr::startSlaveWrite(uint8_t slaveI2cAddr)
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1655|zBase:1
	bool ok = startSlave(slaveI2cAddr|0x00);
	
	// GoToSiSy:d:2431|o:2432|zbase:4
		
	return ok;
} 
/////////////////////////////
//
//	startSlaveRead()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
bool SysTwiBusStandardAvr::startSlaveRead(uint8_t slaveI2cAddr)
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1656|zBase:1
	bool ok = startSlave(slaveI2cAddr|0x01);
	
	// GoToSiSy:d:2431|o:2432|zbase:4
		
	return ok;
} 
/////////////////////////////
//
//	generateStart()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
bool SysTwiBusStandardAvr::generateStart()
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1657|zBase:1
	uint8_t x=TWCR;
	x &= _TWEN|_TWIE;  	// nur Beibehalten von Enable und Interrupt J/N 
	x |= _TWINT|_TWSTA;
	
	bool ok=false;
	uint8 cnt = 3;
	do{
		TWCR = x;	// START
		ok = waitForReady();
	}
	while(!ok && cnt--);
	
	// GoToSiSy:d:2431|o:2432|zbase:14
		
	return ok;
} 
/////////////////////////////
//
//	generateStop()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
void SysTwiBusStandardAvr::generateStop()
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1658|zBase:1
	uint8_t x=TWCR;
	x&=_TWEN|_TWIE;  	// nur Beibehalten von Enable und Interrupt J/N 
	TWCR=x|_TWINT|_TWSTO;
	
	// GoToSiSy:d:2431|o:2432|zbase:6
		

} 
/////////////////////////////
//
//	writeByte()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
bool SysTwiBusStandardAvr::writeByte(uint8_t data)
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1659|zBase:1
	TWDR=data; 	 	// Daten bereitlegen
	// Befehl zusammenstellen
	uint8_t x=TWCR;
	x&=_TWEN|_TWIE; 	 	//nur Beibehalten von Enable und Interrupt J/N 
	x|=_TWINT;
	
	TWCR=x; 	 	 	// senden
	// warten bis fertig
	bool ok=waitForReady();
	
	// GoToSiSy:d:2431|o:2432|zbase:12
		
	return ok;
} 
/////////////////////////////
//
//	writeByteAck()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
bool SysTwiBusStandardAvr::writeByteAck(uint8_t data)
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1660|zBase:1
	TWDR=data; 	 	// Daten bereitlegen
	// Befehl zusammenstellen
	uint8_t x=TWCR;
	x&=_TWEN|_TWIE; 	 	//nur Beibehalten von Enable und Interrupt J/N 
	x|=_TWINT; 	
	x|=_TWEA; 	// evt. TWEA setzen, f�r Datenanforderung
	TWCR=x; 	 	 	// senden
	// warten bis fertig
	bool ok=waitForReady();
	
	// GoToSiSy:d:2431|o:2432|zbase:12
		
	return ok;
} 
/////////////////////////////
//
//	readByte()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
uint8_t SysTwiBusStandardAvr::readByte()
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1661|zBase:1
	// Befehl zusammenstellen
	uint8_t x=TWCR, data = 0;
	x&=_TWEN|_TWIE; 	 	//nur Beibehalten von Enable und Interrupt J/N 
	x|=_TWINT;
	TWCR=x; 	 	 	// senden
	// warten bis fertig
	waitForReady();
	data = TWDR;
	// DebugPrintDez("SysTwiBusStandardAvr::readByte(..) Data=",data);
	
	// GoToSiSy:d:2431|o:2432|zbase:12
		
	return data;
} 
/////////////////////////////
//
//	readByteAck()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
uint8_t SysTwiBusStandardAvr::readByteAck()
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1662|zBase:1
	// Befehl zusammenstellen
	uint8_t x=TWCR, data = 0;
	x&=_TWEN|_TWIE; 	 	//nur Beibehalten von Enable und Interrupt J/N 
	x|=_TWINT;
	x|=_TWEA; 	// evt. TWEA setzen, f�r Datenanforderung
	TWCR=x; 	 	 	// senden
	// warten bis fertig
	waitForReady();
	data = TWDR;
	
	// GoToSiSy:d:2431|o:2432|zbase:12
		
	return data;
} 
/////////////////////////////
//
//	lConfigureBitRate()	GoToSiSy:d:2431|o:2432
//
/////////////////////////////
void SysTwiBusStandardAvr::lConfigureBitRate(uint32_t& divValue, uint32_t& stepValue)
{
	

} 
/////////////////////////////
//
//	waitForReady()	GoToSiSy:d:2431|o:2432
//
//	Warten auf TWI �bertragung fertig.
//	
//	
//	PA:
//		errorcode='T' bei timeout, sonst errorcode=0
//
/////////////////////////////
bool SysTwiBusStandardAvr::waitForReady()
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1665|zBase:1
	bool ok=true;
	// warten bis fertig
	uint8_t timeOutCounter=100;
	while( !(TWCR & _TWINT))
	{
		timeOutCounter--;
		wait1ms();
		if(timeOutCounter==0)
		{
			ok=false;
			// TWI-Reset
			if(TWCR&_TWEN)	// wenn TWI erlaubt
			{
				uint8_t sreg1=SREG;	// INTs sperren
				cli();
				TWCR &= ~_TWEN;	// aus
				wait1ms();
				TWCR |= _TWEN;	// an
				SREG=sreg1;
			}
			break;
		}
	}
	
	// GoToSiSy:d:2431|o:2432|zbase:26
		
	return ok;
} 
/////////////////////////////
//
//	startSlave()	GoToSiSy:d:2431|o:2432
//
//	PE:
//		slaveI2CAdr=Slave-Adresse in den Bits 7..1
//		Bit0 = TWI_READ|TWI_WRITE
//
/////////////////////////////
bool SysTwiBusStandardAvr::startSlave(uint8 slaveI2CAdrAndRW)
{
	//  aus Template: SysI2cBusAvr		GoToSiSy:o:1666|zBase:1
	bool ok = generateStart();
	if(ok)
		writeByteAck( slaveI2CAdrAndRW ); 	
	
	// GoToSiSy:d:2431|o:2432|zbase:6
		
	return ok;
} 

