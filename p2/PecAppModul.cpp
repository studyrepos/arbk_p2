//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>81</ObjektNummer>	GoToSiSy:d:80

/////////////////////////////
//
//	Klasse: PecAppModul
//
//	Basisklasse f�r selbst�ndig agierende Programm-Module.
//
/////////////////////////////
#define GeneratedBySisy
#define cpp_PecAppModul
#define SISY_CLASS_NAME PecAppModul
#include "PecAppModul.h"

#include "Controller.h"
#include "VolumesManager.h"

extern  Controller app;
extern  VolumesManager gVolumesManager;


PecAppModul* pFirstPecAppModul=0;
/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:80|o:81
//
/////////////////////////////
PecAppModul::PecAppModul()
{
pNextObject = 0;
	//  aus Template: PecLinkedListElement
	llInit();
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:80|o:81
//
/////////////////////////////
PecAppModul::~PecAppModul()
{
	//  aus Template: PecLinkedListElement
	llDeinit();
	
}
/*/////////////////////////////
//
//	onPower()	GoToSiSy:d:80|o:82
//
/////////////////////////////
*/
void PecAppModul::onPower()
{
	 

} 
/*/////////////////////////////
//
//	onStart()	GoToSiSy:d:80|o:83
//
/////////////////////////////
*/
void PecAppModul::onStart()
{
	

} 
/*/////////////////////////////
//
//	onWork()	GoToSiSy:d:80|o:84
//
/////////////////////////////
*/
void PecAppModul::onWork()
{
	

} 
/*/////////////////////////////
//
//	onTimer10ms()	GoToSiSy:d:80|o:85
//
/////////////////////////////
*/
void PecAppModul::onTimer10ms()
{
	

} 
/*/////////////////////////////
//
//	onEvent100ms()	GoToSiSy:d:80|o:86
//
/////////////////////////////
*/
void PecAppModul::onEvent100ms()
{
	

} 
/*/////////////////////////////
//
//	onEvent1s()	GoToSiSy:d:80|o:87
//
/////////////////////////////
*/
void PecAppModul::onEvent1s()
{
	

} 
/*/////////////////////////////
//
//	llInit()	GoToSiSy:d:80|o:81
//
/////////////////////////////
*/
void PecAppModul::llInit()
{
	//  aus Template: PecLinkedListElement		GoToSiSy:o:64|zBase:1
	////////// verkettete Liste - ADD
	PecAppModul* p;
	p=pFirstPecAppModul;
	// wenn ich der Erste bin
	if(p==0)
	{
		pFirstPecAppModul=this;
	}
	else
	{
		while(p->pNextObject!=0)
			p=p->pNextObject;
		p->pNextObject=this;
	}
	
	// GoToSiSy:d:80|o:81|zbase:17
		

} 
/*/////////////////////////////
//
//	llDeinit()	GoToSiSy:d:80|o:81
//
/////////////////////////////
*/
void PecAppModul::llDeinit()
{
	//  aus Template: PecLinkedListElement		GoToSiSy:o:65|zBase:1
	////////// verkettete Liste - REMOVE
	PecAppModul* p;
	p=pFirstPecAppModul;
	// wenn ich der erste bin
	if(p==this)
	{
		pFirstPecAppModul=pNextObject;
	}
	else
	{
		// ich muss da sein ...
		while(p->pNextObject!=this)	
			p=p->pNextObject;
		p->pNextObject=pNextObject;
	}
	
	
	// GoToSiSy:d:80|o:81|zbase:19
		

} 

