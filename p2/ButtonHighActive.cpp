//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>197</ObjektNummer>	GoToSiSy:d:173

#define GeneratedBySisy
#define cpp_ButtonHighActive
#define SISY_CLASS_NAME ButtonHighActive
#include "ButtonHighActive.h"

#include "Controller.h"
#include "VolumesManager.h"


#define PinInputLogic_1
#ifndef ButtonClickDebounce
#define ButtonClickDebounce 5
#endif
#ifndef ButtonHoldTime
#define ButtonHoldTime 100
#endif
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:173|o:197
//
/////////////////////////////
ButtonHighActive::ButtonHighActive()
{
holdCounter = 0;
releaseCounter = 0;
	state=0;
	
	changeState_state(state_state_Nothing);
	
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:173|o:197
//
/////////////////////////////
ButtonHighActive::~ButtonHighActive()
{
	
	
}
/*/////////////////////////////
//
//	configPu()	GoToSiSy:d:173|o:197
//
/////////////////////////////
*/
void ButtonHighActive::configPu(port_t port, pinMask_t bitmask)
{
	//  aus Template: SysPinInputBaseAvr		GoToSiSy:o:1764|zBase:1
	this->bitmask = bitmask; 
	this->port	  = &port;
	
	// Eingang
	*(this->port-1) &= ~bitmask;	// DDR Register
		
	// PullUp
	*(this->port) |= bitmask;		// PORTx Register
	
	// GoToSiSy:d:173|o:197|zbase:11
		

} 
/*/////////////////////////////
//
//	config()	GoToSiSy:d:173|o:197
//
/////////////////////////////
*/
void ButtonHighActive::config(port_t port, pinMask_t bitmask)
{
	//  aus Template: SysPinInputBaseAvr		GoToSiSy:o:1763|zBase:1
	this->bitmask = bitmask; 
	this->port	  = &port;
		
	// Eingang
	*(this->port-1) &= ~bitmask;	// DDR Register
	
	// GoToSiSy:d:173|o:197|zbase:8
		

} 
/*/////////////////////////////
//
//	configPd()	GoToSiSy:d:173|o:197
//
/////////////////////////////
*/
void ButtonHighActive::configPd(port_t port, pinMask_t bitmask)
{
	

} 
/*/////////////////////////////
//
//	getState()	GoToSiSy:d:173|o:197
//
/////////////////////////////
*/
bool ButtonHighActive::getState()
{
	//  aus Template: SysPinAvr		GoToSiSy:o:1723|zBase:1
	#if defined(PinInputLogic_0)	// Low
		bool isOn = !(*(port-2) & bitmask);
	#else // defined(PinInputLogic_1)	// High
		bool isOn = *(port-2) & bitmask;
	#endif
	
	//  aus Template: PinInputBase		GoToSiSy:o:47|zBase:1
	
	
	// GoToSiSy:d:173|o:197|zbase:11
		
return isOn;
} 
/*/////////////////////////////
//
//	onClick()	GoToSiSy:d:173|o:197
//
/////////////////////////////
*/
void ButtonHighActive::onClick()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:207|zBase:1
	app.postEvent(PecSysEvent|OnClick, 0, this);
	
	// GoToSiSy:d:173|o:197|zbase:4
		

} 
/*/////////////////////////////
//
//	onTimer10ms()	GoToSiSy:d:173|o:197
//
/////////////////////////////
*/
void ButtonHighActive::onTimer10ms()
{
	switch(state)
	{
		case state_state_Nothing:
		{
			// GoToSiSy:d:204|o:3618
			
			if(isPressed())
			{
				changeState_state(state_state_down);
				break;
			}
			break;
		}
		case state_state_down:
		{
			// GoToSiSy:d:204|o:217|zBase:1
			if (isPressed())
			{
				holdCounter++;
				releaseCounter=0;
			}
			else
			{
				holdCounter=0;
				releaseCounter++;
			}
			
			break;
		}
		case state_state_click:
		{
			// GoToSiSy:d:204|o:3620
			
			changeState_state(state_state_Nothing);
			break;
		}
		case state_state_hold:
		{
			// GoToSiSy:d:204|o:223|zBase:1
			onHolding();
			break;
		}
	}
	// GoToSiSy:d:173|o:197|zbase:44
		

} 
/*/////////////////////////////
//
//	onHoldStart()	GoToSiSy:d:173|o:197
//
/////////////////////////////
*/
void ButtonHighActive::onHoldStart()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:209|zBase:1
	app.postEvent(PecSysEvent|OnHoldStart, 0, this);
	
	// GoToSiSy:d:173|o:197|zbase:4
		

} 
/*/////////////////////////////
//
//	onHoldEnd()	GoToSiSy:d:173|o:197
//
/////////////////////////////
*/
void ButtonHighActive::onHoldEnd()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:210|zBase:1
	app.postEvent(PecSysEvent|OnHoldEnd, 0, this);
	
	// GoToSiSy:d:173|o:197|zbase:4
		

} 
/*/////////////////////////////
//
//	isPressed()	GoToSiSy:d:173|o:197
//
//	Ermittelt, ob der Button gedr�ckt ist.
//
/////////////////////////////
*/
bool ButtonHighActive::isPressed()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:211|zBase:1
	bool pressed=getState();
	// #ifdef LogicLowActive
	// 	if (!LogicLowActive)
	// 		pressed = !pressed;
	// #endif
	
	// GoToSiSy:d:173|o:197|zbase:8
		
return pressed;
} 
/*/////////////////////////////
//
//	onHolding()	GoToSiSy:d:173|o:197
//
//	wird alle 10ms aufgerufen solange Hold aktiv ist
//
/////////////////////////////
*/
void ButtonHighActive::onHolding()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:212|zBase:1
	app.postEvent(PecSysEvent|OnHolding, 0, this);
	
	// GoToSiSy:d:173|o:197|zbase:4
		

} 
/*/////////////////////////////
//
//	waitForPress()	GoToSiSy:d:173|o:197
//
//	Wartet bis die taste gedr�ckt wurde.
//
/////////////////////////////
*/
void ButtonHighActive::waitForPress()
{
	//  aus Template: ButtonClickAndHoldBase		GoToSiSy:o:213|zBase:1
	while( !isPressed() )
		{}
		
	// GoToSiSy:d:173|o:197|zbase:5
		

} 
/*/////////////////////////////
//
//	onWork()	GoToSiSy:d:173|o:84
//
/////////////////////////////
*/
void ButtonHighActive::onWork()
{
	switch(state)
	{
		case state_state_down:
		{
			// GoToSiSy:d:204|o:3619
			
			if(releaseCounter>=ButtonClickDebounce)
			{
				changeState_state(state_state_click);
				break;
			}
			// GoToSiSy:d:204|o:3621
			
			if(holdCounter>=ButtonHoldTime )
			{
				changeState_state(state_state_hold);
				break;
			}
			break;
		}
		case state_state_hold:
		{
			// GoToSiSy:d:204|o:3622
			
			if(!isPressed())
			{
				changeState_state(state_state_Nothing);
				break;
			}
			break;
		}
	}
	// GoToSiSy:d:173|o:84|zbase:33
		

} 

/////////////////////////////
//
//	changeState_state()
//
/////////////////////////////
void ButtonHighActive::changeState_state(state_t newState)
{
	switch(state)
	{
		case state_state_hold:
		{
			// GoToSiSy:o:222|zBase:1
			onHoldEnd();
			
			break;
		}
	}
	state = newState;
	switch(state)
	{
		case state_state_down:
		{
			// GoToSiSy:o:216|zBase:1
			releaseCounter=0;
			holdCounter=0;
			
			break;
		}
		case state_state_click:
		{
			// GoToSiSy:o:219|zBase:1
			onClick();
			
			break;
		}
		case state_state_hold:
		{
			// GoToSiSy:o:221|zBase:1
			onHoldStart();
			
			break;
		}
	}
}
