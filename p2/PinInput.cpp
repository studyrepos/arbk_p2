//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>25</ObjektNummer>	GoToSiSy:d:7

#define GeneratedBySisy
#define cpp_PinInput
#define SISY_CLASS_NAME PinInput
#include "PinInput.h"

#include "Controller.h"
#include "VolumesManager.h"


#define PinInputLogic_1
extern  Controller app;
extern  VolumesManager gVolumesManager;


/////////////////////////////
//
//	Konstruktor	GoToSiSy:d:7|o:25
//
/////////////////////////////
PinInput::PinInput()
{
	
}
/////////////////////////////
//
//	Destruktor	GoToSiSy:d:7|o:25
//
/////////////////////////////
PinInput::~PinInput()
{
	
}
/*/////////////////////////////
//
//	configPu()	GoToSiSy:d:7|o:25
//
/////////////////////////////
*/
void PinInput::configPu(port_t port, pinMask_t bitmask)
{
	//  aus Template: SysPinInputBaseAvr		GoToSiSy:o:1764|zBase:1
	this->bitmask = bitmask; 
	this->port	  = &port;
	
	// Eingang
	*(this->port-1) &= ~bitmask;	// DDR Register
		
	// PullUp
	*(this->port) |= bitmask;		// PORTx Register
	
	// GoToSiSy:d:7|o:25|zbase:11
		

} 
/*/////////////////////////////
//
//	config()	GoToSiSy:d:7|o:25
//
/////////////////////////////
*/
void PinInput::config(port_t port, pinMask_t bitmask)
{
	//  aus Template: SysPinInputBaseAvr		GoToSiSy:o:1763|zBase:1
	this->bitmask = bitmask; 
	this->port	  = &port;
		
	// Eingang
	*(this->port-1) &= ~bitmask;	// DDR Register
	
	// GoToSiSy:d:7|o:25|zbase:8
		

} 
/*/////////////////////////////
//
//	configPd()	GoToSiSy:d:7|o:25
//
/////////////////////////////
*/
void PinInput::configPd(port_t port, pinMask_t bitmask)
{
	

} 
/*/////////////////////////////
//
//	getState()	GoToSiSy:d:7|o:25
//
/////////////////////////////
*/
bool PinInput::getState()
{
	//  aus Template: SysPinAvr		GoToSiSy:o:1723|zBase:1
	#if defined(PinInputLogic_0)	// Low
		bool isOn = !(*(port-2) & bitmask);
	#else // defined(PinInputLogic_1)	// High
		bool isOn = *(port-2) & bitmask;
	#endif
	
	//  aus Template: PinInputBase		GoToSiSy:o:47|zBase:1
	
	
	// GoToSiSy:d:7|o:25|zbase:11
		
return isOn;
} 

