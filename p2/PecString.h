//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>171</ObjektNummer>	GoToSiSy:d:117

#if !defined(h_PecString)
#define h_PecString

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#ifndef StringSize_t
#define StringSize_t sysUint_t
#endif
#ifndef StringFormatSizeStart
#define StringFormatSizeStart 50
#endif
#include <string.h>
#include <stdarg.h>
#define String PecString
#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class PecString
{

public:
	//	addContent()	GoToSiSy:d:117|o:171
	//	F�gt an einen vorhandenen String an.
	//	
	//	PE:
	//		text= anzuf�gender Text
	//		size=Anzahl der anzuf�genden Zeichen, wenn -1 wird strlen verwendet
	void addContent(const char* text, StringSize_t size=-1);
	//	find()	GoToSiSy:d:117|o:171
	//	Liefert das erste Auftreten eines Zeichens im String.
	//	
	//	PE:
	//		c=Zeichen
	//		startPos=die Start-Position der Suche
	//	PA:
	//		Zeichenposition oder -1, wenn nicht gefunden
	//		
	StringSize_t find(char c, StringSize_t startPos=0) const;
	//	find()	GoToSiSy:d:117|o:171
	//	Liefert das erste Auftreten einer Zeichenkette im String.
	//	
	//	PE:
	//		search = Zeichenkette, die gesucht wird
	//		startPos = (optional) die Start-Position der Suche
	//	PA:
	//		Startposition oder -1, wenn nicht gefunden
	//		
	StringSize_t find(char* search, StringSize_t startPos=0) const;
	//	format()	GoToSiSy:d:117|o:171
	//	Beachte: es werden immer 16bit-Zahlen umgesetzt, d.h. keine Vorzeichen bei 8-bit-Zahlen und keinerlei 32bit-Zahlen.
	void format(const char* format, ...);
	//	getPart()	GoToSiSy:d:117|o:171
	//	Gibt eine Teilzeichenkette zur�ck.
	//	Wenn zu viel angegeben wurde, wird nur bis zum Ende zur�ckgegeben. Wenn die Startposition zu gro� oder anz gleich Null ist, wird eine leere Zeichenkette zur�ckgegeben.
	//	
	//	PE:
	//		startPos = Offset des ersten Zeichens (mit Null beginnend)
	//		anz = Anzahl der gew�nschten Zeichen
	String getPart(StringSize_t startPos, StringSize_t anz=-1) const;
	//	getSize()	GoToSiSy:d:117|o:171
	//	Liefert die Gr��e des Strings.
	StringSize_t getSize() const;
	//	operator char*()	GoToSiSy:d:117|o:171
	//	Casting-Operator nach char*
	 operator char*();
	//	operator const char*()	GoToSiSy:d:117|o:171
	//	Casting-Operator nach const char*
	 operator const char*() const;
	//	operator+()	GoToSiSy:d:117|o:171
	//	Operator f�r das Zusammensetzen von Strings.
	String operator+(const String& text);
	//	operator+=()	GoToSiSy:d:117|o:171
	//	Operator f�r das Anf�gen eines Zeichens.
	String& operator+=(const char c);
	//	operator+=()	GoToSiSy:d:117|o:171
	//	Operator f�r das Anf�gen von Strings.
	String& operator+=(const String& text);
	//	operator==()	GoToSiSy:d:117|o:171
	//	Operator zum Vergleich mit einem anderen String.
	bool operator==(const String& text) const;
	//	operator=()	GoToSiSy:d:117|o:171
	//	Zuweisungsoperator mit const char*
	//	BSP:
	//		text="Zeichenkette";
	String& operator=(const char* text);
	//	setContent()	GoToSiSy:d:117|o:171
	//	Setzt den Inhalt eines Strings.
	//	
	//	PE:
	//		text = zu verwendender Text
	//		size = Anzal der aus text zu �bertragenden Zeichen
	bool setContent(const char* text, StringSize_t size = -1);
	//	setSize()	GoToSiSy:d:117|o:171
	//	Setzt die Gr��e des Strings.
	//	Die Funktion allokiert Speicher f�r den String in 4-Byte-Bl�cken.
	//	Bei OutOfMemory wird ein Watchdog-Reset ausgel�st.
	//	
	//	PE:
	//		newSize=ben�tigte Speicher-Gr��e
	bool setSize(StringSize_t newSize);
	//	String()	GoToSiSy:d:117|o:171
	//	Erstellt einen String aus einer konstanten Zeichenkette.
	//	
	//	PE:
	//		text=Zeichenkette, z.B. "der Text"
	 String(const char* text);

	//	addInt()	GoToSiSy:d:117|o:171
	void addInt(int32_t num);
	//	operator==()	GoToSiSy:d:117|o:171
	//	Operator zum Vergleich mit einer Flash-Zeichenkette.
	bool operator==(const char* pText) const;
	//	toInt()	GoToSiSy:d:117|o:171
	//	Wandelt einen String in eine Zahl um. Es werden Hexadezimal-Zahlen (0x..), Bin�rzahlen (0b...) und Dezimalzahlen erkannt.
	//	
	//	PE:
	//		endPt = (optional) Zeiger auf Zeiger f�r die R�ckgabe der Position, an der das erste unbekannte Zeichen auftrat.
	//		
	//	PA:
	//		erkannte Zahl, Null bei Fehler
	int32_t toInt(uint8_t** endPt=NULL /*Out*/) const;
	//	findReverse()	GoToSiSy:d:117|o:171
	//	Liefert das letzte Auftreten eines Zeichens im String.
	//	
	//	PE:
	//		c=Zeichen
	//	PA:
	//		Zeichenposition von vorn oder -1, wenn nicht gefunden
	//		
	StringSize_t findReverse(char c) const;

	//	toUpperCase()	GoToSiSy:d:117|o:171
	void toUpperCase();

	//	toLowerCase()	GoToSiSy:d:117|o:171
	void toLowerCase();
	//	operator=()	GoToSiSy:d:117|o:171
	//	Zuweisungsoperator mit const char*
	//	BSP:
	//		text="Zeichenkette";
	String& operator=(const String& other);

	//	addInt()	GoToSiSy:d:117|o:171
	void addInt(uint32_t num);

	//	addNumBin()	GoToSiSy:d:117|o:171
	void addNumBin(uint32_t num);

	//	addNumHex()	GoToSiSy:d:117|o:171
	void addNumHex(uint32_t num);

	//	clear()	GoToSiSy:d:117|o:171
	void clear();
	//	count()	GoToSiSy:d:117|o:171
	//	Z�hlt das Vorkommen eines bestimmten Zeichens im Text.
	StringSize_t count(char ch) const;

	//	fromInt()	GoToSiSy:d:117|o:171
	void fromInt(int32_t num);

	//	fromInt()	GoToSiSy:d:117|o:171
	void fromInt(uint32_t num);

	//	fromNumBin()	GoToSiSy:d:117|o:171
	void fromNumBin(uint32_t num);

	//	fromNumHex()	GoToSiSy:d:117|o:171
	void fromNumHex(uint32_t num);

	//	getPartTo()	GoToSiSy:d:117|o:171
	String getPartTo(char trenn);

	//	insert()	GoToSiSy:d:117|o:171
	void insert(const String& text, StringSize_t startPos);

	//	insert()	GoToSiSy:d:117|o:171
	void insert(char newChar, StringSize_t pos);

	//	remove()	GoToSiSy:d:117|o:171
	void remove(StringSize_t startPos, StringSize_t anz);

	//	replace()	GoToSiSy:d:117|o:171
	void replace(const char* txtOld, const char* txtNew);

	//	replace()	GoToSiSy:d:117|o:171
	void replace(char charOld, char charNew);

	//	trim()	GoToSiSy:d:117|o:171
	void trim();

	//	urlDecode()	GoToSiSy:d:117|o:171
	void urlDecode();

	//	urlEncode()	GoToSiSy:d:117|o:171
	void urlEncode();
	//	operator=()	GoToSiSy:d:117|o:171
	//	Zuweisungsoperator mit const char
	//	BSP:
	//		text='A';
	String& operator=(const char c);
	//	String()	GoToSiSy:d:117|o:171
	//	Erstellt einen String aus einen anderen String (Copy-Konstruktor)
	//	
	//	PE:
	//		other=anderer String
	 String(const String& other);
	//	toFloat()	GoToSiSy:d:117|o:171
	//	Wandelt einen String in eine Gleitkomma-Zahl um.
	//	
	//	PE:
	//		
	//	PA:
	//		erkannte Zahl, Null bei Fehler
	double toFloat() const;

	//	formatAdd()	GoToSiSy:d:117|o:171
	void formatAdd(const char* format, ...);

	//	isEqual()	GoToSiSy:d:117|o:171
	bool isEqual(const char* pText);

	//	addInt()	GoToSiSy:d:117|o:171
	void addInt(uint16_t num);

	//	addInt()	GoToSiSy:d:117|o:171
	void addInt(int16_t num);

	//	addInt()	GoToSiSy:d:117|o:171
	void addInt(uint8_t num);

	//	addInt()	GoToSiSy:d:117|o:171
	void addInt(int8_t num);
	//	setContentFlash()	GoToSiSy:d:117|o:171
	//	Setzt den Inhalt eines Strings.
	//	
	//	PE:
	//		text = zu verwendender Text, muss im Flash stehen
	//		size = Anzal der aus text zu �bertragenden Zeichen
	bool setContentFlash(const char* text, StringSize_t size = -1);
	//	addContentFlash()	GoToSiSy:d:117|o:171
	//	F�gt an einen vorhandenen String an.
	//	
	//	PE:
	//		text= anzuf�gender Text, muss im Flash stehen
	//		size=Anzahl der anzuf�genden Zeichen, wenn -1 wird strlen verwendet
	void addContentFlash(const char* text, StringSize_t size=-1);

	//	isEqualFlash()	GoToSiSy:d:117|o:171
	bool isEqualFlash(const char* pText);
	//	Datenpuffer des Strings
	uint8_t* pBuffer;
	//	Konstruktor	GoToSiSy:d:117|o:171
	PecString();

	//	Destruktor	GoToSiSy:d:117|o:171
	~PecString();

private:

	//	_itoa()	GoToSiSy:d:117|o:171
	void _itoa(char* buf, unsigned int d, int base);

	//	_sprintf()	GoToSiSy:d:117|o:171
	int _sprintf(const char* fmt, va_list va);

protected:
	//	deinit()	GoToSiSy:d:117|o:171
	//	Deinitialisierung/Speicherfreigabe
	void deinit();
	//	aktuelle Stringl�nge
	StringSize_t len;


};
	//	operator+()	GoToSiSy:d:117|o:171
	//	Zusammensetzen von Zeichenketten.
	String operator+(const char* txt1, const String& txt2);

#endif
