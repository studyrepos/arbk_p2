//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1118</ObjektNummer>	GoToSiSy:d:1062

#if !defined(h_Pec24Cxx)
#define h_Pec24Cxx

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "PecI2cDeviceInterface.h"

#include "pec_Strings.h"

#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class Pec24Cxx : public PecI2cDeviceInterface
{

public:
	//	readData()	GoToSiSy:d:1062|o:1119
	//	Liest an einer Adresse im EEPROM ein Byte aus.
	//	
	//	PE:
	//		adress = die Speicheradresse im EEPROM
	//		
	//	PA:
	//		das ausgelesene Byte
	uint8_t readData(uint8_t adress);
	//	writeData()	GoToSiSy:d:1062|o:1120
	//	Schreibt an einer Adresse im EEPROM ein Byte.
	//	
	//	PE:
	//		adress = die Speicheradresse im EEPROM
	//		data =  das zu schreibende Byte
	//		
	//	PA:
	//		ob Aktion erfolgreich war
	bool writeData(uint8_t adress, uint8_t data);
	//	readData()	GoToSiSy:d:1062|o:1121
	//	Liest an einer Adresse im EEPROM mehrere Bytes.
	//	
	//	PE:
	//		address = die Speicheradresse im EEPROM
	//		data =  Pointer f�r die ausgelesenden Daten
	//		count = Anzahl der Daten in Byte
	//		
	//	PA:
	//		ob Aktion erfolgreich war
	bool readData(uint8_t address, void* pData, uint8_t count);
	//	writeData()	GoToSiSy:d:1062|o:1122
	//	Schreibt an einer Adresse im EEPROM mehrere Bytes.
	//	
	//	PE:
	//		address = die Speicheradresse im EEPROM
	//		data =  die zu schreibenden Daten
	//		count = Anzahl der Daten in Byte
	//		
	//	PA:
	//		ob Aktion erfolgreich war
	bool writeData(uint8_t address, void* pData, uint8_t count);
	//	Konstruktor	GoToSiSy:d:1062|o:1118
	Pec24Cxx();

	//	Destruktor	GoToSiSy:d:1062|o:1118
	~Pec24Cxx();

private:

protected:


};

#endif
