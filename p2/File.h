//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>1211</ObjektNummer>	GoToSiSy:d:1182

#if !defined(h_File)
#define h_File

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include "ff.h"
#include "pec_Strings.h"

#include <avr\io.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class File
{

public:

	//	open()	GoToSiSy:d:1182|o:1212
	bool open(const String& fileName, bool readonly=false, bool createIfNotExists=false);

	//	close()	GoToSiSy:d:1182|o:1214
	bool close();

	//	read()	GoToSiSy:d:1182|o:1215
	uint32_t read(uint8_t* pBuffer, uint32_t len);

	//	write()	GoToSiSy:d:1182|o:1216
	uint32_t write(const uint8_t* pBuffer, uint32_t len);

	//	isOpen()	GoToSiSy:d:1182|o:1217
	bool isOpen();

	//	seek()	GoToSiSy:d:1182|o:1218
	bool seek(uint32_t pos= -1);

	//	getSize()	GoToSiSy:d:1182|o:1219
	uint32_t getSize();

	//	eof()	GoToSiSy:d:1182|o:1221
	bool eof();

	//	setSize()	GoToSiSy:d:1182|o:1222
	bool setSize(uint32_t newSize);

	//	tell()	GoToSiSy:d:1182|o:1223
	uint32_t tell();

	//	create()	GoToSiSy:d:1182|o:1224
	bool create(const String& fileName, bool overwriteIfExists=false, bool readonly=false);

	//	read()	GoToSiSy:d:1182|o:1225
	uint32_t read(String& text, uint32_t len=-1);

	//	write()	GoToSiSy:d:1182|o:1226
	uint32_t write(const String& text);

	//	append()	GoToSiSy:d:1182|o:1227
	uint32_t append(const uint8_t* pBuffer, uint32_t len);

	//	append()	GoToSiSy:d:1182|o:1228
	uint32_t append(const String& text);
	FRESULT errorCode;
	//	Konstruktor	GoToSiSy:d:1182|o:1211
	File();

	//	Destruktor	GoToSiSy:d:1182|o:1211
	~File();

private:

protected:
	FIL file;


};

#endif
