//////////////////////////////////////////////////////
//
// Diese Quellcodedatei wurde automatisch erzeugt.
// SiSy UML CodeGenerierung
//
//////////////////////////////////////////////////////

///<ObjektNummer>4645</ObjektNummer>	GoToSiSy:d:4612

#if !defined(h_LedD6)
#define h_LedD6

// Defines
#define F_CPU 3686400
#define MCU ATMEGA8

#include <avr\io.h>
#include <avr\interrupt.h>
#include "PecAppModul.h"
#include "PinOutput.h"

#include "Pec.h"
#include "MCU_ATmega_xxx.h"

#include <avr\io.h>
#include <stdlib.h>
// aus Artefakt:DebugMask GoToSiSy:o:1055
#ifndef DebugMask_h
#define DebugMask_h

		//	DebugPrintM(DmEthSocked,"Hallo");
		#ifndef DmMASK
			#define DmMASK 0
		#endif
		
		#define DmEth 		0x100
		#define DmEthHardware	(DmEth+0x01)
		#define DmEthPacket 	(DmEth+0x02)
		#define DmEthSocket 	(DmEth+0x04)
		#define DmEthArp	 	(DmEth+0x08)
		#define DmEthHttp	 	(DmEth+0x10)
		#define DmEthAll		(DmEth+0xFF)

#endif

// aus Artefakt:avrTypes.h GoToSiSy:o:1639
#ifndef avrTypes_h
#define avrTypes_h

	#define VARISNOTDEF 1

	// Controller Verarbeitungsbreite
	typedef int8_t  sysInt_t;
	typedef uint8_t sysUint_t;
	
	// ADC Ergebnisreite
	typedef uint16_t adc_t;
	
	typedef uint8_t portMask_t;
	typedef uint8_t pinMask_t;
	
	typedef volatile uint8_t& port_t;

	#define SystemCoreClock F_CPU

	#define __PROG_TYPES_COMPAT__
	#include <avr/pgmspace.h>
	#include <stdlib.h>
	#include <myAVR.h>

#endif



class LedD6 : public PecAppModul
{

public:
	//	on()	GoToSiSy:d:4612|o:4645
	//	Schaltet die Led an.
	void on();

	//	off()	GoToSiSy:d:4612|o:4645
	//	Schaltet die Led aus.
	void off();

	//	flash()	GoToSiSy:d:4612|o:4645
	//	L�sst die Led einmal kurz aufblinken.
	void flash(uint8_t flashTime=10);

	//	blink()	GoToSiSy:d:4612|o:4645
	//	Aktiviert einen Blink-Code der Led.
	//	* 0x00: Led aus
	//	* 0xFF: Led an
	//	* 0xFE: Led toggle, bei jedem 10ms-Timer umschalten
	//	* sonst: Led blinkt die angegebene Anzahl, macht eine Pause und blinkt erneut.
	//	
	//	PE:
	//		blinkCode=neuer Blink-Code laut Beschreibung.
	void blink(uint8_t blinkCode=1);

	//	nextBlinkCode()	GoToSiSy:d:4612|o:4645
	//	Schaltet zum n�chsten BlinkCode.
	//	Der aktuelle Blink-Code wird um offset erh�ht.
	//	
	//	PE:
	//		offset=Blink-Code Erh�hung
	void nextBlinkCode(int8_t offset=1);

	//	toggle()	GoToSiSy:d:4612|o:4645
	//	Schaltet die Led an.
	void toggle();

	//	getBlinkCode()	GoToSiSy:d:4612|o:4645
	//	Liefert den aktuellen Blink-Code der Led.
	//	
	//	PE:
	//		keine
	//		
	//	PA:
	//		aktueller blinkCode
	uint8_t getBlinkCode();

	//	setBlinkCode()	GoToSiSy:d:4612|o:4645
	//	Aktiviert einen Blink-Code der Led.
	//	* 0x00: Led aus
	//	* 0xFF: Led an
	//	* 0xFE: Led toggle, bei jedem 10ms-Timer umschalten
	//	* sonst: Led blinkt die angegebene Anzahl, macht eine Pause und blinkt erneut.
	//	
	//	PE:
	//		blinkCode=neuer Blink-Code laut Beschreibung.
	void setBlinkCode(uint8_t blinkCode);

	//	config()	GoToSiSy:d:4612|o:4645
	void config(port_t port, pinMask_t bitmask);

	//	dimm()	GoToSiSy:d:4612|o:4645
	//	Dimmt die LED mit 50%.
	void dimm();

	//	flicker()	GoToSiSy:d:4612|o:4645
	void flicker();

	//	Konstruktor	GoToSiSy:d:4612|o:4645
	LedD6();

	//	Destruktor	GoToSiSy:d:4612|o:4645
	~LedD6();

private:

protected:
	//	onTimer10ms()	GoToSiSy:d:4612|o:4645
	//	Anwendungstimer 10ms, steuert die Blink-Codes.
	virtual void onTimer10ms();

	//	Timeout f�r Flash (in 10 ms-Schritten??)
	uint8_t flashTimeout;
	uint8_t blinkCode;
	uint8_t blinkCounter;
	uint8_t blinkTimeout;
	uint8_t blinkTime;
	uint8_t changeState;
	PinOutput pin;


};

#endif
